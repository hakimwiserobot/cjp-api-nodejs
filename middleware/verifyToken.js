const jwt = require('jsonwebtoken')

function verifyToken (req, res, next) {
  // const token = req.cookies ? req.cookies.token : null // token in cookie
  const authorization = req.headers.Authorization ? req.headers.Authorization : req.headers.authorization;
  const splittedAuthorization = authorization.split(' ')
  const token = splittedAuthorization[1];
  if (!token) {
    return res.status(403).send({
      success:false, message: 'No token provided.'
    })
  }

  jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
    if (err) {
      return res.status(500).send({
        success: false,
        message: 'Fail to Authentication. Error -> ' + err
      })
    }
    req.user = { 
      id: decoded.id,
      role: decoded.role,
      loginId: decoded.loginId
    }

    next()
  })
}

module.exports = verifyToken