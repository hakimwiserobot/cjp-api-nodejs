const { sql,poolPromise } = require('../database/db')
const fs = require('fs');
var rawdata = fs.readFileSync('./query/queries.json');
var queries = JSON.parse(rawdata);

class PortOfRegistryController {

    async getPortOfRegistry(req , res){
      try {
        const pool = await poolPromise
        const result = await pool.request()
          .query(queries.getPortOfRegistry)
          
          res.json(result.recordset)
      } catch (error) {
        console.log(error)
        res.status(500).json({ success:false, message:'Unable to retrieve list of registry port. Please try again.', error })
      }
    }

    async addPortOfRegistry(req , res){
      try {
        console.log("addPortOfRegistry: ", req.body);
        if(req.body.PortOfRegistry != null) {
          const pool = await poolPromise
          const result = await pool.request()
          .input('PortOfRegistry', sql.VarChar, req.body.PortOfRegistry)
          .query(queries.addPortOfRegistry)
          console.log("addPortOfRegistry result: ", result.recordset[0].Id);
          res.json(result.recordset[0])
        } else {
          res.status(200).json({ success:false, message:'Please fill all the details!' })
        }
      } catch (error) {
        console.log(error)
        res.status(500).json({ success:false, message:'Unable to add port of registry. Please try again.', error })
      }
    }
    async updatePortOfRegistry(req, res){
      try {
        console.log("updatePortOfRegistry: ", req.body);
        if(req.body.Id != null && req.body.PortOfRegistry != null) {
        const pool = await poolPromise
          const result = await pool.request()
          .input('Id',sql.SmallInt , req.body.Id)
          .input('PortOfRegistry',sql.VarChar , req.body.PortOfRegistry)
          .query(queries.updatePortOfRegistry)
          console.log("updatePortOfRegistry result: ", req.body.Id);
          res.json({Id: req.body.Id})
        } else {
          res.status(200).json({ success:false, message:'Please fill all the details!' })
        }
      } catch (error) {
        console.log(error)
        res.status(500).json({ success:false, message:'Unable to update port of registry. Please try again.', error })
      }
    }
    async deletePortOfRegistry(req , res){
      try {
          console.log(req);
        if(req.params.Id != null) {
          const pool = await poolPromise
            const result = await pool.request()
            .input('Id',sql.SmallInt , req.params.Id)
            .query(queries.deletePortOfRegistry)
            console.log("deletePortOfRegistry result: ", req.params.Id);
            res.json({Id: req.params.Id})
          } else {
            res.status(200).json({ success:false, message:'Please fill all the details!' })
          }
      } catch (error) {
        console.log(error)
        res.status(500).json({ success:false, message:'Unable to delete port of registry. Please try again.', error })
      }
    }
}

const controller = new PortOfRegistryController()
module.exports = controller;