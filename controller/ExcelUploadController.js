const exceljs = require('exceljs');
const multer = require('multer');
const fs = require('fs');
var rawJson = fs.readFileSync('./query/qApplicant.json');
var qApplicant = JSON.parse(rawJson);
const { sql, poolPromise } = require('../database/db')

var storage = multer.memoryStorage({
    destination: function(req, file, callback) {
        callback(null, '');
    }    
});
var upload = multer({ storage:storage }).any();

class ExcelUploadController {
    async updateEmpId(req, res) {
        upload(req, res, async(err) => {
            if (err) {
                console.log(err);
                return res.status(500).json({ success:false, message:"Fetching file error", error:err });
            }
            let dataToUpdate = [];
            var filebuffer = req.files[0].buffer;

            let workbook = new exceljs.Workbook();

            try {
                await workbook.xlsx.load(filebuffer);
            } catch (error) {
                console.log(error);
                return res.status(500).json({ success:false, message:"Reading file error", error:error }); 
            }

            let worksheet = workbook.getWorksheet(1);
            worksheet.eachRow({ includeEmpty:false },  (row, rowNumber) => {
                dataToUpdate.push(row.values);
            });

            // Verify excel header
            let isColumnExist = false;
            let indexFirstname = -1;
            let indexMiddlename = -1;
            let indexLastname = -1;
            let indexIC = -1;
            let indexPassport = -1;
            let indexEmpId = -1;
            if (dataToUpdate.length > 0 && dataToUpdate[0] != null) {
                let arrHeader = dataToUpdate[0];
                for (let i = 0; i < arrHeader.length; i++) {
                    if (arrHeader[i] == null) {
                        continue;
                    }
                    let header = arrHeader[i].toString().toLowerCase();
                    if (header == "ic") {
                        indexIC = i;
                    } else if (header == "passport") {
                        indexPassport = i;
                    } else if (header == "empid") {
                        indexEmpId = i;
                    } else if (header == "firstname") {
                        indexFirstname = i;
                    } else if (header == "middlename") {
                        indexMiddlename = i;
                    } else if (header == "lastname") {
                        indexLastname = i;
                    }
                }
            }

            if (indexFirstname < 0 || indexMiddlename < 0 || indexLastname < 0 || indexIC < 0 || indexPassport < 0 || indexEmpId < 0) {
                return res.status(500).json({ success:false, message:"Missing column in excel file" })
            }

            let result = {}
            let recordNotFound = [];
            try {
                const pool = await poolPromise
                for (let i = 1; i < dataToUpdate.length; i++) {
                    let isUpdateByIC = true;
                    let result1 = await pool
                    .request()
                    .input('IC', sql.VarChar, dataToUpdate[i][indexIC])
                    .input('Passport', sql.VarChar, dataToUpdate[i][indexPassport])
                    .query(qApplicant.getByIC);
                    let applicant = result1.recordset[0]

                    if (applicant == null) {
                        result1 = await pool
                        .request()
                        .input('IC', sql.VarChar, dataToUpdate[i][indexIC])
                        .input('Passport', sql.VarChar, dataToUpdate[i][indexPassport])
                        .query(qApplicant.getByPassport);
                        applicant = result1.recordset[0];
                        isUpdateByIC = false;
                    }

                    if (!applicant) {
                        recordNotFound.push({ IC : dataToUpdate[i][indexIC], Passport:dataToUpdate[i][indexPassport], EmpId:dataToUpdate[i][indexEmpId] });
                        continue;
                        // if (applicant.Name != dataToUpdate[i][indexFirstname]) {
                        //     continue;
                        // }
                        // if (applicant.MiddleName != dataToUpdate[i][indexMiddlename]) {
                        //     continue;
                        // }
                        // if (applicant.LastName != dataToUpdate[i][indexLastname]) {
                        //     continue;
                        // }
                    }

                    let updateQuery = ''
                    if (isUpdateByIC) {
                        updateQuery = qApplicant.UpdateEmpIdByIC;
                    } else {
                        updateQuery = qApplicant.UpdateEmpIdByPassport;
                    }

                    let newEmpId = '';
                    // newEmpid = applicant.EmpId ? applicant.EmpId + ',' : '';
                    // newEmpid = newEmpid + dataToUpdate[i][indexEmpId];
                    
                    const inputEmpId = dataToUpdate[i][indexEmpId].toString();
                    console.log(inputEmpId);
                    let newEmpIds = inputEmpId.split(',');
                    for(let i = 0; i < newEmpIds.length; i++) {
                        let empId = newEmpIds[i].replace(/\s/g, '');
                        if (empId.length > 0) {
                            var exist = -1;
                            if (applicant.EmpId != null) {
                                let currentEmpIds = applicant.EmpId.split(',');
                                for (let i = 0; i < currentEmpIds.length; i++) {
                                    const currentEmpId = currentEmpIds[i];
                                    if (currentEmpId == empId) {
                                        exist++;
                                    }
                                }
                            }

                            if (exist < 0) {
                                if (newEmpId.length == 0) {
                                    newEmpId = empId;
                                } else {
                                    newEmpId = newEmpId + ',' + empId;
                                }
                            }
                        }
                    }
                    
                    if (applicant.EmpId != null) {
                        if (newEmpId.length > 0) {
                            newEmpId = applicant.EmpId + ',' + newEmpId;
                        } else {
                            newEmpId = applicant.EmpId;
                        }
                    }
                    console.log(newEmpId);

                    result = await pool
                    .request()
                    .input('EmpId', sql.VarChar, newEmpId)
                    .input('IC', sql.VarChar, dataToUpdate[i][indexIC])
                    .input('Passport', sql.VarChar, dataToUpdate[i][indexPassport])
                    .query(updateQuery);
                }
            } catch (error) {
                console.log(error);
                return res.status(500).json({ success:false, message:"Reading file error", error:error }); 
            }

            return res.status(200).json({ success:true, result:result, totalRecord:dataToUpdate.length-1, recordNotFound:recordNotFound });
        });
    }
}

const controller = new ExcelUploadController()
module.exports = controller