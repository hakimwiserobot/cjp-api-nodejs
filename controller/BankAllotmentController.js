const { sql, poolPromise } = require('../database/db')
const fs = require('fs');
var path = require('path')
var rawdata = fs.readFileSync('./query/qApplicantBankAllotment.json');
var qApplicantBankAllotment = JSON.parse(rawdata);

class EmailController {

    async getAllBankAllotment(req, res) {
        try {
            const pool = await poolPromise
            const result = await pool.request()
              .query(qApplicantBankAllotment.getAll)
            res.json(result.recordset)
        } catch (error) {
            console.log(error)
            res.status(500).json({ success:false, message:__("error.retrieveList", __("module.bankAllotment")), error })
        }
    }

    async getBankAllotment(req, res) {
        const Id = req.params.Id
        try {
            const pool = await poolPromise
            const result = await pool.request()
              .input('Id', sql.VarChar, Id)
              .query(qApplicantBankAllotment.getById)
            res.json(result.recordset[0])
        } catch (error) {
            console.log(error)
            res.status(500).json({ success:false, message:__("error.retrieveList", __("module.bankAllotment")), error })
        }
    }

    async getBankAllotmentApplicant(req, res) {
        const Id = req.params.LoginEmail
        try {
            const pool = await poolPromise
            const result = await pool.request()
              .input('Id', sql.VarChar, Id)
              .query(qApplicantBankAllotment.getByApplicantId)
              res.json(result.recordset)
        } catch (error) {
            console.log(error)
            res.status(500).json({ success:false, message:__("error.retrieveList", __("module.bankAllotment")), error })
        }
    }

    async addBankAllotmentDetails(req, res) {
        try {
            const Id = req.body.Id;
            const LoginEmail = req.body.LoginEmail;
            const BeneficiaryType = req.body.BeneficiaryType;
            const Name = req.body.Name;
            const MiddleName = req.body.MiddleName;
            const LastName = req.body.LastName;
            const Relationship = req.body.Relationship;
            const ContactCtryCode = req.body.ContactCtryCode;
            const ContactNumber = req.body.ContactNumber;
            const IdentityType = req.body.IdentityType;
            const Identification = req.body.Identification;
            const BankName = req.body.BankName;
            const BankPlace = req.body.BankPlace;
            const BankSwiftCode = req.body.BankSwiftCode;
            const AccountNumber = req.body.AccountNumber;
            const Currency = req.body.Currency;
            const Signature = req.body.Signature;
            
            const pool = await poolPromise
            const result = await pool.request()
            .input('Id', sql.VarChar, Id)
            .input('LoginEmail', sql.VarChar, LoginEmail)
            .input('BeneficiaryType', sql.VarChar, BeneficiaryType)
            .input('Name', sql.VarChar, Name)
            .input('MiddleName', sql.VarChar, MiddleName)
            .input('LastName', sql.VarChar, LastName)
            .input('Relationship', sql.VarChar, Relationship)
            .input('ContactCtryCode', sql.VarChar, ContactCtryCode)
            .input('ContactNumber', sql.VarChar, ContactNumber)
            .input('IdentityType', sql.VarChar, IdentityType)
            .input('Identification', sql.VarChar, Identification)
            .input('BankName', sql.VarChar, BankName)
            .input('BankPlace', sql.VarChar, BankPlace)
            .input('BankSwiftCode', sql.VarChar, BankSwiftCode)
            .input('AccountNumber', sql.VarChar, AccountNumber)
            .input('Currency', sql.VarChar, Currency)
            .input('Signature', sql.VarChar, Signature)
            .query(qApplicantBankAllotment.add.join(' '))

            if (result.recordset[0] != null) {
                res.status(200).json({ success:true, message:__("success.add", __("module.bankAllotment")), Id:result.recordset[0].Id })
            } else {
                console.log("Record is empty");
                throw result
            }
        } catch (error) {
            console.log(error)
            res.status(500).json({ success:false, message:__("error.add", __("module.bankAllotment")), error })
        }
    }

    async updateBankAllotmentDetails(req, res) {
        try {
            const Id = req.body.Id;
            const LoginEmail = req.body.LoginEmail;
            const BeneficiaryType = req.body.BeneficiaryType;
            const Name = req.body.Name;
            const MiddleName = req.body.MiddleName;
            const LastName = req.body.LastName;
            const Relationship = req.body.Relationship;
            const ContactCtryCode = req.body.ContactCtryCode;
            const ContactNumber = req.body.ContactNumber;
            const IdentityType = req.body.IdentityType;
            const Identification = req.body.Identification;
            const BankName = req.body.BankName;
            const BankPlace = req.body.BankPlace;
            const BankSwiftCode = req.body.BankSwiftCode;
            const AccountNumber = req.body.AccountNumber;
            const Currency = req.body.Currency;
            const Signature = req.body.Signature;
            
            const pool = await poolPromise
            const result = await pool.request()
            .input('Id', sql.VarChar, Id)
            .input('LoginEmail', sql.VarChar, LoginEmail)
            .input('BeneficiaryType', sql.VarChar, BeneficiaryType)
            .input('Name', sql.VarChar, Name)
            .input('MiddleName', sql.VarChar, MiddleName)
            .input('LastName', sql.VarChar, LastName)
            .input('Relationship', sql.VarChar, Relationship)
            .input('ContactCtryCode', sql.VarChar, ContactCtryCode)
            .input('ContactNumber', sql.VarChar, ContactNumber)
            .input('IdentityType', sql.VarChar, IdentityType)
            .input('Identification', sql.VarChar, Identification)
            .input('BankName', sql.VarChar, BankName)
            .input('BankPlace', sql.VarChar, BankPlace)
            .input('BankSwiftCode', sql.VarChar, BankSwiftCode)
            .input('AccountNumber', sql.VarChar, AccountNumber)
            .input('Currency', sql.VarChar, Currency)
            .input('Signature', sql.VarChar, Signature)
            .query(qApplicantBankAllotment.updateDetails.join(' '))

            if (result.recordset[0]) {
                res.status(200).json({ success:true, message:__("success.update", __("module.bankAllotment")), Id:result.recordset[0].Id })
            } else {
                console.log("Record is empty");
                throw result
            }

        } catch(error) {
            console.log(error)
            res.status(500).json({ success:false, message:__("error.update", __("module.bankAllotment")), error })
        }
    }

    async updateBankAllotmentFiles(req, res) {
        try {
            const Id = req.body.Id;
            const LoginEmail = req.body.LoginEmail;
            const FileBankBook = req.body.FileBankBook;
            const FileMarriageCert = req.body.FileMarriageCert;
            const FileICPassport = req.body.FileICPassport;
            const FileBirthCert = req.body.FileBirthCert;
            const FileBirthCert2 = req.body.FileBirthCert2;
            
            const pool = await poolPromise
            const result = await pool.request()
            .input('Id', sql.VarChar, Id)
            .input('LoginEmail', sql.VarChar, LoginEmail)
            .input('FileBankBook', sql.VarChar, FileBankBook)
            .input('FileMarriageCert', sql.VarChar, FileMarriageCert)
            .input('FileICPassport', sql.VarChar, FileICPassport)
            .input('FileBirthCert', sql.VarChar, FileBirthCert)
            .input('FileBirthCert2', sql.VarChar, FileBirthCert2)
            .query(qApplicantBankAllotment.updateFiles.join(' '))

            if (result.rowsAffected[0]) {
                res.status(200).json({ success:true, message:__("success.update", __("module.bankAllotment")) })
            } else {
                console.log("Record is empty");
                throw result
            }
        } catch (error) {
            console.log(error)
            res.status(500).json({ success:false, message:__("error.update", __("module.bankAllotment")), error })
        }
    }

    async updateSelection(req, res) {
        try {
            const LoginEmail = req.body.LoginEmail;
            const IdNew = req.body.IdNew;
            // const IdOld = req.body.IdOld;

            const pool = await poolPromise
            const result1 = await pool.request()
            .input('Id', sql.VarChar, LoginEmail)
            .query(qApplicantBankAllotment.updateAllUnselected.join(' '))
            
            const result2 = await pool.request()
            .input('Id', sql.VarChar, IdNew)
            .query(qApplicantBankAllotment.updateSelected.join(' '))

            if (result2.recordset[0]) {
                res.status(200).json({ success:true, message:'Successfully update selection' })
            } else {
                console.log("Record is empty");
                throw result
            }
        } catch (error) {
            console.log(error)
            res.status(500).json({ success:false, message:'Unable to update bank allotment selection. Please try again.', error })
        }
    }
}

const controller = new EmailController()
module.exports = controller;