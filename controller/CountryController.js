const { sql,poolPromise } = require('../database/db')
const fs = require('fs');
var rawdata = fs.readFileSync('./query/queries.json');
var queries = JSON.parse(rawdata);

class CountryController {

    async getCountry(req , res){
      try {
        const pool = await poolPromise
        const result = await pool.request()
          .query(queries.getCountry)
          res.json(result.recordset)
      } catch (error) {
        console.log(error)
        res.status(500).json({ success:false, message:__("error.retrieveList", __("module.country")), error })
      }
    }

    async addCountry(req , res){
      try {
        console.log("addCountry: ", req.body);
        if(req.body.Country != null) {
          const pool = await poolPromise
          const result = await pool.request()
          .input('Country', sql.VarChar, req.body.Country)
          .query(queries.addCountry)
          console.log("addCountry result: ", result.recordset[0].Id);
          res.json(result.recordset[0])
        } else {
          res.status(200).json({ success:false, message:__("error.fillDetails", __("module.country")) })
        }
      } catch (error) {
        console.log(error)
        res.status(500).json({ success:false, message:__("error.add", __("module.country")), error })
      }
    }
    async updateCountry(req, res){
      try {
        console.log("updateCountry: ", req.body);
        if(req.body.Id != null && req.body.Country != null) {
        const pool = await poolPromise
          const result = await pool.request()
          .input('Id',sql.SmallInt , req.body.Id)
          .input('Country',sql.VarChar , req.body.Country)
          .query(queries.updateCountry)
          console.log("updateCountry result: ", req.body.Id);
          res.json({Id: req.body.Id})
        } else {
          res.status(200).json({ success:false, message:__("error.fillDetails", __("module.country")) })
        }
      } catch (error) {
        console.log(error)
        res.status(500).json({ success:false, message:__("error.update", __("module.country")), error })
      }
    }
    async deleteCountry(req , res){
      try {
          console.log(req);
        if(req.params.Id != null) {
          const pool = await poolPromise
            const result = await pool.request()
            .input('Id',sql.SmallInt , req.params.Id)
            .query(queries.deleteCountry)
            console.log("deleteCountry result: ", req.params.Id);
            res.json({Id: req.params.Id})
          } else {
            res.status(200).json({ success:false, message:__("error.fillDetails", __("module.country")) })
          }
      } catch (error) {
        console.log(error)
        res.status(500).json({ success:false, message:__("error.delete", __("module.country")), error })
      }
    }
    async getCountryPhoneCodes(req , res){
      try {
        res.json({"BD": "880", "BE": "32", "BF": "226", "BG": "359", "BA": "387", "BB": "1-246", "WF": "681", "BL": "590", "BM": "1-441", "BN": "673", "BO": "591", "BH": "973", "BI": "257", "BJ": "229", "BT": "975", "JM": "1-876", "BW": "267", "WS": "685", "BQ": "599", "BR": "55", "BS": "1-242", "JE": "44-1534", "BY": "375", "BZ": "501", "RU": "7", "RW": "250", "RS": "381", "TL": "670", "RE": "262", "TM": "993", "TJ": "992", "RO": "40", "TK": "690", "GW": "245", "GU": "1-671", "GT": "502", "GR": "30", "GQ": "240", "GP": "590", "JP": "81", "GY": "592", "GG": "44-1481", "GF": "594", "GE": "995", "GD": "1-473", "GB": "44", "GA": "241", "SV": "503", "GN": "224", "GM": "220", "GL": "299", "GI": "350", "GH": "233", "OM": "968", "TN": "216", "JO": "962", "HR": "385", "HT": "509", "HU": "36", "HK": "852", "HN": "504", "HM": " ", "VE": "58", "PR": "1-787 / 1-939", "PS": "970", "PW": "680", "PT": "351", "SJ": "47", "PY": "595", "IQ": "964", "PA": "507", "PF": "689", "PG": "675", "PE": "51", "PK": "92", "PH": "63", "PN": "870", "PL": "48", "PM": "508", "ZM": "260", "EH": "212", "EE": "372", "EG": "20", "ZA": "27", "EC": "593", "IT": "39", "VN": "84", "SB": "677", "ET": "251", "SO": "252", "ZW": "263", "SA": "966", "ES": "34", "ER": "291", "ME": "382", "MD": "373", "MG": "261", "MF": "590", "MA": "212", "MC": "377", "UZ": "998", "MM": "95", "ML": "223", "MO": "853", "MN": "976", "MH": "692", "MK": "389", "MU": "230", "MT": "356", "MW": "265", "MV": "960", "MQ": "596", "MP": "1-670", "MS": "1-664", "MR": "222", "IM": "44-1624", "UG": "256", "TZ": "255", "MY": "60", "MX": "52", "IL": "972", "FR": "33", "IO": "246", "SH": "290", "FI": "358", "FJ": "679", "FK": "500", "FM": "691", "FO": "298", "NI": "505", "NL": "31", "NO": "47", "NA": "264", "VU": "678", "NC": "687", "NE": "227", "NF": "672", "NG": "234", "NZ": "64", "NP": "977", "NR": "674", "NU": "683", "CK": "682", "CI": "225", "CH": "41", "CO": "57", "CN": "86", "CM": "237", "CL": "56", "CC": "61", "CA": "1", "CG": "242", "CF": "236", "CD": "243", "CZ": "420", "CY": "357", "CX": "61", "CR": "506", "CW": "599", "CV": "238", "CU": "53", "SZ": "268", "SY": "963", "SX": "599", "KG": "996", "KE": "254", "SS": "211", "SR": "597", "KI": "686", "KH": "855", "KN": "1-869", "KM": "269", "ST": "239", "SK": "421", "KR": "82", "SI": "386", "KP": "850", "KW": "965", "SN": "221", "SM": "378", "SL": "232", "SC": "248", "KZ": "7", "KY": "1-345", "SG": "65", "SE": "46", "SD": "249", "DO": "1-809 / 1-829", "DM": "1-767", "DJ": "253", "DK": "45", "VG": "1-284", "DE": "49", "YE": "967", "DZ": "213", "US": "1", "UY": "598", "YT": "262", "UM": "1", "LB": "961", "LC": "1-758", "LA": "856", "TV": "688", "TW": "886", "TT": "1-868", "TR": "90", "LK": "94", "LI": "423", "LV": "371", "TO": "676", "LT": "370", "LU": "352", "LR": "231", "LS": "266", "TH": "66", "TG": "228", "TD": "235", "TC": "1-649", "LY": "218", "VA": "379", "VC": "1-784", "AE": "971", "AD": "376", "AG": "1-268", "AF": "93", "AI": "1-264", "VI": "1-340", "IS": "354", "IR": "98", "AM": "374", "AL": "355", "AO": "244", "AS": "1-684", "AR": "54", "AU": "61", "AT": "43", "AW": "297", "IN": "91", "AX": "358-18", "AZ": "994", "IE": "353", "ID": "62", "UA": "380", "QA": "974", "MZ": "258"})
      } catch (error) {
        console.log(error)
        res.status(500).json({ success:false, message:__("error.retrieveList", __("module.country")), error })
      }
    }
    async getCountryCurrency(req , res){
      try {
        res.json([{"CurrencyCode":"AED","CurrencyName":"United Arab Emirates Dirham","id":1},{"CurrencyCode":"ARS","CurrencyName":"Argentine Peso","id":2},{"CurrencyCode":"AUD","CurrencyName":"Australian Dollar","id":3},{"CurrencyCode":"BDT","CurrencyName":"Bangladeshi Taka","id":4},{"CurrencyCode":"BGN","CurrencyName":"Bulgarian Lev","id":5},{"CurrencyCode":"BHD","CurrencyName":"Bahraini Dinar","id":6},{"CurrencyCode":"BND","CurrencyName":"Brunei Dollar","id":7},{"CurrencyCode":"BOB","CurrencyName":"Bolivian Boliviano","id":8},{"CurrencyCode":"BRL","CurrencyName":"Brazilian Real","id":9},{"CurrencyCode":"BWP","CurrencyName":"Botswanan Pula","id":10},{"CurrencyCode":"BYN","CurrencyName":"Belarusian Ruble","id":11},{"CurrencyCode":"CAD","CurrencyName":"Canadian Dollar","id":12},{"CurrencyCode":"CHF","CurrencyName":"Swiss Franc","id":13},{"CurrencyCode":"CLP","CurrencyName":"Chilean Peso","id":14},{"CurrencyCode":"CNY","CurrencyName":"Chinese Yuan","id":15},{"CurrencyCode":"COP","CurrencyName":"Colombian Peso","id":16},{"CurrencyCode":"CRC","CurrencyName":"Costa Rican Colón","id":17},{"CurrencyCode":"CZK","CurrencyName":"Czech Koruna","id":18},{"CurrencyCode":"DKK","CurrencyName":"Danish Krone","id":19},{"CurrencyCode":"DOP","CurrencyName":"Dominican Peso","id":20},{"CurrencyCode":"DZD","CurrencyName":"Algerian Dinar","id":21},{"CurrencyCode":"EGP","CurrencyName":"Egyptian Pound","id":22},{"CurrencyCode":"EUR","CurrencyName":"Euro","id":23},{"CurrencyCode":"FJD","CurrencyName":"Fijian Dollar","id":24},{"CurrencyCode":"GBP","CurrencyName":"British Pound Sterling","id":25},{"CurrencyCode":"GEL","CurrencyName":"Georgian Lari","id":26},{"CurrencyCode":"GHS","CurrencyName":"Ghanaian Cedi","id":27},{"CurrencyCode":"HKD","CurrencyName":"Hong Kong Dollar","id":28},{"CurrencyCode":"HRK","CurrencyName":"Croatian Kuna","id":29},{"CurrencyCode":"HUF","CurrencyName":"Hungarian Forint","id":30},{"CurrencyCode":"IDR","CurrencyName":"Indonesian Rupiah","id":31},{"CurrencyCode":"ILS","CurrencyName":"Israeli New Sheqel","id":32},{"CurrencyCode":"INR","CurrencyName":"Indian Rupee","id":33},{"CurrencyCode":"IQD","CurrencyName":"Iraqi Dinar","id":34},{"CurrencyCode":"JOD","CurrencyName":"Jordanian Dinar","id":35},{"CurrencyCode":"JPY","CurrencyName":"Japanese Yen","id":36},{"CurrencyCode":"KES","CurrencyName":"Kenyan Shilling","id":37},{"CurrencyCode":"KRW","CurrencyName":"South Korean Won","id":38},{"CurrencyCode":"KWD","CurrencyName":"Kuwaiti Dinar","id":39},{"CurrencyCode":"KZT","CurrencyName":"Kazakhstani Tenge","id":40},{"CurrencyCode":"LBP","CurrencyName":"Lebanese Pound","id":41},{"CurrencyCode":"LKR","CurrencyName":"Sri Lankan Rupee","id":42},{"CurrencyCode":"LTL","CurrencyName":"Lithuanian Litas","id":43},{"CurrencyCode":"MAD","CurrencyName":"Moroccan Dirham","id":44},{"CurrencyCode":"MMK","CurrencyName":"Myanma Kyat","id":45},{"CurrencyCode":"MOP","CurrencyName":"Macanese Pataca","id":46},{"CurrencyCode":"MUR","CurrencyName":"Mauritian Rupee","id":47},{"CurrencyCode":"MXN","CurrencyName":"Mexican Peso","id":48},{"CurrencyCode":"MYR","CurrencyName":"Malaysian Ringgit","id":49},{"CurrencyCode":"NAD","CurrencyName":"Namibian Dollar","id":50},{"CurrencyCode":"NGN","CurrencyName":"Nigerian Naira","id":51},{"CurrencyCode":"NIO","CurrencyName":"Nicaraguan Córdoba","id":52},{"CurrencyCode":"NOK","CurrencyName":"Norwegian Krone","id":53},{"CurrencyCode":"NPR","CurrencyName":"Nepalese Rupee","id":54},{"CurrencyCode":"NZD","CurrencyName":"New Zealand Dollar","id":55},{"CurrencyCode":"OMR","CurrencyName":"Omani Rial","id":56},{"CurrencyCode":"PEN","CurrencyName":"Peruvian Nuevo Sol","id":57},{"CurrencyCode":"PHP","CurrencyName":"Philippine Peso","id":58},{"CurrencyCode":"PKR","CurrencyName":"Pakistani Rupee","id":59},{"CurrencyCode":"PLN","CurrencyName":"Polish Zloty","id":60},{"CurrencyCode":"PYG","CurrencyName":"Paraguayan Guarani","id":61},{"CurrencyCode":"QAR","CurrencyName":"Qatari Rial","id":62},{"CurrencyCode":"RON","CurrencyName":"Romanian Leu","id":63},{"CurrencyCode":"RSD","CurrencyName":"Serbian Dinar","id":64},{"CurrencyCode":"RUB","CurrencyName":"Russian Ruble","id":65},{"CurrencyCode":"SAR","CurrencyName":"Saudi Riyal","id":66},{"CurrencyCode":"SEK","CurrencyName":"Swedish Krona","id":67},{"CurrencyCode":"SGD","CurrencyName":"Singapore Dollar","id":68},{"CurrencyCode":"SVC","CurrencyName":"Salvadoran Colón","id":69},{"CurrencyCode":"THB","CurrencyName":"Thai Baht","id":70},{"CurrencyCode":"TND","CurrencyName":"Tunisian Dinar","id":71},{"CurrencyCode":"TRY","CurrencyName":"Turkish Lira","id":72},{"CurrencyCode":"TWD","CurrencyName":"New Taiwan Dollar","id":73},{"CurrencyCode":"TZS","CurrencyName":"Tanzanian Shilling","id":74},{"CurrencyCode":"UAH","CurrencyName":"Ukrainian Hryvnia","id":75},{"CurrencyCode":"UGX","CurrencyName":"Ugandan Shilling","id":76},{"CurrencyCode":"USD","CurrencyName":"US Dollar","id":77},{"CurrencyCode":"UYU","CurrencyName":"Uruguayan Peso","id":78},{"CurrencyCode":"UZS","CurrencyName":"Uzbekistan Som","id":79},{"CurrencyCode":"VEF","CurrencyName":"Venezuelan Bolívar (2008-2018)","id":80},{"CurrencyCode":"VES","CurrencyName":"Venezuelan Bolívar","id":81},{"CurrencyCode":"VND","CurrencyName":"Vietnamese Dong","id":82},{"CurrencyCode":"XOF","CurrencyName":"CFA Franc BCEAO","id":83},{"CurrencyCode":"ZAR","CurrencyName":"South African Rand","id":84}])
      } catch (error) {
        console.log(error)
        res.status(500).json({ success:false, message:__("error.retrieveList", __("module.country")), error })
      }
    }
}

const controller = new CountryController()
module.exports = controller;