const { sql,poolPromise } = require('../database/db')
const fs = require('fs');
var rawdata = fs.readFileSync('./query/queries.json');
var queries = JSON.parse(rawdata);

class StateController {

    async getState(req , res){
      try {
        const pool = await poolPromise
        const result = await pool.request()
          .query(queries.getState)
          res.status(200).json(result.recordset)
      } catch (error) {
        console.log(error)
        res.status(500).json({ success:false, message:'Unable to retrieve list of state. Please try again.', error })
      }
    }

    async addState(req , res){
      try {
        console.log("addState: ", req.body);
        if(req.body.State != null) {
          const pool = await poolPromise
          const result = await pool.request()
          .input('State', sql.VarChar, req.body.State)
          .query(queries.addState)
          console.log("addState result: ", result.recordset[0].Id);
          res.status(200).json(result.recordset[0])
        } else {
          res.status(200).json({ success:false, message:'Please fill all the details!' })
        }
      } catch (error) {
        console.log(error)
        res.status(500).json({ sucess:false, message:'Unable to add state', error })
      }
    }
    async updateState(req, res){
      try {
        console.log("updateState: ", req.body);
        if(req.body.Id != null && req.body.State != null) {
        const pool = await poolPromise
          const result = await pool.request()
          .input('Id',sql.SmallInt , req.body.Id)
          .input('State',sql.VarChar , req.body.State)
          .query(queries.updateState)
          console.log("updateState result: ", req.body.Id);
          res.status(200).json({Id: req.body.Id})
        } else {
          res.status(200).json({ success:false, message:'Please fill all the details!' })
        }
      } catch (error) {
        console.log(error)
        res.status(500).json({ success:false, message:'Unable to update state', error })
      }
    }
    async deleteState(req , res){
      try {
          console.log(req);
        if(req.params.Id != null) {
          const pool = await poolPromise
            const result = await pool.request()
            .input('Id',sql.SmallInt , req.params.Id)
            .query(queries.deleteState)
            console.log("deleteState result: ", req.params.Id);
            res.status(200).json({Id: req.params.Id})
          } else {
            res.status(200).json({ success:false, message:'Please fill all the details!' })
          }
      } catch (error) {
        console.log(error)
        res.status(500).json({ success:false, message:'Unable to delete state', error })
      }
    }
}

const controller = new StateController()
module.exports = controller;