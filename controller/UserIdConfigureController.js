const { sql, poolPromise } = require('../database/db')
const fs = require('fs')
var rawdata = fs.readFileSync('./query/queries.json')
const bcrypt = require('bcryptjs')
var queries = JSON.parse(rawdata)

class UserIdConfigureController {
  async getUserIdConfigure(req, res) {
    try {
      const pool = await poolPromise
      const result = await pool.request().query(queries.getUserIdConfigure)
      
      res.json(result.recordset)
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }

  async getAdminDetails(req, res) {
    try {
      const adminId = req.params.Id;
      console.log("Get admin " + adminId + " details");
      if (adminId != null) {
        const pool = await poolPromise
        const result = await pool
              .request()
              .input('Id', sql.VarChar, req.params.Id)
              .query(queries.getAdminDetails)
              
        res.status(200).json(result.recordset)
      } else {
        console.log('Admin id is not provided')
      }
    } catch (error) {
      console.log("ERROR : ", error);
      res.status(500).send(error.message);
    }
  }

  async getManagerList(req, res) {
    try {
      var queryStr = queries.getManagerList.join(' ')
      const pool = await poolPromise
      const result = await pool.request().query(queryStr)
      
      // Added by Hakim on 25 Feb 2021 - Start
      // Filter manager
      let managerList = result.recordset
      let managerListFiltered = []
      if (result != null) {
        managerListFiltered = managerList.filter((manager) => {
          if (manager.Email == 'shahru.hassan@skom.com.my' || manager.Email == 'woonping.kay@skom.com.my') {
            return manager
          }
        })
      }
      // Added by Hakim on 25 Feb 2021 - End
      res.json(managerListFiltered)
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }

  async getUserModule(req, res) {
    try {
      const pool = await poolPromise
      const result = await pool.request().query(queries.getUserModule)
      
      res.json(result.recordset)
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }

  async getUserIdConfigureById(req, res) {
    try {
      if (req.params.UserConfigureID != null) {
        const pool = await poolPromise
        const result = await pool
          .request()
          .input('UserConfigureID', sql.SmallInt, req.params.UserConfigureID)
          .query(queries.getUserIdConfigureById)
        
        res.json(result.recordset)
      }
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }

  async addUserIdConfigure(req, res) {
    try {
      console.log('addUserIdConfigure: ', req.body)
      if (
        req.body.constructor === Object &&
        Object.keys(req.body).length === 0
      ) {
        console.log('Object missing')
        res.status(500)
        res.send(error.message)
      } else {
        console.log('Object detected')
        const pool = await poolPromise
        const values = Object.values(req.body)
        console.log('values: ', values)
        var success_val = true
        var insertAccessModuleUser =
          'INSERT INTO [AccessModuleUser] '
        insertAccessModuleUser +=
          '([ModuleID], [UserConfigureID], [Chk], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) '
        insertAccessModuleUser += 'VALUES ('
        // validate useridconfigure data not null
        if (values[0].UserID == null || values[0].UserName == null) {
          success_val = false
        }
        var UserConfigureID = 0
        if (success_val) {
          // Added by Hakim on 3 Feb 2021 - Start
          const salt = await bcrypt.genSalt(10);
          const hashPassword = await bcrypt.hash(values[0].Password, salt)
          // Added by Hakim on 3 Feb 2021 - End

          const result = await pool
            .request()
            .input('UserID', sql.VarChar, values[0].UserID)
            .input('Name', sql.VarChar, values[0].Name)
            .input('FirstName', sql.VarChar, values[0].FirstName)
            .input('MiddleName', sql.VarChar, values[0].MiddleName)
            .input('LastName', sql.VarChar, values[0].LastName)
            .input('UserName', sql.VarChar, values[0].UserName)
            .input('Password', sql.VarChar, hashPassword) // Added by Hakim on 3 Feb 2021
            .input('CreatedBy', sql.VarChar, values[0].CreatedBy) // Updated by Hakim on 3 Feb 2021
            .input('UpdatedBy', sql.VarChar, values[0].CreatedBy) // Updated by Hakim on 3 Feb 2021
            .input('ManagerId', sql.Int, values[0].ManagerId)
            .input('ManagerName', sql.VarChar, values[0].ManagerName)
            .query(queries.addUserIdConfigure)
          UserConfigureID = result.recordset[0].UserConfigureID
        } else {
          res.send('All fields are required!')
        }

        var queries_insertAccessModuleUser = insertAccessModuleUser
        if (UserConfigureID == null) {
          success_val = false
        }

        for (var i = 0; i < values.length; i++) {
          if (
            values[i].ModuleID == null ||
            UserConfigureID == null ||
            values[i].Chk == null
          ) {
            success_val = false
            break
          }
          queries_insertAccessModuleUser += "'" + values[i].ModuleID + "', "
          queries_insertAccessModuleUser += "'" + UserConfigureID + "', "
          queries_insertAccessModuleUser += "'" + values[i].Chk + "', "
          queries_insertAccessModuleUser += 'NULL, NULL, NULL, NULL)'
          if (i !== values.length - 1) {
            queries_insertAccessModuleUser += ';'
            queries_insertAccessModuleUser += insertAccessModuleUser
          }
        }
        queries_insertAccessModuleUser += ';'
        queries_insertAccessModuleUser += 'SELECT SCOPE_IDENTITY() AS [Id];'
        if (success_val) {
          console.log(
            'queries_insertAccessModuleUser: ',
            queries_insertAccessModuleUser
          )
          const result = await pool
            .request()
            .query(queries_insertAccessModuleUser)
          res.json({ Id: UserConfigureID })
        } else {
          // res.send('All fields are required!')
        }
      }
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }

  async updateUserIdConfigure(req, res) {
    try {
      // console.log('updateUserIdConfigure: ', req.body)
      // console.log(
      //   'updateUserIdConfigure - req.params.UserConfigureID: ',
      //   req.params.UserConfigureID
      // )
      if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
        console.log('Object missing')
        res.status(500)
        res.send(error.message)
      } else {
        console.log('Object detected')
        console.log('req.params.UserConfigureID: ', req.params.UserConfigureID)
        const pool = await poolPromise
        /*const result = await pool.request()
          .input('UserConfigureID', sql.SmallInt, parseInt(req.params.UserConfigureID))
          .query(queries.getUserIdConfigureById)
          
        if(result.recordset == null)
        {
          res.status(500)
          res.send("User Not Found!")
        }*/
        const values = Object.values(req.body)
        //console.log('values: ', values)
        var success_val = true
        var updateAccessModuleUser = 'UPDATE [AccessModuleUser] SET '
        var updateQuery = '';
        var insertAccessModuleUser = 'INSERT INTO [AccessModuleUser] ([ModuleID], [UserConfigureID], [Chk]) VALUES '
        var updateUserIdConfigureUser = 'UPDATE [UserConfigure] SET '
        var queries_updateAccessModuleUser = updateQuery
        if (req.params.UserConfigureID == null) {
          success_val = false
        }

        for (var i = 0; i < values.length; i++) {
          if (values[i].ModuleID == null || values[i].UserConfigureID == null || values[i].Chk == null) {
            success_val = false
            break
          }

          const resultAccessModuleUser = await pool
            .request()
            .input('ModuleID', sql.VarChar, values[i].ModuleID)
            .input('UserConfigureID', sql.VarChar, values[i].UserConfigureID)
            .query(queries.getAccessModuleUser)

          if (resultAccessModuleUser.recordset.length) {
            // record exists so update the permission only
            updateQuery += updateAccessModuleUser +"[Chk] = '" + values[i].Chk + "' "
            updateQuery += "WHERE [UserConfigureID] = '" + values[i].UserConfigureID + "'"
            updateQuery += ' AND [ModuleID] = ' + values[i].ModuleID +';'
          } else {
            // record not exists so need to create the permission
            updateQuery += insertAccessModuleUser +"(" + values[i].ModuleID + ", "
            updateQuery += values[i].UserConfigureID + ", "
            updateQuery += "'" + values[i].Chk + "'); SELECT SCOPE_IDENTITY() AS [Id];"
          }          
        }

        queries_updateAccessModuleUser += updateQuery
        if (values[0].UserID == null || values[0].UserName == null) {
          success_val = false
        }

        updateUserIdConfigureUser += "[UserID] = '" + values[0].UserID + "', "
        // updateUserIdConfigureUser += "[Name] = '" + values[0].Name + "', "
        updateUserIdConfigureUser +=
          "[UserName] = '" + values[0].UserName + "', "
        updateUserIdConfigureUser +=
          "[FirstName] = '" + values[0].FirstName + "', "
        updateUserIdConfigureUser +=
          "[MiddleName] = '" + values[0].MiddleName + "', "
        updateUserIdConfigureUser +=
          "[LastName] = '" + values[0].LastName + "', "
        updateUserIdConfigureUser +=
          "[ManagerId] = '" + (values[0].ManagerId ? values[0].ManagerId : "") + "', "
        updateUserIdConfigureUser +=
          "[ManagerName] = '" + (values[0].ManagerName ? values[0].ManagerName : "") + "', "
        updateUserIdConfigureUser +=
          "[UpdatedBy] = '" + values[0].UpdatedBy + "' " // Added by Hakim on 3 Feb 2021
        updateUserIdConfigureUser += 'WHERE [Id] = ' + values[0].UserConfigureID
        queries_updateAccessModuleUser += updateUserIdConfigureUser
        queries_updateAccessModuleUser += '; '

        if (success_val) {
          const result = await pool
            .request()
            .query(queries_updateAccessModuleUser)
          res.json({ UserConfigureID: req.params.UserConfigureID })
        } else {
          res.send('All fields are required!')
        }
      }
    } catch (error) {
	  console.log(error)
      res.status(500)
      res.send(error.message)
    }
  }

  // Added by Hakim on 3 Feb 2021 - Start
  async updateUserIdConfigurePassword(req, res) {
    try {
      console.log('updateUserIdConfigurePassword: ', req.body)
      console.log(
        'updateUserIdConfigurePassword - req.params.UserConfigureID: ',
        req.params.UserConfigureID
      )
      if (
        req.params.UserConfigureID == null
      ) {
        console.log('Object missing')
        res.status(500)
        res.send(error.message)
      } else {
        console.log('Object detected')
        const pool = await poolPromise
        var success_val = true

        const salt = await bcrypt.genSalt(10);
        const hashPassword = await bcrypt.hash(req.body.Password, salt)

        var updateUserIdConfigureUser =
          'UPDATE [UserConfigure] SET '
        updateUserIdConfigureUser +=
          "[Password] = '" + hashPassword + "' "
        updateUserIdConfigureUser += 'WHERE [Id] = ' + req.params.UserConfigureID + ';'

        if (success_val) {
          console.log(
            'queries_updateUserIdConfigurePasswordUser: ',
            updateUserIdConfigureUser
          )
          const result = await pool
            .request()
            .query(updateUserIdConfigureUser)
          res.json({ UserConfigureID: req.params.UserConfigureID })
        } else {
          res.send('All fields are required!')
        }
      }
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }
  // Added by Hakim on 3 Feb 2021 - End

  async deleteUserIdConfigure(req, res) {
    try {
      console.log('req.params: ', req.params)
      if (req.params.UserConfigureID != null) {
        const pool = await poolPromise
        const result = await pool
          .request()
          .input('UserConfigureID', sql.VarChar, req.params.UserConfigureID)
          .query(queries.deleteUserIdConfigure)
        console.log(
          'deleteUserIdConfigure result: ',
          req.params.UserConfigureID
        )
        res.json({ UserConfigureID: req.params.UserConfigureID })
      } else {
        res.send('Please fill all the details!')
      }
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }
}

const controller = new UserIdConfigureController()
module.exports = controller
