const { sql, poolPromise } = require('../database/db')
const fs = require('fs');
var path = require('path');
var moment = require('moment');
var rawdata = fs.readFileSync('./query/queries.json');
var queries = JSON.parse(rawdata);
var rawdata2 = fs.readFileSync('./query/qApplicantApply.json');
var qApplicantApply = JSON.parse(rawdata2);
const generationCtlr = require('../controller/GenerationController')
const ejs = require("ejs");
const standByRates = [
    { Id: 1, Rate: '0.5x daily rate' },
    { Id: 2, Rate: '1.0x daily rate' }
]

class EmailController {
    async test(req, res) {
        let email = req.body.email;
        let name = 'Test Name';
        let filename = '';
        let filepath = '';
        let emailData = {
            Email: email,
            Name:'Hakim',
            Identification:'NRIC : 911111101111',
            OfferPosition:'Oiler',
            NameofVessel:'NK Naval',
            ContractPeriodFrom: '11/04/2021',
            ContractPeriodTo: '10/07/2021',
            DailyRate: '550 / day',
            StandbyAllowance: '0',
            AdminName: 'Ali Wahab',
            AdminEmail: 'ali@grr.la'
        }

        if (email == null || email == '') {
            return res.status(500).json({ success:false, message:'Email is not provide' });
        }

        ejs.renderFile(path.resolve('./Templates/JobOffer.ejs'), emailData, function (err, data) {
            if (err) {
                console.log(err);
            } else {
                var nodemailer = require('nodemailer')
                let transporter = nodemailer.createTransport({
                    pool: false,
                    host: "smtp.office365.com",
                    port: 587,
                    secureConnection: false,
                    tls: { ciphers: 'SSLv3' },
                    auth: {
                        user: process.env.EMAIL_ADDRESS,
                        pass: process.env.EMAIL_PASSWORD
                    }
                });

                transporter.verify(function (error, success) {
                    if (error) {
                        console.log(error)
                    } else {
                        console.log("Server ready to send email")
                    }
                })
                
                transporter.sendMail({
                    from: process.env.EMAIL_ADDRESS,
                    to: email,
                    subject: '[TEST] SKOM eCrew Job Portal',
                    html: data
                }, 
                async function(err, info) {
                    console.log("Sent email to " + email)
                    if (!err) {
                        res.status(200).json({ success:true, message:'Successfully sent email to ' + email })
                    } else {
                        console.log(err)
                        res.status(200).json({ success:false, message:'Failed to send email to ' + email })
                    }
                })
            }
        });
    }
    async sendApprovedApplication(req, res) {
        const pool = await poolPromise
        const ApplicationId = req.body.Id

        try {
            console.log('Starting send email for application ' + ApplicationId)
            const result = await pool
            .request()
            .input('Id', sql.SmallInt, ApplicationId)
            .query(qApplicantApply.v_getById)
            if (result.recordset == null) {
                let err = { success:false, message:'Application not found' }
                throw err
            } 
            let application = result.recordset[0];

            const adminDetails = await pool
                .request()
                .input('UserName', sql.VarChar, application.ConfirmByName)
                .query(queries.getLoginAdmin2)

            let AdminName = adminDetails.recordset[0].UserName
            let AdminLastName = adminDetails.recordset[0].LastName
            let AdminEmail = adminDetails.recordset[0].LoginEmail

            var fullname = '';
            if (application.Name != null && application.Name != '') {
                fullname = application.Name + ' ';
            }
            if (application.MiddleName != null && application.MiddleName != '') {
                fullname = fullname + application.MiddleName + ' ';
            }
            if (application.LastName != null && application.LastName != '') {
                fullname = fullname + application.LastName;
            }

            var identityType = 'NRIC';
            var identification = '';
            if (application.IC == null || application.IC == '') {
                identityType = 'Passport';
                identification = application.Passport ? application.Passport : '';
            } else {
                identification = application.IC ? application.IC : '';
            }

            var dailyRate = application.Currency + ' ' + (application.DailyRate ? application.DailyRate : '0') + ' / day';
            var allowance = application.Currency + ' ' + (application.Allowance ? application.Allowance : '0') + ' / day';
            var allowanceRemark = application.TypesofAllowance ? '(' + application.TypesofAllowance + ')' : '';
            var allowanceStandby = application.Currency + ' ' + (application.StandbyAllowance ? application.StandbyAllowance : '0') + ' / day';
            var allowanceStandbyRemark = application.StandbyRate ? '(' + (standByRates.filter(rate => { return rate.Id == application.StandbyRate }))[0].Rate + ')' : '';
            var allowanceOther = application.Currency + ' ' + (application.OtherAllowance ? application.OtherAllowance : '0') + ' / day';
            var allowanceOtherRemark = '(' + (application.AllowanceRemarks ? application.AllowanceRemarks : '0') + ')';

            let startDate = ''
            if (Date(application.ContractPeriodFrom)) {
                startDate = moment.utc(application.ContractPeriodFrom).format('DD/MM/YYYY')
            }

            let endDate = ''
            if (Date(application.ContractPeriodTo)) {
                endDate = moment.utc(application.ContractPeriodTo).format('DD/MM/YYYY')
            }

            let adminFullname = AdminName;
            if (AdminLastName != null) {
                adminFullname = adminFullname + ' ' + AdminLastName;
            }


            let email = application.LoginEmail;
            let name = '';
            let filename = '';
            let filepath = '';
            let emailData = {
                ApplicationID: ApplicationId,
                Email: email,
                Name: fullname,
                Identification: identityType + ' : ' + identification,
                OfferPosition:application.OfferPosition,
                NameofVessel:application.NameofVessel,
                ContractPeriodFrom: startDate,
                ContractPeriodTo: endDate,
                DailyRate: dailyRate,
                Allowance: allowance,
                AllowanceRemark: allowanceRemark,
                StandbyAllowance: allowanceStandby,
                StandbyAllowanceRemark: allowanceStandbyRemark,
                OtherAllowance: allowanceOther,
                OtherAllowanceRemark: allowanceOtherRemark,
                AdminName: adminFullname,
                AdminEmail: AdminEmail
            }

            if (email == null || email == '') {
                return res.status(500).json({ success:false, message:'Email is not provide' });
            }

            ejs.renderFile(path.resolve('./Templates/JobOffer.ejs'), emailData, function (err, data) {
                if (err) {
                    console.log(err);
                } else {
                    var nodemailer = require('nodemailer')
                    let transporter = nodemailer.createTransport({
                        pool: false,
                        host: "smtp.office365.com",
                        port: 587,
                        secureConnection: false,
                        tls: { ciphers: 'SSLv3' },
                        auth: {
                            user: process.env.EMAIL_ADDRESS,
                            pass: process.env.EMAIL_PASSWORD
                        }
                    });

                    transporter.verify(function (error, success) {
                        if (error) {
                            console.log(error)
                        } else {
                            console.log("Server ready to send email")
                        }
                    })
                    
                    transporter.sendMail({
                        from: process.env.EMAIL_ADDRESS,
                        to: email,
                        subject: '[TEST] SKOM eCrew Job Portal',
                        html: data
                    }, 
                    async function(err, info) {
                        console.log("Sent email to " + email)
                        if (!err) {
                            res.status(200).json({ success:true, message:'Successfully sent email to ' + email })
                        } else {
                            console.log(err)
                            res.status(200).json({ success:false, message:'Failed to send email to ' + email })
                        }
                    })
                }
            });
        } catch(error) {
            console.log(error);
            res.status(200).json({ success:false, message:'Failed to send email to ' + email, error:error });
        }
    }
    async sendTerminatedApplication(req, res) {
        const pool = await poolPromise
        const ApplicationId = req.body.Id

        try {
            console.log('Starting send email for application ' + ApplicationId)
            const result = await pool
            .request()
            .input('Id', sql.SmallInt, ApplicationId)
            .query(qApplicantApply.v_getById)
            if (result.recordset == null) {
                let err = { success:false, message:'Application not found' }
                throw err
            } 
            let application = result.recordset[0];

            const adminDetails = await pool
                .request()
                .input('UserName', sql.VarChar, application.ConfirmByName)
                .query(queries.getLoginAdmin2)

            let AdminName = adminDetails.recordset[0].UserName
            let AdminLastName = adminDetails.recordset[0].LastName
            let AdminEmail = adminDetails.recordset[0].LoginEmail

            var fullname = '';
            if (application.Name != null && application.Name != '') {
                fullname = application.Name + ' ';
            }
            if (application.MiddleName != null && application.MiddleName != '') {
                fullname = fullname + application.MiddleName + ' ';
            }
            if (application.LastName != null && application.LastName != '') {
                fullname = fullname + application.LastName;
            }

            var identityType = 'NRIC';
            var identification = '';
            if (application.IC == null || application.IC == '') {
                identityType = 'Passport';
                identification = application.Passport ? application.Passport : '';
            } else {
                identification = application.IC ? application.IC : '';
            }

            var dailyRate = application.Currency + ' ' + (application.DailyRate ? application.DailyRate : '0') + ' / day';
            var allowance = application.Currency + ' ' + (application.Allowance ? application.Allowance : '0') + ' / day';
            var allowanceRemark = application.TypesofAllowance ? '(' + application.TypesofAllowance + ')' : '';
            var allowanceStandby = application.Currency + ' ' + (application.StandbyAllowance ? application.StandbyAllowance : '0') + ' / day';
            var allowanceStandbyRemark = application.StandbyRate ? '(' + (standByRates.filter(rate => { return rate.Id == application.StandbyRate }))[0].Rate + ')' : '';
            var allowanceOther = application.Currency + ' ' + (application.OtherAllowance ? application.OtherAllowance : '0') + ' / day';
            var allowanceOtherRemark = '(' + (application.AllowanceRemarks ? application.AllowanceRemarks : '0') + ')';

            let startDate = ''
            if (Date(application.ContractPeriodFrom)) {
                startDate = moment.utc(application.ContractPeriodFrom).format('DD/MM/YYYY')
            }

            let endDate = ''
            if (Date(application.ContractPeriodTo)) {
                endDate = moment.utc(application.ContractPeriodTo).format('DD/MM/YYYY')
            }

            let adminFullname = AdminName;
            if (AdminLastName != null) {
                adminFullname = adminFullname + ' ' + AdminLastName;
            }


            let email = application.LoginEmail;
            let name = '';
            let filename = '';
            let filepath = '';
            let emailData = {
                ApplicationID: ApplicationId,
                Email: email,
                Name: fullname,
                Identification: identityType + ' : ' + identification,
                OfferPosition:application.OfferPosition,
                NameofVessel:application.NameofVessel,
                ContractPeriodFrom: startDate,
                ContractPeriodTo: endDate,
                DailyRate: dailyRate,
                Allowance: allowance,
                AllowanceRemark: allowanceRemark,
                StandbyAllowance: allowanceStandby,
                StandbyAllowanceRemark: allowanceStandbyRemark,
                OtherAllowance: allowanceOther,
                OtherAllowanceRemark: allowanceOtherRemark,
                AdminName: adminFullname,
                AdminEmail: AdminEmail
            }

            if (email == null || email == '') {
                return res.status(500).json({ success:false, message:'Email is not provide' });
            }

            ejs.renderFile(path.resolve('./Templates/JobTerminate.ejs'), emailData, function (err, data) {
                if (err) {
                    console.log(err);
                } else {
                    var nodemailer = require('nodemailer')
                    let transporter = nodemailer.createTransport({
                        pool: false,
                        host: "smtp.office365.com",
                        port: 587,
                        secureConnection: false,
                        tls: { ciphers: 'SSLv3' },
                        auth: {
                            user: process.env.EMAIL_ADDRESS,
                            pass: process.env.EMAIL_PASSWORD
                        }
                    });

                    transporter.verify(function (error, success) {
                        if (error) {
                            console.log(error)
                        } else {
                            console.log("Server ready to send email")
                        }
                    })
                    
                    transporter.sendMail({
                        from: process.env.EMAIL_ADDRESS,
                        to: email,
                        subject: '[TEST] SKOM eCrew Job Portal',
                        html: data
                    }, 
                    async function(err, info) {
                        console.log("Sent email to " + email)
                        if (!err) {
                            res.status(200).json({ success:true, message:'Successfully sent email to ' + email })
                        } else {
                            console.log(err)
                            res.status(200).json({ success:false, message:'Failed to send email to ' + email })
                        }
                    })
                }
            });
        } catch(error) {
            console.log(error);
            res.status(200).json({ success:false, message:'Failed to send email to ' + email, error:error });
        }
    }
    async sendApprovedApplicationOld(req, res) {

        const pool = await poolPromise
        const ApplicationId = req.body.Id

        try {
			console.log('Starting send email for application ' + ApplicationId)
            const result = await pool
                .request()
                .input('Id', sql.SmallInt, ApplicationId)
                .query(queries.getApplicantById)

            if (result.recordset == null) {
                let err = { success:false, message:'Application not found' }
                throw err
            } 
            let application = result.recordset[0];

            if (application.FileSEA == '' || application.FileSEA == null) {
                // Checking if SEA file existed
                let resultSearch = await generationCtlr.verifyFileExist(ApplicationId);
                if (!resultSearch.SEA) {
                    let err = { success:false, message:'Unable to send email due to SEA document is not available. Please proceed to "Report" -> "AFE/CV/SEA" to generate the document' }
                    throw err
                }
                application.FileSEA = resultSearch.SEA;
            }

            if (application.ConfirmByName == '' || application.ConfirmByName == null) {
                let err = { success:false, message:'Application have not been approved' }
                throw err
            }

            const adminDetails = await pool
                .request()
                .input('UserName', sql.VarChar, application.ConfirmByName)
                .query(queries.getLoginAdmin2)

            let AdminName = adminDetails.recordset[0].UserName
            let AdminLastName = adminDetails.recordset[0].LastName
            let AdminEmail = adminDetails.recordset[0].LoginEmail

            console.log('AdminData')
            console.log(AdminName, AdminEmail)

            const filename = application.FileSEA
            const filepath = path.resolve('../dist/assets/UserDoc/' + filename)
            
            // const filename = application.FileSEA.replace('.docx', '.pdf')
            // const outputPath = path.resolve('../dist/assets/UserDoc/' + filename)

            // // Convert doc to pdf - Start
            // const { PDFDocument } = require("pdf-lib")
            // const { wordToPdf } = require('node-docto')
            // var source = path.resolve('../dist/assets/UserDoc/' + application.FileSEA)
            // var destination = outputPath

            // let pathToPDF = outputPath /*converted file*/
            // let pathToImage = "../dist/assets/images/SKOM-logo.png" /*watermark pmg image*/

            // //check the pdf file existing
            // fs.stat(pathToPDF, async (exists) => {
            //     if (exists == null) {
			// 		console.log("PDF file exist");
			// 		return true;
            //     } else if (exists.code === "ENOENT") {
			// 		console.log("PDF file not exist");
			// 		console.log("path:" + pathToPDF);

            //         await new Promise((resolve, reject) => {
            //             /*convert docx to pdf (start)*/
            //             wordToPdf(source, destination)
            //             .then(stdout => {
            //                 console.log("Successfully generate SEA pdf")
            //                 return resolve()
            //             });
            //             /*convert docx to pdf (end)*/
            //         })

			// 		return false;
            //     }
            // });

            // //load pdf file
            // const pdfDoc = await PDFDocument.load(fs.readFileSync(pathToPDF));
            // //load watermark image
            // const img = await pdfDoc.embedPng(fs.readFileSync(pathToImage));

            // /* watermark placement (no need change) */
            // // const imagePage = pdfDoc.insertPage(0);
            // for (let i = 0; i < pdfDoc.getPageCount(); i++) {
            //     let imagePage = "";
            //     imagePage = pdfDoc.getPage(i);
            //     let xx = imagePage.getWidth();
            //     let yy = imagePage.getHeight();
            //     imagePage.drawImage(img, {
            //     x: imagePage.getWidth() / 2 - img.width / 2,
            //     y: imagePage.getHeight() / 2 - img.height / 2,
            //     width: img.width,
            //     height: img.height,
            //     opacity: 0.2
            //     });
            // }
            // /* watermark placement (no need change) */

            // //write pdf file
            // const pdfBytes = await pdfDoc.save();
            // const newFilePath = `../dist/assets/UserDoc/${path.basename(pathToPDF, ".pdf")}-result.pdf`;
            // fs.writeFileSync(newFilePath, pdfBytes);
            // Convert doc to pdf - End
            
            let htmlText =
            '<strong>Dear <strong>' +
            (application.Name ? application.Name : '') +
            '<br />' +
            '<strong>Thank you for your application.<strong>' +
            '<br />' +
            '<strong>We are pleased to make the following offer of employment.<strong>' +
            '<br />' +
            '· Rank : ' +
            (application.OfferPosition ? application.OfferPosition : '') +
            '<br />' +
            '· Vessel Name : ' +
            (application.NameofVessel ? application.NameofVessel : '') +
            '<br />' +
            '· Daily Rate : ' +
            (application.DailyRate ? application.DailyRate : '') +
            '<br />' +
            '· Standby Allowance : ' +
            (application.StandbyAllowance ? application.StandbyAllowance : '') +
            '<br />' +
            '· Other Allowance : ' +
            (application.OtherAllowance ? application.OtherAllowance : '') +
            '<br />' +
            '· Contract Period : ' +
            (application.ContractPeriodFromInMth ? application.ContractPeriodFromInMth : '') +
            '	/month – this may subject to your final acceptance and sign on date.' +
            '<br />' +
            'Please acknowledge your acceptance of the above offer and email signed SEA to ' +
            (application.LoginEmail ? application.LoginEmail : '') +
            '<br />' +
            'Should you need further clarification, please contact ' +
            (AdminName ? AdminName : '') +
            ' ' +
            (AdminLastName ? AdminLastName : '') +
            ' at ' +
            (AdminEmail ? AdminEmail : '') +
            '<br />' +
            'SKOM Sdn. Bhd.' +
            '<br />' +
            'This is a computer generated message and no signature is required.'
            
            var nodemailer = require('nodemailer')
            let transporter = nodemailer.createTransport({
                pool: false,
                host: "smtp.office365.com",
                port: 587,
                secureConnection: false,
                tls: { ciphers: 'SSLv3' },
                auth: {
                    user: process.env.EMAIL_ADDRESS,
                    pass: process.env.EMAIL_PASSWORD
                }
            });

            // let transporter = nodemailer.createTransport({
            //     sendmail: true,
            //     newline: 'windows',
            //     path: '/usr/lib/sendmail'
            // })

            transporter.verify(function (error, success) {
                if (error) {
					console.log(error)
                } else {
					console.log("Server ready to send email")
                }
            })
            
            transporter.sendMail({
                from: process.env.EMAIL_ADDRESS,
                to: application.LoginEmail,
                subject: '[TEST] SKOM eCrew Job Portal',
                html: htmlText,
                    attachments: [
                    {
                        filename: filename,
                        content: fs.createReadStream(filepath)
                    }
                ]
            }, 
            async function(err, info) {
                console.log("Sent email to " + application.LoginEmail)
                if (!err) {
                    await pool
                        .request()
                        .input('Id', sql.SmallInt, ApplicationId)
                        .query(qApplicantApply.UpdateSendEmailCountById)

                    res.status(200).json({ success:true, message:'Successfully sent email to ' + application.LoginEmail })
                } else {
                    console.log(err)
                    res.status(200).json({ success:false, message:'Failed to send email to ' + application.LoginEmail })
                }
            })

        } catch(err) {
			console.log(err)
            res.status(500).json(err)
        }
    }
    async sendAcceptedApplication(req, res) {
        const pool = await poolPromise
        const ApplicationId = req.body.Id

        try {
            console.log('Starting send email for acceptance application ' + ApplicationId)
            const result = await pool
            .request()
            .input('Id', sql.SmallInt, ApplicationId)
            .query(qApplicantApply.v_getById)
            if (result.recordset == null) {
                let err = { success:false, message:'Application not found' }
                throw err
            } 
            let application = result.recordset[0];

            const adminDetails = await pool
                .request()
                .input('UserName', sql.VarChar, application.ConfirmByName)
                .query(queries.getLoginAdmin2)

            let AdminName = adminDetails.recordset[0].UserName
            let AdminLastName = adminDetails.recordset[0].LastName
            let AdminEmail = adminDetails.recordset[0].LoginEmail

            var fullname = '';
            if (application.Name != null && application.Name != '') {
                fullname = application.Name + ' ';
            }
            if (application.MiddleName != null && application.MiddleName != '') {
                fullname = fullname + application.MiddleName + ' ';
            }
            if (application.LastName != null && application.LastName != '') {
                fullname = fullname + application.LastName;
            }

            var identityType = 'NRIC';
            var identification = '';
            if (application.IC == null || application.IC == '') {
                identityType = 'Passport';
                identification = application.Passport ? application.Passport : '';
            } else {
                identification = application.IC ? application.IC : '';
            }

            var dailyRate = application.Currency + ' ' + (application.DailyRate ? application.DailyRate : '0') + ' / day';
            var allowance = application.Currency + ' ' + (application.Allowance ? application.Allowance : '0') + ' / day';
            var allowanceRemark = application.TypesofAllowance ? '(' + application.TypesofAllowance + ')' : '';
            var allowanceStandby = application.Currency + ' ' + (application.StandbyAllowance ? application.StandbyAllowance : '0') + ' / day';
            var allowanceStandbyRemark = application.StandbyRate ? '(' + (standByRates.filter(rate => { return rate.Id == application.StandbyRate }))[0].Rate + ')' : '';
            var allowanceOther = application.Currency + ' ' + (application.OtherAllowance ? application.OtherAllowance : '0') + ' / day';
            var allowanceOtherRemark = '(' + (application.AllowanceRemarks ? application.AllowanceRemarks : '0') + ')';

            let startDate = ''
            if (Date(application.ContractPeriodFrom)) {
                startDate = moment.utc(application.ContractPeriodFrom).format('DD/MM/YYYY')
            }

            let endDate = ''
            if (Date(application.ContractPeriodTo)) {
                endDate = moment.utc(application.ContractPeriodTo).format('DD/MM/YYYY')
            }

            let adminFullname = AdminName;
            if (AdminLastName != null) {
                adminFullname = adminFullname + ' ' + AdminLastName;
            }


            let email = application.LoginEmail;
            let name = '';
            let filename = '';
            let filepath = '';
            let emailData = {
                ApplicationID: ApplicationId,
                Email: email,
                Name: fullname,
                Identification: identityType + ' : ' + identification,
                OfferPosition:application.OfferPosition,
                NameofVessel:application.NameofVessel,
                ContractPeriodFrom: startDate,
                ContractPeriodTo: endDate,
                DailyRate: dailyRate,
                Allowance: allowance,
                AllowanceRemark: allowanceRemark,
                StandbyAllowance: allowanceStandby,
                StandbyAllowanceRemark: allowanceStandbyRemark,
                OtherAllowance: allowanceOther,
                OtherAllowanceRemark: allowanceOtherRemark,
                AdminName: adminFullname,
                AdminEmail: AdminEmail
            }

            if (email == null || email == '') {
                return res.status(500).json({ success:false, message:'Email is not provide' });
            }

            ejs.renderFile(path.resolve('./Templates/JobAccept.ejs'), emailData, function (err, data) {
                if (err) {
                    console.log(err);
                } else {
                    var nodemailer = require('nodemailer')
                    let transporter = nodemailer.createTransport({
                        pool: false,
                        host: "smtp.office365.com",
                        port: 587,
                        secureConnection: false,
                        tls: { ciphers: 'SSLv3' },
                        auth: {
                            user: process.env.EMAIL_ADDRESS,
                            pass: process.env.EMAIL_PASSWORD
                        }
                    });

                    transporter.verify(function (error, success) {
                        if (error) {
                            console.log(error)
                        } else {
                            console.log("Server ready to send email")
                        }
                    })
                    
                    transporter.sendMail({
                        from: process.env.EMAIL_ADDRESS,
                        to: email,
                        cc: "crew@skom.com.my",
                        subject: '[TEST] SKOM eCrew Job Portal',
                        html: data
                    }, 
                    async function(err, info) {
                        console.log("Sent email to " + email)
                        if (!err) {
                            res.status(200).json({ success:true, message:'Successfully sent email to ' + email })
                        } else {
                            console.log(err)
                            res.status(200).json({ success:false, message:'Failed to send email to ' + email })
                        }
                    })
                }
            });
        } catch(error) {
            console.log(error);
            res.status(200).json({ success:false, message:'Failed to send email to ' + email, error:error });
        }
    }
}

const controller = new EmailController()
module.exports = controller;