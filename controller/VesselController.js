const { sql,poolPromise } = require('../database/db')
const fs = require('fs');
var rawdata = fs.readFileSync('./query/queries.json');
var queries = JSON.parse(rawdata);

class VesselController {

    async getVesselType(req , res){
      try {
        console.log("Get Vessel!")
        const pool = await poolPromise
        const result = await pool.request()
          .query(queries.getVesselType)          
          res.status(200).json(result.recordset)
      } catch (error) {
        console.log(error)
        res.status(500).json({ success:false, message:'Unable to retrieve list of vessel. Please try again.', error })
      }
    }

    async addVessel(req , res){
      try {
        console.log("addVessel: ", req.body);
        if(req.body.VesselName != null && req.body.VesselType != null) {
          const pool = await poolPromise
          const result = await pool.request()
          .input('VesselName', sql.VarChar, req.body.VesselName)
          .input('VesselType', sql.VarChar, req.body.VesselType)
          .query(queries.addVessel)
          console.log("addVessel result: ", result.recordset[0].Id);
          res.status(200).json(result.recordset[0])
        } else {
          res.status(200).josn({ success:false, message:'All fields are required!' })
        }
      } catch (error) {
        res.status(500).json({ success:false, message:'Unable to add vessel. Please try again later.', error })
      }
    }
    async updateVessel(req, res){
      try {
        console.log("updateVessel: ", req.body);
        if(req.body.Id != null && req.body.VesselName != null && req.body.VesselType != null) {
        const pool = await poolPromise
          const result = await pool.request()
          .input('Id',sql.SmallInt , req.body.Id)
          .input('VesselName',sql.VarChar , req.body.VesselName)
          .input('VesselType',sql.VarChar , req.body.VesselType)
          //.input('UpdatedBy',sql.VarChar , "Kevin Ng")
          .query(queries.updateVessel)
          console.log("updateVessel result: ", req.body.Id);
          res.status(200).json({ success:true, message:'Successfully update vessel', Id: req.body.Id })
        } else {
          res.status(200).josn({ success:false, message:'All fields are required!' })
        }
      } catch (error) {
        console.log(error)
        res.status(500).json({ success:false, message:'Unable to update vessel. Please try again.', error })
      }
    }
    async deleteVessel(req , res){
      try {
        if(req.params.Id != null) {
          const pool = await poolPromise
            const result = await pool.request()
            .input('Id',sql.SmallInt , req.params.Id)
            .query(queries.deleteVessel)
            console.log("deleteVessel result: ", req.params.Id);
            res.status(200).json({ success:true, message:'Successfully delete vessel', Id: req.params.Id})
          } else {
            res.status(200).json({ success:false, message:'Please fill all the details!' })
          }
      } catch (error) {
        console.log(error)
        res.status(500).json({ success:false, message:'Unable to delete vessel. Please try again.', error })
      }
    }
}

const controller = new VesselController()
module.exports = controller;