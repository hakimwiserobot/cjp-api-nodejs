const { sql, poolPromise } = require('../database/db')
const fs = require('fs')
var rawApplicantQuery = fs.readFileSync('./query/qApplicant.json')
var rawUserConfigureQuery = fs.readFileSync('./query/qUserConfigure.json')
var qApplicant = JSON.parse(rawApplicantQuery)
var qUserConfigure = JSON.parse(rawUserConfigureQuery)
const jwt = require("jsonwebtoken");
const bcrypt = require('bcryptjs')
var moment = require('moment');
const tokenKey = process.env.JWT_SECRET;

class AuthController {
  async refreshToken(token) {
    return new Promise((resolve, reject) => {
        try {
            if (token) {
                jwt.verify(token, tokenKey, async (err, decoded) => {
                    // if (err) { 
                    //     if (err.name === 'TokenExpiredError') {
                    //         throw { error: "TokenExpiredError" };
                    //     } else {
                    //         throw err;
                    //     }
                    // }
        
                    // Create new token
                    const isAdmin = decoded.role == 'admin' ? true : false;
                    const token = await this.generateTokenForUser(decoded.id, isAdmin);
        
                    resolve(token);
                });
            } else {
                throw "Token is not provided"
            }
         
        } catch (error) {
            console.log("ERROR : ", error)
            resolve(null);
        }
    })
  }

  async generateTokenForUser(userId, isAdmin) {

    try {

        let queryUser = '';

        if (isAdmin) {
            console.log('Generate token for admin');
            queryUser = qUserConfigure.getById;
        } else {
            console.log('Generate token for crew');
            queryUser = qApplicant.getById;
        }

        // Get user details
        const pool = await poolPromise
        const result = await pool.request()
            .input('Id', sql.VarChar, userId)
            .input('UserID', sql.VarChar, userId)
            .query(queryUser)

        let user = {};
        if (result.recordset.length == 1) {
            user = result.recordset[0];
        } else {
            throw "User " + userId + " not found";
        }

        // Create token payload
        const payload = {
            id: user.Id,
            name: user.Name,
            loginId: isAdmin ? user.UserName : user.LoginEmail,
            role: isAdmin ? 'admin' : 'crew'
        }
        
        // Generate token
        const token = jwt.sign(payload, tokenKey, { expiresIn: '1d' })        
        return token;

    } catch (error) {
        console.log("ERROR : ", error)
        return null;
    }
  }

  async loginForUser(username, password, isAdmin) {
    console.log('Trying to login as ' + username + ' on ' + new Date());
    try {
        if (password && username) {
            
            var resultUser = '';
            const pool = await poolPromise
            if (isAdmin) {
                resultUser = await pool.request()
                .input('Username', sql.VarChar, username)
                .query(qUserConfigure.getByUsernameForLogin)
            } else {
                resultUser = await pool.request()
                .input('LoginEmail', sql.VarChar, username)
                .query(queries.getLoginUser)
            }


            const user = resultUser.recordset[0];
            if (user) {
                var isValidPassword = false

                // Compare input password with password data in db
                if (isAdmin) {
                    isValidPassword = await bcrypt.compare(password, user.Password);
                } else {
                    isValidPassword = password == user.Password;
                }

                let token = '';
                if (isValidPassword) {
                    token = await this.generateTokenForUser(user.Id, isAdmin);
                } else {
                    if (isAdmin) {
                        throw { error: "User id or password not correct" }; 
                    } else {
                        throw { error: "Email or password not correct" }; 
                    }
                }

                if (isAdmin) {
                    return { "token" : token, "id": user.Id, "name": user.UserName, "email": user.Email, "verifyStatus": user.verifyStatus, "UserID": user.UserID[0] };
                } else {
                    return { "token" : token, "id": user.Id, "name": user.Name, "email": user.LoginEmail, "verifyStatus": user.verifyStatus };
                }
            } else {
                throw "User is not exist in the system";
            }
    
        } else {
            throw "Username or password not provided";
        }
    } catch (error) {
        console.log("ERROR : ", error);
        return null;
    }
  }
}

const controller = new AuthController()
module.exports = controller
