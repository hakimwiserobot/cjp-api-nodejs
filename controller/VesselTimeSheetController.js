const { sql, poolPromise } = require('../database/db')
const fs = require('fs')
const sgMail = require('@sendgrid/mail')
var rawdata = fs.readFileSync('./query/queries.json')
var queries = JSON.parse(rawdata)
var rawdata2 = fs.readFileSync('./query/qApplicantApply.json');
var qApplicantApply = JSON.parse(rawdata2);
var GenerationController = require('./GenerationController.js')
var path = require('path')

//added 20/1/2021
const { PDFNet } = require('@pdftron/pdfnet-node')
var Docxtemplater = require('docxtemplater')
var ImageModule = require('docxtemplater-image-module-free')
var moment = require('moment')

var PizZip = require('pizzip')
const { createNotEmittedStatement } = require('typescript')

var opts = { centered: false }

var XlsxTemplate = require('xlsx-template');
var Exceljs = require('exceljs');

//added watermark
const Jimp = require('jimp')
const { resolve } = require('path')
const { start } = require('repl')

opts.getImage = function (tagValue, tagName) {
  return fs.readFileSync(tagValue, 'binary')
}

opts.getSize = function (img, tagValue, tagName) {
  return [150, 150]
}

class VesselTimeSheetController {
  async getVesselTimeSheet(req, res) {
      try {
          var applications = [];
          console.log("Get application from " + req.query.dateFrom + " - " + req.query.dateTo)
          if (req.query.dateFrom != null && req.query.dateFrom != '' && req.query.dateTo != null && req.query.dateTo != '') {
            var dateFromSplitted = req.query.dateFrom.split('/');
            var dateToSplitted = req.query.dateTo.split('/');

            var dateFrom = new Date(dateFromSplitted[2], (parseInt(dateFromSplitted[1])-1), dateFromSplitted[0]);
            var dateTo = new Date(dateToSplitted[2], (parseInt(dateToSplitted[1])-1), dateToSplitted[0]);

            var name = req.query.name ? req.query.name : '';
            var position = req.query.position ? req.query.position : '';
            var vessel = req.query.vessel ? req.query.vessel : '';

            const pool = await poolPromise
            var result = {};
            if (vessel == 'All') {
              result = await pool.request().query(queries.getVesselTimeSheetAll);
            } else {
              result = await pool.request().input('Vessel', sql.VarChar, vessel).query(queries.getVesselTimeSheet);
            }

            for (var i = 0; i < result.recordset.length; i++) {
              var isValidDate = false;

              var application = result.recordset[i];
              var dateContractFrom = new Date(application.ContractPeriodFrom);
              var dateContractTo = new Date(application.ContractPeriodTo);
  
              if (dateContractFrom >= dateFrom && dateContractTo <= dateTo) {
                isValidDate = true;             
              } else if (dateContractFrom < dateFrom && dateContractTo > dateTo) {
                isValidDate = true;             
              } else if (dateContractTo >= dateFrom && dateContractTo <= dateTo) {
                isValidDate = true;             
              } else if (dateContractFrom >= dateFrom && dateContractFrom <= dateTo) {
                isValidDate = true;             
              }

              var applicantFullname = '';
              if (application.Name != null && application.Name != '') {
                applicantFullname = applicantFullname + application.Name;
              }

              if (application.MiddleName != null && application.MiddleName != '') {
                applicantFullname = applicantFullname + ' ' + application.MiddleName;
              }

              if (application.LastName != null && application.LastName != '') {
                applicantFullname = applicantFullname + ' ' + application.LastName;
              }

              if (isValidDate && (name == '' || applicantFullname.toLowerCase().indexOf(name.toLowerCase()) > -1) 
              && (position == 'All' || application.ApplyPosition == position)
              && (vessel == 'All' || application.NameofVessel == vessel)) {
                applications.push(application);
              }
            }
          } else {
            applications = result.recordset;
          }

          res.json(applications)
      } catch (error) {
          console.log(error);
          res.status(500)
          res.send(error.message)
      }
  }

  async generateTimesheet(req, res) {
    try {
      if (req.body.rowInfo.length && req.body.totalVessel.length) {
        var today = new Date();
        if (req.body.selectedMonth) {
          today = new Date(req.body.selectedMonth);
        }
        var dd = String(today.getDate()).padStart(2, '0')
        var mm = String(today.getMonth() + 1).padStart(2, '0') //January is 0!
        var yyyy = today.getFullYear()
        var hh = today.getHours()
        var min = today.getMinutes()
        var sec = today.getSeconds()

        var splitDateFrom = req.body.dateFrom.split('/');
        var splitDateTo = req.body.dateTo.split('/');
        var startDay = splitDateFrom[0];
        var startDayCutOff = moment(new Date(splitDateFrom[2], splitDateFrom[1], splitDateFrom[0])).daysInMonth();
        var endDay = splitDateTo[0];

        var timesheetMonth = moment(today).format("MMM");
        var timesheetYear = yyyy;
        var daysInTimesheet = moment(req.body.dateTo, 'DD/MM/YYYY').diff(moment(req.body.dateFrom, 'DD/MM/YYYY'), 'days', false)+1; // Add 1 to include start and end date
        if (daysInTimesheet == null || daysInTimesheet < 0) {
          daysInTimesheet = moment(req.body.dateTo, 'MM/DD/YYYY').diff(moment(req.body.dateFrom, 'MM/DD/YYYY'), 'days', false)+1; // Add 1 to include start and end date
          if (daysInTimesheet == null || daysInTimesheet < 0) {
            throw "Unable to get total numbers of days between date " + req.body.dateFrom + " and " + req.body.dateTo; 
          }
        }
        // var daysInTimesheet = moment(new Date(splitDateTo[2], splitDateTo[1], splitDateTo[0])).diff(moment(new Date(splitDateFrom[2], splitDateFrom[1], splitDateFrom[0])), 'days'); // +2; // Add 2 to include start and end date
        // if (parseInt(splitDateFrom[1]) == parseInt(splitDateTo[1])) {
        //   if (startDay == 1) {
        //     daysInMonth = endDay;
        //   } else {
        //     daysInMonth = parseInt(startDayCutOff) - (parseInt(startDayCutOff) - parseInt(endDay)) - (parseInt(startDay)-1);
        //   }
        // } else if (parseInt(splitDateFrom[1]) != parseInt(splitDateTo[1])) {
        //   const date1 = parseInt(startDayCutOff) - parseInt(startDay) ;
        //   const date2 = parseInt(endDay);
        //   console.log("startDayCutOff : " + startDayCutOff + ", startDay : " + startDay + ", endDay : " + endDay);
        //   daysInMonth = date1+date2+1; // Plus 1 to include selected date
        // }
        console.log("Generate timesheet from " + req.body.dateFrom + " until " + req.body.dateTo);
        console.log("Total days : " + daysInTimesheet);

        today = yyyy + mm + dd + hh + min + sec

        var templatename = 'Timesheet.xlsx';
        // if (daysInMonth == 31) { templatename = 'Timesheet31Days.xlsx' }
        // if (daysInMonth == 28) { templatename = 'Timesheet28Days.xlsx' }

        var data = fs.readFileSync(path.join('./Templates/', templatename));

        var template = new XlsxTemplate(data);

        // clone sheet for different vessel
        for (var i=0; i<req.body.totalVessel.length; i++) {
            var vesselname = req.body.totalVessel[i];
            template.copySheet("Sheet 1", vesselname);
        }

        // remove original sheet
        template.deleteSheet("Sheet 1");

        var cellGrey = []

        // Set days and empty cells
        let days = [];
        let emptyCells = [];
        let timesheetDateFromMonth = parseInt(splitDateFrom[1]);
        let timesheetDateFromYear = parseInt(splitDateFrom[2]);
        let timesheetDateToMonth = parseInt(splitDateTo[1]);
        let timesheetDateToYear = parseInt(splitDateTo[2]);
        let newDayCounter = parseInt(splitDateFrom[0]);
        let newDayLimit = moment(new Date(timesheetDateFromYear, timesheetDateFromMonth-1, 1)).daysInMonth();
        // console.log("Current month : " + timesheetDateFromMonth + ", Days limit : " + newDayLimit);
        for (let i = 1; i <= daysInTimesheet; i++) {

          days.push(newDayCounter);
          emptyCells.push("");

          if (newDayCounter < newDayLimit) {
            newDayCounter++;
          } else {
            timesheetDateFromMonth++;
            if (timesheetDateFromMonth > 12) {
              timesheetDateFromYear++;
              timesheetDateFromMonth = 1;
            }

            newDayLimit = moment(new Date(timesheetDateFromYear, timesheetDateFromMonth-1, 1)).daysInMonth();
            newDayCounter = 1;

            // console.log("Current month : " + timesheetDateFromMonth + ", Days limit : " + newDayLimit);
          }
        }

        // loop row to sheet with same type of vessel
        for (var j=0; j<req.body.totalVessel.length; j++) {
          var sheetNumber = j+1;

          var exportData = [];
          var sheetCellGrey = [];
          var row = -1;

          // get only the same vessel to write in same sheet
          var selectedData = req.body.rowInfo.filter(x => x.NameofVessel === req.body.totalVessel[j]);
          let rowCellGrey = [];
          for (var i = 0; i<selectedData.length; i++) {

            // define payment per day data
            var paymentPerDay = '';
            var paymentPerDayRemarks = ''
            if (selectedData[i].Salary != null && selectedData[i].Salary != '') {
              row = row + 1;

              let data = this.createRowData(row, mm, yyyy, startDay, endDay, daysInTimesheet, req.body.dateFrom, req.body.dateTo, days, emptyCells, selectedData[i], 'Salary', selectedData[i].Salary);
              sheetCellGrey.push(data.nonWorDayCell);
              exportData.push(data.rowData)
            
            }
            if (selectedData[i].Allowance != null && selectedData[i].Allowance != '') {
              row = row + 1;

              let data = this.createRowData(row, mm, yyyy, startDay, endDay, daysInTimesheet, req.body.dateFrom, req.body.dateTo, days, emptyCells, selectedData[i], 'Allowance', selectedData[i].Allowance);
              sheetCellGrey.push(data.nonWorDayCell);
              exportData.push(data.rowData)
              
            }
            if (selectedData[i].StandbyAllowance != null && selectedData[i].StandbyAllowance != '') {
              row = row + 1;
              
              let data = this.createRowData(row, mm, yyyy, startDay, endDay, daysInTimesheet, req.body.dateFrom, req.body.dateTo, days, emptyCells, selectedData[i], 'Standby Allowance', selectedData[i].StandbyAllowance);
              sheetCellGrey.push(data.nonWorDayCell);
              exportData.push(data.rowData)

            }
            if (selectedData[i].OtherAllowance != null && selectedData[i].OtherAllowance != '') {
              row = row + 1;
              
              let data = this.createRowData(row, mm, yyyy, startDay, endDay, daysInTimesheet, req.body.dateFrom, req.body.dateTo, days, emptyCells, selectedData[i], 'Other Allowance', selectedData[i].OtherAllowance);
              sheetCellGrey.push(data.nonWorDayCell);
              exportData.push(data.rowData)

            }
            if ((selectedData[i].Salary == null || selectedData[i].Salary == '') 
            && (selectedData[i].Allowance == null || selectedData[i].Allowance == '')
            && (selectedData[i].StandbyAllowance == null || selectedData[i].StandbyAllowance == '')
            && (selectedData[i].OtherAllowance == null || selectedData[i].OtherAllowance == '')) {
              row = row + 1;

              let data = this.createRowData(row, mm, yyyy, startDay, endDay, daysInTimesheet, req.body.dateFrom, req.body.dateTo, days, emptyCells, selectedData[i], '', '');
              sheetCellGrey.push(data.nonWorDayCell);
              exportData.push(data.rowData)
              
            }

          }

          // sheetCellGrey.push(rowCellGrey);


          cellGrey.push(sheetCellGrey);

          // Set up some placeholder values matching the placeholders in the template
          var values = {
              timesheetMonth: timesheetMonth.toUpperCase(),
              timesheetYear: yyyy,
              vesselname: req.body.totalVessel[j],
              dateRange: req.body.dateFrom + ' - ' + req.body.dateTo,
              days: days,
              emptyCell: emptyCells,
              people: exportData
          };

          // Perform substitution to append the value to script template
          template.substitute(sheetNumber, values);
        }

        var buf = template
          .generate({ type: 'nodebuffer', compression: 'DEFLATE' })

        fs.writeFileSync(
          path.resolve('../dist/assets/UserDoc/' + 'Timesheet_' + today + '.xlsx'),
          buf
        )

        console.log('Successfully generated Timesheet');

        // Add grey column
        let workbook = new Exceljs.Workbook();
        await workbook.xlsx.readFile('../dist/assets/UserDoc/' + 'Timesheet_' + today + '.xlsx');

        for (var i = 0; i < cellGrey.length; i++) {
          let worksheet = workbook.getWorksheet(i+1);
          let rowStart = 6
          let cellStart = 10
          for (var j = 0; j < cellGrey[i].length; j++) {
            let rowIndex = rowStart+j;
            const row = worksheet.getRow(rowIndex);
            for (var k = 0; k < cellGrey[i][j].length; k++) {
              let cellIndex = parseInt(cellStart) + parseInt(cellGrey[i][j][k]);
              const cell = row.getCell(cellIndex);
              cell.style = {
                    ...cell.style,
                    fill :{ type: 'pattern', pattern:'solid', fgColor: { argb: 'FFFFFF' }},
              };
            }

            // Resize column
            for (let i = 0; i < daysInTimesheet; i++) {
              worksheet.getColumn(cellStart+i).width = 5;
            }

            // let startColumn = cellStart+6;
            // let endColumn = cellStart+daysInTimesheet-6;
            // const row4 = worksheet.getRow(4);
            // row4.splice(10, 0, 'new value 1', 'new value 2');

            let cellM4Style = worksheet.getCell('M4').style;
            const row4 = worksheet.getRow(4);
            const cellLabelTotal = row4.getCell(10+daysInTimesheet+2);
            cellLabelTotal.value = "TOTAL (RM)";
            cellLabelTotal.style = {
              ...cellM4Style,
              fill: { type: 'pattern', pattern:'solid', fgColor: { argb: 'FFFFFF' }},
              font: { bold: true }
            };

            let cellJ4 = worksheet.getCell('J4');
            let cellJ4Value = cellJ4.value;
            worksheet.unMergeCells('J4');
            worksheet.mergeCells(4,10,4,10+daysInTimesheet+1);
            worksheet.getCell('J4').value = cellJ4Value;

            // let cellJ2 = worksheet.getCell('J2');
            // let cellJ2Value = cellJ2.value;
            // worksheet.unMergeCells('J2');
            // worksheet.mergeCells(2,10,2,10+daysInTimesheet+1);
            // worksheet.getCell('J2').value = cellJ2Value;

            if (10+daysInTimesheet+1 > 25) {
              const rowLabelBy = worksheet.getRow(7+cellGrey[i].length);
              const cellLabelVerified = rowLabelBy.getCell(16);
              cellLabelVerified.value = "Verified by:";
              cellLabelVerified.style = rowLabelBy.getCell(8).style;

              const cellLabelApproved = rowLabelBy.getCell(24);
              cellLabelApproved.value = "Approved by:";
              cellLabelApproved.style = rowLabelBy.getCell(8).style;

              const row13 = worksheet.getRow(12+cellGrey[i].length);
              const cellLabelDottedLine = row13.getCell(17);
              cellLabelDottedLine.value = "……………………………………";

              const cellLabelDottedLine2 = row13.getCell(25);
              cellLabelDottedLine2.value = "……………………………………";
            }
            
          }

          

          // for (var j = 0; j < cellGrey[i].length; j++) {
          //   const cell = worksheet.getCell(cellGrey[i][j])
          //   cell.style = {
          //     ...cell.style,
          //     fill :{ type: 'pattern', pattern:'solid', fgColor: { argb: 'FFFFFF' }}
          //   };
          // }
        }


        
        await workbook.xlsx.writeFile('../dist/assets/UserDoc/' + 'Timesheet_' + today + '-work-days.xlsx');

        console.log('Successfully adding non working days cells to timesheet');

        return { isSuccess: true, filePath: 'Timesheet_'+today+'-work-days.xlsx' }
        
      } else {
        return false
      }
      
    } catch (error) {
      
      console.log('Error: ', error)
      return false
    }
  }

  async generateTimesheetTest(req, res) {
    console.log('Start generating test excel');

    var templatename = 'Template1.xlsx';
    var data = fs.readFileSync(path.join('./Templates/', templatename));

    var template = new XlsxTemplate(data);
    // Replacements take place on first sheet
    var sheetNumber = 1;

    // Set up some placeholder values matching the placeholders in the template
    var values = {
            extractDate: new Date(),
            dates: [ new Date("2013-06-01"), new Date("2013-06-02"), new Date("2013-06-03") ],
            people: [
                {name: "John Smith", age: 20, days: [1, 2, 3], role: 'Supervisor'},
                {name: "Bob Johnson", age: 22, days: [1, 2, 3], role: 'Technician'}
            ]
        };

    // Perform substitution
    template.substitute(sheetNumber, values);

    // Get binary data
    var data = template.generate();

    var buf = template.generate({ type: 'nodebuffer', compression: 'DEFLATE' })

    let today = new Date();

    var dd = String(today.getDate()).padStart(2, '0')
    var mm = String(today.getMonth() + 1).padStart(2, '0') //January is 0!
    var yyyy = today.getFullYear()
    var hh = today.getHours()
    var min = today.getMinutes()
    var sec = today.getSeconds()
    
    today = yyyy + mm + dd + hh + min + sec

    let filename = 'Test_' + today + '.xlsx';
    fs.writeFileSync(path.resolve('../dist/assets/UserDoc/' + filename), buf)

    console.log('Successfully generated test excel');
    return { isSuccess: true, filePath: filename }
  }

  createRowData(row, timesheetMonth, timesheetYear, timesheetMinDay, timesheetMaxDay, daysInTimesheet, timesheetDateFrom, timesheetDateTo, days, emptyCells, data, remarks, amount,) {
    // define the row of data
    var rowData = {};
    rowData.rowNumber = row+1;
    rowData.Name = data.SignatureName;
    rowData.Email = data.LoginEmail;
    rowData.EmpId = data.EmpId;
    rowData.IC = data.IC;
    // rowData.Password = data.Password;
    rowData.OfferPosition = data.OfferPosition;
    rowData.ContractPeriodFrom = moment.utc(data.ContractPeriodFrom).format('DD/MM/YYYY');
    rowData.ContractPeriodTo = moment.utc(data.ContractPeriodTo).format('DD/MM/YYYY');
    rowData.Allowance = amount;
    rowData.Remarks = remarks;
    rowData.Status = data.ApplyStatus;
    rowData.TotalWorkDay = daysInTimesheet;
    rowData.WorkDay = emptyCells;

    var timesheetDateFromSplitted = timesheetDateFrom.split('/');
    var timesheetDateToSplitted = timesheetDateTo.split('/');

    let arrCellNonWorkDay = [];
    var dateFromSplitted = rowData.ContractPeriodFrom.split('/');
    var dateToSplitted = rowData.ContractPeriodTo.split('/');

    // Get start day index in days array
    var startIndex = -1;
    var minIndex = -1;
    // for(let i = 0; i < days.length; i++) {
    //   let day = days[i];
    //   if (parseInt(day) == parseInt(dateFromSplitted[0])) {
    //     startIndex = i;
    //   }
    //   if (parseInt(day) == parseInt(timesheetMinDay)) {
    //     minIndex = i;
    //   }
    //   if (startIndex > 0 && minIndex > 0) {
    //     break;
    //   }
    // }

    // Get end day index in days array
    let endIndex = -1;
    let maxIndex = -1;
    // for(let i = days.length-1; i > 0; i--) {
    //   let day = days[i];
    //   if (day == parseInt(dateToSplitted[0])) {
    //     endIndex = i;
    //   }
    //   if (day == parseInt(timesheetMaxDay)) {
    //     maxIndex = i;
    //   }
    //   if (endIndex > 0 && maxIndex > 0) {
    //     break;
    //   }
    // }

    // Validate if same year and month
    // if (parseInt(dateFromSplitted[1]) == parseInt(timesheetMonth) && parseInt(dateFromSplitted[2]) == parseInt(timesheetYear)
    // && parseInt(dateToSplitted[1]) == parseInt(timesheetMonth) && parseInt(dateToSplitted[2]) == parseInt(timesheetYear)) {
    //   // arrCellNonWorkDay = arrCellNonWorkDay.concat(this.getNonWorkDayCell(row, dateFromSplitted[0], dateToSplitted[0], timesheetMinDay, timesheetMaxDay, daysInMonth));
    //   arrCellNonWorkDay = arrCellNonWorkDay.concat(this.getNonWorkDayCell(row, startIndex, endIndex, minIndex, maxIndex, daysInMonth));
    // } else 
    // startIndex = moment(new Date(dateFromSplitted[2], dateFromSplitted[1], dateFromSplitted[0])).diff(moment(new Date(timesheetDateFromSplitted[2], timesheetDateFromSplitted[1], timesheetDateFromSplitted[0])), 'days', false); // Add 1 to include current day
    startIndex = moment(rowData.ContractPeriodFrom, 'DD/MM/YYYY').diff(moment(timesheetDateFrom, 'DD/MM/YYYY'), 'days', false); // Add 1 to include current day
    // console.log('Different between start date : ' + rowData.ContractPeriodFrom + ' - ' + timesheetDateFrom + ' is ' + startIndex)
    if (startIndex > 0) {
      // arrCellNonWorkDay = arrCellNonWorkDay.concat(this.getNonWorkDayCell(row, dateFromSplitted[0], null, timesheetMinDay, timesheetMaxDay, daysInMonth));
      arrCellNonWorkDay = arrCellNonWorkDay.concat(this.getNonWorkDayCell(row, startIndex, null, minIndex, maxIndex, daysInTimesheet));
    }

    // endIndex = moment(new Date(timesheetDateToSplitted[2], timesheetDateToSplitted[1], timesheetDateToSplitted[0])).diff(moment(new Date(dateToSplitted[2], dateToSplitted[1], dateToSplitted[0])), 'days', false); // Add 1 to include current day
    endIndex = moment(timesheetDateTo, 'DD/MM/YYYY').diff(moment(rowData.ContractPeriodTo, 'DD/MM/YYYY'), 'days', false); // Add 1 to include current day
    // console.log('Different between end date : ' + timesheetDateTo + ' - ' + rowData.ContractPeriodTo + ' is ' + endIndex)
    // if (parseInt(dateToSplitted[0]) <= parseInt(timesheetDateToSplitted[0]) && parseInt(dateToSplitted[1]) == parseInt(timesheetDateToSplitted[1]) && parseInt(dateToSplitted[2]) == parseInt(timesheetDateToSplitted[2])) {
      // arrCellNonWorkDay = arrCellNonWorkDay.concat(this.getNonWorkDayCell(row, null, dateToSplitted[0], timesheetMinDay, timesheetMaxDay, daysInMonth));
    if (endIndex > 0 && endIndex <= daysInTimesheet) {
      arrCellNonWorkDay = arrCellNonWorkDay.concat(this.getNonWorkDayCell(row, null, endIndex, minIndex, maxIndex, daysInTimesheet));
    } 

    // arrCellNonWorkDay = this.getNonWorkDayCell(row, null, null, minIndex, maxIndex, daysInTimesheet);
    
    if (endIndex > 0 && endIndex <= daysInTimesheet) {
      rowData.TotalWorkDay = daysInTimesheet - arrCellNonWorkDay.length + 1;
    } else {
      rowData.TotalWorkDay = daysInTimesheet - arrCellNonWorkDay.length;
    }

    // if (parseInt(dateFromSplitted[1]) < parseInt(timesheetMonth) && parseInt(dateToSplitted[1]) > parseInt(timesheetMonth)
    // && parseInt(dateFromSplitted[2]) <= parseInt(timesheetYear) && parseInt(dateToSplitted[2]) >= parseInt(timesheetYear)) {
    //   arrCellNonWorkDay = this.getNonWorkDayCell(row, null, null, timesheetMinDay, timesheetMaxDay, daysInMonth);
    //   rowData.TotalWorkDay = daysInMonth - arrCellNonWorkDay.length;
    // }

    // if (parseInt(dateFromSplitted[1]) < parseInt(timesheetMonth) && parseInt(dateToSplitted[1]) < parseInt(timesheetMonth)
    // && parseInt(dateFromSplitted[2]) <= parseInt(timesheetYear) && parseInt(dateToSplitted[2]) <= parseInt(timesheetYear)) {
    //   arrCellNonWorkDay = this.getNonWorkDayCell(row, null, null, timesheetMinDay, timesheetMaxDay, daysInMonth);
    //   rowData.TotalWorkDay = daysInMonth - arrCellNonWorkDay.length;
    // }
    // if (parseInt(dateFromSplitted[1]) > parseInt(timesheetMonth) && parseInt(dateToSplitted[1]) > parseInt(timesheetMonth)
    //   && parseInt(dateFromSplitted[2]) >= parseInt(timesheetYear) && parseInt(dateToSplitted[2]) >= parseInt(timesheetYear)) {
    //   arrCellNonWorkDay = this.getNonWorkDayCell(row, null, null, timesheetMinDay, timesheetMaxDay, daysInMonth);
    //   rowData.TotalWorkDay = daysInMonth - arrCellNonWorkDay.length;
    // }
    // if (parseInt(dateFromSplitted[1]) > parseInt(timesheetMonth) && parseInt(dateToSplitted[1]) < parseInt(timesheetMonth)
    //   && parseInt(dateFromSplitted[2]) < parseInt(timesheetYear) && parseInt(dateToSplitted[2]) == parseInt(timesheetYear)) {
    //   arrCellNonWorkDay = this.getNonWorkDayCell(row, null, null, timesheetMinDay, timesheetMaxDay, daysInMonth);
    //   rowData.TotalWorkDay = daysInMonth - arrCellNonWorkDay.length;
    // }

    return { rowData:rowData, nonWorDayCell:arrCellNonWorkDay };
  }

  getNonWorkDayCell(index, startIndex, endIndex, minIndex, maxIndex, daysInTimesheet) {
    let indexInExcel = 6;
    let whiteCell = [];
    let column = [];
    // if (daysInMonth == 31) {
    //   column = ['J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN'];
    // } else if (daysInMonth == 28) {
    //   column = ['K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL']; 
    // } else {
    //   column = ['J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM'];
    // }
    
    if (startIndex != null) {
      // startDate = parseInt(startDate) < parseInt(minDate) ? minDate : startDate;
      for (var i = parseInt(startIndex)-1; i >= 0; i--) {
        whiteCell.push(i);
      }
    }
    if (endIndex != null) {
      // endDate = parseInt(endDate) > parseInt(maxDate) ? maxDate : endDate;
      for (var i = parseInt(endIndex)+1; i > 0; i--) {
        whiteCell.push(daysInTimesheet--);
      }
    }

    // if (startDate == null && parseInt(minDate) > 0) {
    //   for (var i = parseInt(minDate)-1; i >= 0; i--) {
    //     whiteCell.push(column[i]+(index+indexInExcel));
    //   }
    // }
    // if (endDate == null && parseInt(maxDate) < parseInt(daysInMonth)) {
    //   for (var i = parseInt(maxDate); i < parseInt(daysInMonth); i++) {
    //     whiteCell.push(column[i]+(index+indexInExcel));
    //   }
    // }

    return whiteCell;
  }
}

const controller = new VesselTimeSheetController()
module.exports = controller
