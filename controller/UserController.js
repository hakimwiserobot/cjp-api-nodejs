const { sql,poolPromise } = require('../database/db')
const fs = require('fs');
var rawdata = fs.readFileSync('./query/queries.json');
var queries = JSON.parse(rawdata);
var rawdata2 = fs.readFileSync('./query/qApplicant.json');
var qApplicant = JSON.parse(rawdata2);
const bcrypt = require('bcryptjs')
const jwt = require("jsonwebtoken");
var generator = require('generate-password');
const sgMail = require('@sendgrid/mail')
const nodemailer = require("nodemailer");
const authController = require('./AuthController');

class UserController {

    async getUser(req , res){
      //console.log("come in user")
      //console.log("getUser: ", req.body);
      try {
        const pool = await poolPromise
        const result = await pool.request()
          .input('UserName',sql.VarChar , req.body.username)
          .input('Password',sql.VarChar , req.body.password)
          .query(queries.getUser)
          
          //res.json(result.recordset)
          res.send(result.recordset)
      } catch (error) {
        res.status(500)
        res.send(error.message)
      }
    }

    async authenticateUserApp(req , res){
      //console.log("come in authenticateUserApp")
      //console.log("getUser: ", req.body);
      try {
        const pool = await poolPromise
        const result = await pool.request()
          .input('LoginEmail',sql.VarChar , req.body.LoginEmail)
          .query(queries.authenticateUserApp)
          
          if(result != null && result.recordset != null) {
            console.log("Verify IC/Passport : "+req.body.NRICPassport);
            if (result.recordset[0].IC == req.body.NRICPassport || result.recordset[0].Passport == req.body.NRICPassport ||  req.body.NRICPassport == null || result.recordset[0].IC == null || result.recordset[0].Passport == null) {
              if(result.recordset[0].Password == req.body.Password) {
                //console.log("User authenticated!")
                res.send(result.recordset)
                return
              } else {
                //console.log("check if hashed password")
                // check if password is hashed
                const salt = await bcrypt.genSalt(10);
                const hashPassword = await bcrypt.hash(req.body.Password, salt)
                //console.log("loginAdmin bcrypt.genSalt(10) password: ", hashPassword)
                const validPassword = await bcrypt.compare(hashPassword, result.recordset[0].Password);
                if(validPassword) {
                  //console.log("User authenticated! Hashed Password")
                  res.send(result.recordset)
                  return
                }
              }
            }
          }
          // set to empty array to return
          result.recordset = []
          //res.json(result.recordset)
          res.send(result.recordset)
      } catch (error) {
        res.status(500)
        res.send(error.message)
      }
    }

    async authenticateAdmin(req, res) {
      //console.log("come in authenticate")
      //console.log("get authenticate: ", req.body);
      const token = req.body.token;
      if (!token) return res.status(401).json({ error: "Access denied" });
      try {
        const verified = jwt.verify(token, "anystring");
        res.status(200).json({ ok: "Token valid" });
      } catch (err) {
        res.status(400).json({ error: "Token is not valid" });
      }
    }

    async forgotPassword(req, res) {
      try { 
        if (req.body.email) {
          const pool = await poolPromise
          const result = await pool.request()
            .input('LoginEmail',sql.VarChar , req.body.email)
            .query(queries.getLoginUser)

            const userEmailExist = result.recordset.length;

            var password = generator.generate({
              length: 10,
              numbers: true
            });

            var sendEmail = false;
          
            if (userEmailExist) {
              //send email
              // sgMail.setApiKey(
              //   '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'
              // )
              let htmlText =
                'Dear ' +
                result.recordset[0].Name +
                '<br /><br />' +
                'Temporary password is created. Please use the temporary password to login to account' +
                '<br /><br />' +
                '<strong>Temporary password: <strong>' + password + '<br />' 

              // const msg = {
              //   //to: 'desmond@wiserobot.com',
              //   to: result.recordset[0].LoginEmail, // Change to your recipient
              //   from: 'hakim@wiserobot.com', // Change to your verified sender
              //   subject: '[TEST]: SKOM eCrew Job Portal Password Reset',
              //   text: 'SKOM eCrew Job Portal Password Reset',
              //   html: htmlText,
              // }
              // sgMail
              //   .send(msg)
              //   .then(() => {
              //     sendEmail = true
              //     //console.log('Forgot Password Email sent to ' + result.recordset[0].LoginEmail)
              //   })
              //   .then(() => {
                  // if (sendEmail) {
                  //   //const pool = await poolPromise
                  //   const result1 = pool.request()
                  //   .input('Password', sql.VarChar , password)
                  //   .input('LoginEmail', sql.VarChar, req.body.email)
                  //   .query(queries.forgotPasswordUser)

                  //   result1.then(function(data) {
                  //     var recordUser = data.rowsAffected[0];

                  //     if (recordUser) {
                  //       res.status(200).send({message: 'Reset Password successfully. Please check your email!'});
                  //     } else {
                  //       res.status(400).send('Failed to reset password!')
                  //     }
                  //   })
                    
                  // } else {
                  //   res.status(400).send('Failed to send email!')
                  // }
              //   })
              //   .catch((error) => {
              //     console.error(error)
              //     res.status(500)
              //     res.send(error.message)
              //   })

              // Added by Hakim on 22 Feb 2021 
              let transporter = nodemailer.createTransport({
                pool: false,
                host: "smtp.office365.com",
                port: 587,
                secureConnection: false,
                tls: { ciphers: 'SSLv3' },
                auth: {
                    user: process.env.EMAIL_ADDRESS,
                    pass: process.env.EMAIL_PASSWORD
                }
              });

              // let transporter = nodemailer.createTransport({
              //   sendmail: true,
              //   newline: 'windows',
              //   path: '/usr/lib/sendmail'
              // })

              transporter.verify(function (error, success) {
                if (error) {
                  console.log(error)
                } else {
                  console.log("Server ready to send email")
                }
              })

              transporter.sendMail({
                from: process.env.EMAIL_ADDRESS,
                to:  result.recordset[0].LoginEmail,
                subject: 'SKOM eCrew Job Portal Password Reset',
                html: htmlText
              }, 
              function(err, info) {
                if (!err) {
                  //const pool = await poolPromise
                  const result1 = pool.request()
                  .input('Password', sql.VarChar , password)
                  .input('LoginEmail', sql.VarChar, req.body.email)
                  .query(queries.forgotPasswordUser)

                  result1.then(function(data) {
                    var recordUser = data.rowsAffected[0];

                    if (recordUser) {
                      res.status(200).send({message: 'Reset Password successfully. Please check your email!'});
                    } else {
                      res.status(400).send('Failed to reset password!')
                    }
                  })
                } else {
                  res.status(400).send('Failed to send email!')
                }
              }
              )
			  // Added by Hakim on 22 Feb 2021 
              
            } else {
              res.status(400).send('Email not exist!')
            }
        }
      } catch (error) {
        res.status(500)
        res.send(error.message)
      }
    }

    async updatePasswordAdmin(req, res) {
      try {
        //console.log("updatePasswordAdmin: ", req.body);
        if(req.body.password != null && req.body.username != null) {
            let isNewDB = true
            const pool = await poolPromise
            let result = await pool.request()
              .input('UserName',sql.VarChar , req.body.username)
              .query("SELECT * FROM [UserConfigure] WHERE Username=@UserName");
			
            let user = result.recordset[0]
            if (result.recordset.length == 0 ) {
              res.status(400).send('Password Incorrect?!')
            } else {
              
              // Added by Hakim on 8 Feb 2021 - Start
              let query = queries.updatePasswordAdmin
              let new_password = req.body.new_password
              if (isNewDB) {
                let validPassword = await bcrypt.compare(req.body.password, user.Password)
                if (!validPassword) {
                res.status(400).send('Password Incorrect?!')
                }
                
                const salt = await bcrypt.genSalt(10);
                new_password = await bcrypt.hash(req.body.new_password, salt)
                query = queries.updatePasswordAdmin2
              }
              // Added by Hakim on 8 Feb 2021 - End

              const pool = await poolPromise
              const result = await pool.request()
              .input('Password', sql.VarChar , new_password) // Update by Hakim on 8 Feb 2021
              .input('UserName', sql.VarChar, req.body.username)
              .query(query)

              var recordUser = result.rowsAffected[0];

              if (recordUser) {
                const token = await authController.generateTokenForUser(recordUser.UserID, true);
                // const token = jwt.sign({
                //   name: recordUser.UserName,
                //   id: recordUser.UserID,
                // }, "anystring", {expiresIn: 3600})
                // res.status(200).json({token})
                res.status(200).send({"token" : token, "name": recordUser.UserName, "email": recordUser.LoginEmail});
              } else {
                res.status(400).send('Failed to change password!')
              }
            }
        } else {
          res.status(400).send('Please fill all the details!')
        }
      } catch (error) {
        res.status(500)
        res.send(error.message)
      }
    }

    async updatePasswordUser(req, res) {
      try {
        //console.log("updatePasswordUser: ", req.body);
        if(req.body.password != null && req.body.new_password != null 
          && req.body.email != null) {
          const pool = await poolPromise
          const result = await pool.request()
            .input('LoginEmail',sql.VarChar , req.body.email)
            .query(queries.getEmailUser)
            
          const userEmailExist = result.recordset.length;
          var getRecordUser = result.recordset[0];

          if (!userEmailExist) {
            res.send('Failed to change password, email no exist!')
          } else {
            //const salt = await bcrypt.genSalt(10);
            //req.body.new_password = await bcrypt.hash(req.body.new_password, salt)
            
            const pool = await poolPromise
            const result = await pool.request()
            .input('newPassword', sql.VarChar , req.body.new_password)
            .input('Password', sql.VarChar , req.body.password)
            .input('LoginEmail', sql.VarChar, req.body.email)
            .query(queries.updatePasswordUser)

            var recordUser = result.rowsAffected[0];

            if (recordUser) {
              const token = await authController.generateTokenForUser(recordUser.Id, false);
              // const token = jwt.sign({
              //   name: recordUser.Name,
              //   id: recordUser.Id,
              // }, "anystring", {expiresIn: 3600})
              // res.status(200).json({token})
              //res.send(token)
              res.status(200).send({"token" : token, "name": recordUser.Name, "email": getRecordUser.LoginEmail});
            } else {
              res.status(400).send('Failed to change password!')
            }
          }
        } else {
          res.status(400).send('Please fill all the details!')
        }
      } catch (error) {
        res.status(500)
        res.send(error.message)
      }
    }

    async updateEmailUser(req, res) {
      try {
        //console.log("updateEmailUser: ", req.body);
        if(req.body.password != null && req.body.new_email != null && req.body.email != null && req.body.username != null) {
            const pool = await poolPromise
            const result = await pool.request()
              .input('LoginEmail',sql.VarChar , req.body.new_email)
              .query(queries.getEmailUser)

              const userEmailExist = result.recordset.length;

              var uniqueString = await module.exports.randString();
              var activeExpires = Date.now() + 24 * 3600 * 1000;
              
              if (userEmailExist) {
                // if the new email already exist, user not able to update
                let err = { success:false, message:'Failed to change email, the current new email has been used by others!' }
                throw err
              } else {
                const pool1 = await poolPromise
                const result1 = await pool1.request()
                .input('newEmail', sql.VarChar , req.body.new_email)
                .input('Password', sql.VarChar , req.body.password)
                .input('LoginEmail', sql.VarChar, req.body.email)
                .input('activeToken', sql.VarChar, uniqueString)
                .input('activeExpires', sql.VarChar, activeExpires)
                .query(queries.updateEmailUser)

                const result2 = await pool1.request()
                .input('newEmail', sql.VarChar , req.body.new_email)
                .input('LoginEmail', sql.VarChar, req.body.email)
                .query(queries.updateEmailApplicantApply)

                const result3 = await pool1.request()
                .input('newEmail', sql.VarChar , req.body.new_email)
                .input('UserID', sql.VarChar, req.body.email)
                .query(queries.updateEmailApplicantDocument)

                const result4 = await pool1.request()
                .input('newEmail', sql.VarChar , req.body.new_email)
                .input('UserID', sql.VarChar, req.body.email)
                .query(queries.updateEmailApplicantExperience)

                const result5 = await pool1.request()
                .input('newEmail', sql.VarChar , req.body.new_email)
                .input('LoginEmail', sql.VarChar, req.body.email)
                .query(queries.updateEmailApplicantGeneralAnswer)

                const result6 = await pool1.request()
                .input('newEmail', sql.VarChar , req.body.new_email)
                .input('LoginEmail', sql.VarChar, req.body.email)
                .query(queries.updateEmailApplicantMedicalAnswer)

                const result7 = await pool1.request()
                .input('newEmail', sql.VarChar , req.body.new_email)
                .input('UserID', sql.VarChar, req.body.email)
                .query(queries.updateEmailApplicantNOK)

                const result8 = await pool1.request()
                .input('newEmail', sql.VarChar , req.body.new_email)
                .input('Email', sql.VarChar, req.body.email)
                .query(queries.updateEmailContactUs)

                var recordUser = result1.rowsAffected[0];

                if (recordUser) {
                  const token = await authController.generateTokenForUser(recordUser.Id, false);
                  // const token = jwt.sign({
                  //   name: recordUser.Name,
                  //   id: recordUser.Id,
                  // }, "anystring", {expiresIn: 3600})

                  const resultEmail = await module.exports.sendEmail(req.body.new_email,req.body.username,uniqueString);

                  res.status(200).send({"token" : token, "name": recordUser.Name, "email": req.body.new_email});
                  
                } else {
                  res.status(400).json({ success:false, message:'Failed to change email! Please try again.' })
                }
              }
        } else {
          res.status(400).json({ success:false, message:'Please fill all the details!' })
        }
      } catch (err) {
        console.log(err)
        res.status(500).json(err)
      }
    }

    async updateEmailAdmin(req, res) {
      try {
        //console.log("updateEmailAdmin: ", req.body);
        if(req.body.password != null && req.body.new_email != null && req.body.email != null && req.body.username != null) {
            const pool = await poolPromise
            const result = await pool.request()
              .input('LoginEmail',sql.VarChar , req.body.new_email)
              .query(queries.getEmailAdminNewDB)

              const adminEmailExist = result.recordset.length;

              var uniqueString = await module.exports.randString();
              var activeExpires = Date.now() + 24 * 3600 * 1000;
              
              if (adminEmailExist) {
                // if the new email already exist, user not able to update
                let err = { success:false, message:'Failed to update email, the current new email has been used by others!' }
                throw err
              } else {

                const result2 = await pool.request()
                .input('UserName',sql.VarChar , req.body.username)
                .query(queries.getAdmin2)
                let getRecordAdmin = result2.recordset[0];

                const validPassword = await bcrypt.compare(req.body.password, getRecordAdmin.Password);

                if (validPassword) {
                  
                  const pool1 = await poolPromise
                  const result1 = await pool1.request()
                    .input('newEmail', sql.VarChar , req.body.new_email)
                    .input('Password', sql.VarChar , getRecordAdmin.Password)
                    .input('LoginEmail', sql.VarChar, req.body.email)
                    .input('activeToken', sql.VarChar, uniqueString)
                    .input('activeExpires', sql.VarChar, activeExpires)
                    .query(queries.updateEmailAdmin)

                  var recordUser = result1.rowsAffected[0];

                  if (recordUser) {
                    const token = await authController.generateTokenForUser(recordUser.Id, true);
                    // const token = jwt.sign({
                    //   name: recordUser.UserName,
                    //   id: recordUser.Id,
                    // }, "anystring", {expiresIn: 3600})

                    const resultEmail = await module.exports.sendEmailAdmin(req.body.new_email,req.body.username,uniqueString);

                    res.status(200).send({"token" : token, "name": recordUser.UserName, "email": req.body.new_email});
                    
                  } else {
                    res.status(400).json({ success:false, message:'Failed to change email! Please try again.' })
                  }

                } else {

                  if (req.body.password === getRecordAdmin.Password) {
                    //password no hash but correct password
                    const result2 = await pool1.request()
                      .input('newEmail', sql.VarChar , req.body.new_email)
                      .input('Password', sql.VarChar , getRecordAdmin.Password)
                      .input('LoginEmail', sql.VarChar, req.body.email)
                      .input('activeToken', sql.VarChar, uniqueString)
                      .input('activeExpires', sql.VarChar, activeExpires)
                      .query(queries.updateEmailAdmin)

                    var recordUser2 = result2.rowsAffected[0];

                    if (recordUser2) {
                      const token = await authController.generateTokenForUser(recordUser2.Id, true);
                      // const token = jwt.sign({
                      //   name: recordUser2.UserName,
                      //   id: recordUser2.Id,
                      // }, "anystring", {expiresIn: 3600})

                      const resultEmail = await module.exports.sendEmail(req.body.new_email,req.body.username,uniqueString);

                      res.status(200).send({"token" : token, "name": recordUser2.UserName, "email": req.body.new_email});
                      
                    } else {
                      res.status(400).json({ success:false, message:'Failed to change email! Please try again.' })
                    }

                  } else {
                    res.status(400).json({ success:false, message:'Password incorrect! Please try again.' })
                  }
                }
              }
        } else {
          res.status(400).json({ success:false, message:'Please fill all the details!' })
        }
      } catch (err) {
        console.log(err)
        res.status(500).json(err)
      }
    }

    async updateEmailDefaultAdmin(req, res) {
      try {
        //console.log("updateEmailDefaultAdmin: ", req.body);
        if(req.body.password != null && req.body.new_email != null && req.body.username != null) {
            const pool = await poolPromise
            const result = await pool.request()
              .input('LoginEmail',sql.VarChar , req.body.new_email)
              .query(queries.getEmailAdminNewDB)

              const adminEmailExist = result.recordset.length;

              var uniqueString = await module.exports.randString();
              var activeExpires = Date.now() + 24 * 3600 * 1000;
              
              if (adminEmailExist) {
                // if the new email already exist, user not able to update
                let err = { success:false, message:'Failed to update email, the current new email has been used by others!' }
                throw err
              } else {

                const resultCurrentUserName = await pool.request()
                  .input('UserName',sql.VarChar , req.body.username)
                  .query(queries.getUserNameAdminNewDB)
                
                var getRecordAdmin = resultCurrentUserName.recordset[0];

                const validPassword = await bcrypt.compare(req.body.password, getRecordAdmin.Password);

                if (validPassword) {
                  
                  const pool1 = await poolPromise
                  const result1 = await pool1.request()
                    .input('newEmail', sql.VarChar , req.body.new_email)
                    .input('Password', sql.VarChar , getRecordAdmin.Password)
                    .input('UserName', sql.VarChar , req.body.username)
                    .input('activeToken', sql.VarChar, uniqueString)
                    .input('activeExpires', sql.VarChar, activeExpires)
                    .query(queries.updateEmailDefaultAdmin)

                  var recordUser = result1.rowsAffected[0];

                  if (recordUser) {
                    const token = await authController.generateTokenForUser(recordUser.Id, true);
                    // const token = jwt.sign({
                    //   name: recordUser.UserName,
                    //   id: recordUser.Id,
                    // }, "anystring", {expiresIn: 3600})

                    const resultEmail = await module.exports.sendEmailAdmin(req.body.new_email,req.body.username,uniqueString);

                    res.status(200).send({"token" : token, "name": recordUser.UserName, "email": req.body.new_email});
                    
                  } else {
                    res.status(400).json({ success:false, message:'Failed to change email! Please try again.' })
                  }

                } else {

                  if (req.body.password === getRecordAdmin.Password) {
                    //password no hash but correct password
                    const result2 = await pool1.request()
                      .input('newEmail', sql.VarChar , req.body.new_email)
                      .input('Password', sql.VarChar , getRecordAdmin.Password)
                      .input('UserName', sql.VarChar , req.body.username)
                      .input('activeToken', sql.VarChar, uniqueString)
                      .input('activeExpires', sql.VarChar, activeExpires)
                      .query(queries.updateEmailDefaultAdmin)

                    var recordUser2 = result2.rowsAffected[0];

                    if (recordUser2) {
                      const token = await authController.generateTokenForUser(recordUser2.Id, true);
                      // const token = jwt.sign({
                      //   name: recordUser2.UserName,
                      //   id: recordUser2.Id,
                      // }, "anystring", {expiresIn: 3600})

                      const resultEmail = await module.exports.sendEmail(req.body.new_email,req.body.username,uniqueString);

                      res.status(200).send({"token" : token, "name": recordUser2.UserName, "email": req.body.new_email});
                      
                    } else {
                      res.status(400).json({ success:false, message:'Failed to change email! Please try again.' })
                    }

                  } else {
                    res.status(400).json({ success:false, message:'Password incorrect! Please try again.' })
                  }
                }
              }
        } else {
          res.status(400).json({ success:false, message:'Please fill all the details!' })
        }
      } catch (err) {
        console.log(err)
        res.status(500).json(err)
      }
    }

    async authenticateUser(req, res, next) {
      //console.log("come in authenticate")
      //console.log("get authenticate: ", req.body);
      const header = req.headers['authorization'];

      if(typeof header !== 'undefined') {
        //console.log("header !== undefined")
        const bearer = header.split(' ');
        const token = bearer[1];

        req.token = token;
        //console.log("token: ", token)
        //next();
      } else {
        //If header is undefined return Forbidden (403)
        //console.log("Forbidden (403)")
        return res.sendStatus(403)
      }

      const token = req.token;
      if (!token) return res.status(401).json({ error: "Access denied" });
      try {
        const verified = jwt.verify(token, "anystring", (err, verifiedJwt) => {
          if(err){
            //console.log("error: ", err)
            if (err.name === 'TokenExpiredError') {
              //create a new token and send the same way you created initially
              return res.status(401).json({ error: "TokenExpiredError" });
            }
            return res.status(400).json({ error: "Token is not valid" });
          }else{
            //console.log("Successfully verified: ", verifiedJwt)
            //res.status(200).json({ ok: "Token valid" });
            //next()
          }
        })
        //console.log("verified: next")
        next()
        //return res.status(200).json({ ok: "Token valid" });
        //return res.status(400).json({ error: "Token is not valid" });
      } catch (err) {
        //console.log("catch error: ", err)
        res.status(400).json({ error: "Token is not valid" });
      }
    }

    async loginAdmin(req , res){
      try {
        var username = req.body.username;
        var password = req.body.password;
        const pool = await poolPromise;

        let loginData = await authController.loginForUser(username, password, true);
        if (loginData != null) {
          // Get admin access module
          const resultAccessModule = await pool.request()
          .input('UserConfigureID', sql.SmallInt, loginData.id)
          .query(queries.getUserIdConfigureById)
          const userAccessModule = resultAccessModule.recordset;

          res.status(200).json({ ...loginData, "AccessModule":userAccessModule });
        } else {
          res.send("User id or password is not correct")
        }
      } catch (error) {
		    console.log(error)
        res.status(500).send(error.message)
      }
    }

    async loginUser(req , res){
      //console.log("come in login")
      //console.log("get login: ", req.body);
      try {
        const pool = await poolPromise
        const result = await pool.request()
          .input('LoginEmail',sql.VarChar , req.body.email)
          .input('Password',sql.VarChar , req.body.password)
          .query(queries.getLoginUser)
          
          ////console.log(result.recordset[0].Id);
          //res.json(result.recordset)
          //res.send(result.recordset)
          var userData = result.recordset[0];
          if (userData) {
            const validPassword = await bcrypt.compare(req.body.password, userData.Password);
            
            if (!validPassword) {
              //console.log("check plaintext password")
              // maybe only not hash but the password is correct
              if (req.body.password === userData.Password) {
                const token = await authController.generateTokenForUser(userData.Id, false);
                // const token = jwt.sign({
                //   name: userData.Name,
                //   id: userData.Id,
                // }, "anystring", {expiresIn: 3600})
                // res.status(200).json({token})
                //res.json({"token" : token})
                res.status(200).send({"token" : token, "name": userData.Name, "email": userData.LoginEmail, "verifyStatus": userData.verifyStatus});
              } else {
                //res.json({"token" : null, "error": 'Password not correct'});
                return res.status(401).json({ error: "Email or password not correct" }); 
              }
            } else {
              //console.log("check hashed password")
              const token = await authController.generateTokenForUser(userData.Id, false);
              // const token = jwt.sign({
              //   name: userData.Name,
              //   id: userData.Id,
              // }, "anystring", {expiresIn: 3600})
              // res.status(200).json({token})
              //console.log("token: ", token)
              res.status(200).send({"token" : token, "name": userData.Name, "email": userData.LoginEmail, "verifyStatus": userData.verifyStatus});
              // res.header("auth-token", token).json({
              //   error: null,
              //   data: {
              //     token,
              //   },
              // });
            }
          } else {
            res.status(400).json({"token" : null, "error": "Email or password not correct"});
            //res.send('Email not correct!')
          }
      } catch (error) {
        res.status(500)
        res.send(error.message)
      }
    }

    async createAdmin(req , res){
      try {
        //console.log("createAdmin: ", req.body);
        // LoginEmail, Password, Name, RetypePassword
        if(req.body.username != null && req.body.password != null && req.body.email != null && req.body.retyped_password != null) {
            const pool = await poolPromise
            const result = await pool.request()
              .input('Email',sql.VarChar , req.body.email)
              .query(queries.getEmailAdmin2)

              const adminEmailExist = result.recordset.length;
            
              if (!adminEmailExist) {
                //console.log("email no exist")
                const salt = await bcrypt.genSalt(10);
                req.body.password = await bcrypt.hash(req.body.password, salt)
                
                const pool = await poolPromise
                const result = await pool.request()
                .input('UserName', sql.VarChar, req.body.username)
                .input('Password', sql.VarChar , req.body.password)
                .input('Email', sql.VarChar, req.body.email)
                .input('StatusID', sql.Int, 1)
                .input('DepartmentID', sql.Int, 4)
                .input('FirstName', sql.VarChar, "Kevin")
                .input('LastName', sql.VarChar, "Ng")
                .query(queries.addAdmin)
                //console.log("addAdmin result: ", result.recordset[0].UserID);
                
                //res.json(result.recordset[0].UserID)
                var newUser = result.recordset[0];

                if (newUser) {
                  const token = await authController.generateTokenForUser(newUser.UserID, true);
                  // const token = jwt.sign({
                  //   name: newUser.UserName,
                  //   id: newUser.UserID,
                  // }, "anystring", {expiresIn: 3600})
                  // res.status(200).json({token})
                  //res.send(token)
                  res.status(200).send({"token" : token, "name": newUser.UserName, "email": newUser.LoginEmail});
                } else {
                  res.status(400).send('Failed to create admin!')
                }
              } else {
                res.status(400).send('Email already Exist!')
              }
        } else {
          res.status(400).send('Please fill all the details!')
        }
      } catch (error) {
        //console.log("in error")
        res.status(500)
        res.send(error.message)
      }
    }

    async createUser(req , res){
      try {
        //console.log("createUser: ", req.body);
        // LoginEmail, Password, Name, RetypePassword
        if(req.body.username != null && req.body.password != null 
          && req.body.email != null && req.body.retyped_password != null
          && (req.body.ic != null || req.body.passport != null)) {

          const pool = await poolPromise
          const result = await pool.request()
            .input('LoginEmail',sql.VarChar , req.body.email)
            .query(queries.getEmailUser)
          const userEmailExist = result.recordset.length;

          var userICExist = 0;
          if (req.body.ic != null && req.body.ic.length > 0) {
            const resultIC = await pool.request()
            .input('IC', sql.VarChar, req.body.ic)
            .query(qApplicant.getByIC)
            userICExist = resultIC.recordset.length
          }
          
          var userPassportExist = 0;
          if (req.body.passport != null && req.body.passport.length > 0) {
            const resultPassport = await pool.request()
            .input('Passport', sql.VarChar, req.body.passport)
            .query(qApplicant.getByPassport)
            userPassportExist = resultPassport.recordset.length
          }

          var uniqueString = await module.exports.randString();
          var activeExpires = Date.now() + 24 * 3600 * 1000;
        
          console.log(userEmailExist);
          console.log(userICExist);
          console.log(userPassportExist);
          if (!userEmailExist && !userICExist && !userPassportExist) {
            console.log("CreateUser : Creating new user...");
            //console.log("email no exist")
            //const salt = await bcrypt.genSalt(10);
            //req.body.password = await bcrypt.hash(req.body.password, salt)
            
            const pool1 = await poolPromise
            const result1 = await pool1.request()
            .input('Name', sql.VarChar, req.body.username)
            .input('IC', sql.VarChar, req.body.ic)
            .input('Passport', sql.VarChar, req.body.passport)
            .input('Password', sql.VarChar , req.body.password)
            .input('LoginEmail', sql.VarChar, req.body.email)
            .input('Email', sql.VarChar, req.body.email)
            .input('activeToken', sql.VarChar, uniqueString)
            .input('activeExpires', sql.VarChar, activeExpires)
            .query(queries.addUser)
            //console.log("addUser result: ", result.recordset[0].Id);
            
            //res.json(result1.recordset[0].UserID)
            var newUser = result1.recordset[0];

            if (newUser && uniqueString) {
              const token = await authController.generateTokenForUser(newUser.Id, false);
              // const token = jwt.sign({
              //   name: req.body.username,
              //   id: newUser.Id,
              // }, "anystring", {expiresIn: 3600})
              // res.status(200).json({token})

              const resultEmail = await module.exports.sendEmail(req.body.email,req.body.username,uniqueString);

              return res.status(200).send({"token" : token, "name": req.body.username, "email": req.body.email, "verifyStatus": 0});
            } else {
              //res.send('Failed to create user!')
              return res.status(400).send({error: __("error.registerUser")});
            }
          } else {
            //res.send('Email already Exist!')
            return res.status(400).send({error: __("error.emailIdNumberExisted")});
          }
        } else {
          res.status(400).send(__("error.fillDetails"))
        }
      } catch (error) {
        //console.log("in error")
        res.status(500)
        res.send(error.message)
      }
    }

    async randString() {
      // considering a 8 length string
      const len = 8;
      var randStr = '';
      for (let i=0; i<len; i++) {
        //ch = a number between 1 to 10
        const ch = Math.floor((Math.random() * 10) + 1)
        randStr += ch
      }

      return randStr
    }

    async resendEmail(req , res){
      try {
        //console.log("resend email function")
        
        if(req.body.username != null && req.body.email != null) {
          
          const pool = await poolPromise
          const result = await pool.request()
            .input('LoginEmail',sql.VarChar , req.body.email)
            .query(queries.getEmailUser)

            const userEmailExist = result.recordset.length;
            var uniqueString = await module.exports.randString();
            var activeExpires = Date.now() + 24 * 3600 * 1000;
          
            if (userEmailExist) {
              const pool1 = await poolPromise
              const result1 = await pool1.request()
              .input('LoginEmail', sql.VarChar, req.body.email)
              .input('activeToken', sql.VarChar, uniqueString)
              .input('activeExpires', sql.VarChar, activeExpires)
              .query(queries.updateActivateToken)

              var updateToken = result1.rowsAffected[0];

              if (updateToken && uniqueString) {
                
                const resultEmail = await module.exports.sendEmail(req.body.email,req.body.username,uniqueString);

                return res.status(200).send({"success" : true, "name": req.body.username, "email": req.body.email});
              } else {
                //res.send('Failed to create user!')
                return res.status(400).send({error: 'Failed to create activation token!'});
              }
            } else {
              //res.send('Email already Exist!')
              return res.status(400).send({error: 'Email Not Exist!'});
            }
        } else {
          res.status(400).send('Username and Email are required!')
        }

      } catch (error) {
        //console.log("in error")
        res.status(500)
        res.send(error.message)
      }
    }

    async resendEmailAdmin(req , res){
      try {
        //console.log("resend email function admin")
        
        if(req.body.username != null && req.body.email != null) {
          const pool = await poolPromise
          const result = await pool.request()
            .input('LoginEmail',sql.VarChar , req.body.email)
            .query(queries.getEmailAdminNewDB)

            const userEmailExist = result.recordset.length;
            var uniqueString = await module.exports.randString();
            var activeExpires = Date.now() + 24 * 3600 * 1000;
          
            if (userEmailExist) {
              const pool1 = await poolPromise
              const result1 = await pool1.request()
              .input('LoginEmail', sql.VarChar, req.body.email)
              .input('activeToken', sql.VarChar, uniqueString)
              .input('activeExpires', sql.VarChar, activeExpires)
              .query(queries.updateActivateTokenAdmin)

              var updateToken = result1.rowsAffected[0];

              if (updateToken && uniqueString) {
                
                const resultEmail = await module.exports.sendEmailAdmin(req.body.email,req.body.username,uniqueString);

                return res.status(200).send({"success" : true, "name": req.body.username, "email": req.body.email});
              } else {
                //res.send('Failed to create user!')
                return res.status(400).send({error: 'Failed to create activation token!'});
              }
            } else {
              //res.send('Email already Exist!')
              return res.status(400).send({error: 'Email Not Exist!'});
            }
        } else {
          res.status(400).send('Username and Email are required!')
        }

      } catch (error) {
        //console.log("in error")
        res.status(500)
        res.send(error.message)
      }
    }

    async sendEmail(email,username,uniqueString){
      try {
        //console.log("in send email")
        
        let htmlText =
          'Dear ' +
          username +
          '<br /><br />' +
          'Click <a href='+process.env.DOMAIN+'/verify-email?email='+email+'&'+'token='+uniqueString+ '> here </a> to verify your email.'
          '<br />'

        let transporter = nodemailer.createTransport({
            pool: false,
            host: "smtp.office365.com",
            port: 587,
            secureConnection: false,
            tls: { ciphers: 'SSLv3' },
            auth: {
                user: process.env.EMAIL_ADDRESS,
                pass: process.env.EMAIL_PASSWORD
            }
        });

        transporter.verify(function (error, success) {
            if (error) {
                console.log(error)
            } else {
                console.log("Server ready to send email")
            }
        })
        
        transporter.sendMail({
            from: process.env.EMAIL_ADDRESS,
            to: email,
            subject: '[TEST] Email Activation',
            html: htmlText
        }, 
        async function(err, info) {
            console.log("Sent email to " + email)
            if (err) {
                console.log(err)
            }
        })

      } catch (err) {
        console.log(err)
      }
    }

    async sendEmailAdmin(email,username,uniqueString){
      try {
        //console.log("in send email admin")
        
        let htmlText =
                'Dear ' +
                username +
                '<br /><br />' +
                'Click <a href='+process.env.DOMAIN+'/verify-email-admin?email='+email+'&'+'token='+uniqueString+ '> here </a> to verify your email.'
                '<br />'

        let transporter = nodemailer.createTransport({
            pool: false,
            host: "smtp.office365.com",
            port: 587,
            secureConnection: false,
            tls: { ciphers: 'SSLv3' },
            auth: {
                user: process.env.EMAIL_ADDRESS,
                pass: process.env.EMAIL_PASSWORD
            }
        });

        transporter.verify(function (error, success) {
            if (error) {
                console.log(error)
            } else {
                console.log("Server ready to send email")
            }
        })
        
        transporter.sendMail({
            from: process.env.EMAIL_ADDRESS,
            to: email,
            subject: '[TEST] Email Activation',
            html: htmlText
        }, 
        async function(err, info) {
            console.log("Sent email to " + email)
            if (err) {
                console.log(err)
            }
        })

      } catch (err) {
        console.log(err)
      }
    }

    async verifyEmail(req, res) {
      try { 
        //console.log("come in api email")
        if (req.body.email && req.body.token) {
          const pool = await poolPromise
          const result = await pool.request()
            .input('LoginEmail',sql.VarChar , req.body.email)
            .query(queries.getVerifyUser)

            const userEmailExist = result.recordset.length;
            var recordUser = result.recordset
          
            if (userEmailExist) {

              if (recordUser[0].Email===req.body.email && recordUser[0].activeToken===req.body.token && recordUser[0].activeExpires >= Date.now()) {
                //const pool = await poolPromise
                const result1 = pool.request()
                .input('verifyStatus', sql.SmallInt, 1)
                .input('LoginEmail', sql.VarChar, req.body.email)
                .query(queries.updateVerifyUser)

                result1.then(function(data) {
                  var updateVerifyUser = data.rowsAffected[0];

                  if (updateVerifyUser) {
                    res.status(200).send({message: 'Email activation successfully. Please login to your account!'});
                  } else {
                    res.status(400).send('Failed to activate the email!')
                  }
                })
              } else {
                res.status(400).send('Activation Link invalid!')
              }
              
              // res.status(200).send({message: 'Verify Done!'});
            } else {
              res.status(400).send('Email not exist!')
            }
        } else {
          res.status(400).send('Activation Link invalid!')
        }
      } catch (error) {
        res.status(500)
        res.send(error.message)
      }
    }

    async verifyEmailAdmin(req, res) {
      try { 
        //console.log("verify email admin function")
        if (req.body.email && req.body.token) {
          const pool = await poolPromise
          const result = await pool.request()
            .input('LoginEmail',sql.VarChar , req.body.email)
            .query(queries.getVerifyAdmin)

            const adminEmailExist = result.recordset.length;
            var recordAdmin = result.recordset
          
            if (adminEmailExist) {

              if (recordAdmin[0].Email===req.body.email && recordAdmin[0].activeToken===req.body.token && recordAdmin[0].activeExpires >= Date.now()) {
                //const pool = await poolPromise
                const result1 = pool.request()
                .input('verifyStatus', sql.SmallInt, 1)
                .input('LoginEmail', sql.VarChar, req.body.email)
                .query(queries.updateVerifyAdmin)

                result1.then(function(data) {
                  var updateVerifyAdmin = data.rowsAffected[0];

                  if (updateVerifyAdmin) {
                    res.status(200).send({message: 'Email activation successfully. Please login to your account!'});
                  } else {
                    res.status(400).send('Failed to activate the email!')
                  }
                })
              } else {
                res.status(400).send('Activation Link invalid!')
              }
              
              // res.status(200).send({message: 'Verify Done!'});
            } else {
              res.status(400).send('Email not exist!')
            }
        } else {
          res.status(400).send('Activation Link invalid!')
        }
      } catch (error) {
        res.status(500)
        res.send(error.message)
      }
    }

    async updateUser(req, res){
      try {
        //console.log("updateUser: ", req.body);
        if(req.body.Id != null && req.body.Name != null) {
        const pool = await poolPromise
          const result = await pool.request()
          .input('Id',sql.SmallInt , req.body.Id)
          .input('Name',sql.VarChar , req.body.Name)
          .input('LoginEmail',sql.VarChar , req.body.LoginEmail)
          .input('Email',sql.VarChar , req.body.Email)
          .input('Password',sql.VarChar , req.body.Password)
          .query(queries.updateUser)
          //console.log("updateUser result: ", req.body.Id);
          res.status(200).json({Id: req.body.Id})
        } else {
          res.status(400).send('All fields are required!')
        }
      } catch (error) {
        res.status(500)
        res.send(error.message)
      }
    }

    async updateUserICPassport(req, res) {
      try {
        if(req.body.Id != null && (req.body.IC != null || req.body.Passport != null) && (req.body.IC != '' || req.body.Passport != '')) {
          const pool = await poolPromise
          let result
          if (req.body.IC != null && req.body.IC != '') {
            let query = "UPDATE [Applicant] SET IC=@IC WHERE Id=@Id";
            result = await pool.request()
            .input('Id',sql.SmallInt , req.body.Id)
            .input('IC',sql.VarChar , req.body.IC)
            .query(query)
            console.log("Updated user " + req.body.Id + " ic to " + req.body.IC);
          }

          if (req.body.Passport != null && req.body.Passport != '') {
            let query = "UPDATE [Applicant] SET Passport=@Passport WHERE Id=@Id";
            result = await pool.request()
            .input('Id',sql.SmallInt , req.body.Id)
            .input('Passport',sql.VarChar , req.body.Passport)
            .query(query)
            console.log("Updated user " + req.body.Id + " passport to " + req.body.Passport);
          }
          
          res.status(200).json({success:true, message:'Successfully update details', Id: req.body.Id})
        } else {
          res.status(400).json({ message:'All fields are required!' })
        }
      } catch (error) {
        res.status(500)
        res.send(error.message)
      }
    }

    async deleteUser(req , res){
      try {
          //console.log(req);
        if(req.params.Id != null) {
          const pool = await poolPromise
            const result = await pool.request()
            .input('Id',sql.SmallInt , req.params.Id)
            .query(queries.deleteUser)
            //console.log("deleteUser result: ", req.params.Id);
            res.status(200).json({Id: req.params.Id})
          } else {
            res.status(400).send('Please fill all the details!')
          }
      } catch (error) {
        res.status(500)
        res.send(error.message)
      }
    }
}

const controller = new UserController()
module.exports = controller;