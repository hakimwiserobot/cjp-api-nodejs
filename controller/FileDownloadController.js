const { sql,poolPromise } = require('../database/db')
const fs = require('fs');
var rawdata = fs.readFileSync('./query/queries.json');
var queries = JSON.parse(rawdata);

class FileDownloadController {

    async downloadAFE_CV_SEA(req, res) {

        if (req.query.filename == null || req.query.filename == '') {
            return res.status(401).json({ success: false, message: "Please provide the file name" })
        }

        let dir = "../dist/assets/UserDoc";
        let folder = req.query.email;

        if (folder == null || folder == '') {
            dir = "D:/eCrew"
            folder = "Applicant"
        }

        let filepath = dir + '/' + folder + '/' + req.query.filename;
        // console.log("Get file at ", filepath);
        let isExist = fs.existsSync(filepath);
        if (!isExist) {
            filepath = dir + '/' + req.query.filename;
            isExist = fs.existsSync(filepath);
        }
        
        if (!isExist) {
            return res.status(500).json({ success: false, message: "File not exist" }); 
        }

        res.download(filepath, function(error) {
            if (error) {
                console.log("Error download : ", error)
                return res.status(500).json({ success: false, message: "Error downloading file" });
            }
        });
    }

    async downloadTimesheet(req, res) {

        if (req.query.filename == null || req.query.filename == '') {
            return res.status(401).json({ success: false, message: "Please provide the file name" })
        }

        if (req.query.filename.search('Timesheet') < 0) {
            return res.status(401).json({ success: false, message: "Incorrect file name" })
        }

        let dir = "../dist/assets/UserDoc";
        let filepath = dir + '/' + req.query.filename;
        let isExist = fs.existsSync(filepath);
        if (!isExist) {
            return res.status(500).json({ success: false, message: "File not exist" }); 
        }

        res.download(filepath, function(error) {
            if (error) {
                console.log("Error download : ", error)
                return res.status(500).json({ success: false, message: "Error downloading file" });
            }
        });
    }
}

const controller = new FileDownloadController()
module.exports = controller;