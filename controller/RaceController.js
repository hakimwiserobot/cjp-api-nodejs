const { sql,poolPromise } = require('../database/db')
const fs = require('fs');
var rawdata = fs.readFileSync('./query/queries.json');
var queries = JSON.parse(rawdata);

class RaceController {

    async getRace(req , res){
      try {
        const pool = await poolPromise
        const result = await pool.request()
          .query(queries.getRace)

        res.json(result.recordset)
      } catch (error) {
        console.log(error)
        res.status(500).json({ success:false, message:'Unable to retrieve list of race. Please try again.', error })
      }
    }

    async addRace(req , res){
      try {
        console.log("addRace: ", req.body);
        if(req.body.Race != null) {
          const pool = await poolPromise
          const result = await pool.request()
          .input('Race', sql.VarChar, req.body.Race)
          .query(queries.addRace)
          console.log("addRace result: ", result.recordset[0].Id);
          res.json(result.recordset[0])
        } else {
          res.status(200).json({ success:false, message:'Please fill all the details!' })
        }
      } catch (error) {
        console.log(error)
        res.status(500).json({ success:false, message:'Unable to add race. Please try again.', error })
      }
    }
    async updateRace(req, res){
      try {
        console.log("updateRace: ", req.body);
        if(req.body.Id != null && req.body.Race != null) {
        const pool = await poolPromise
          const result = await pool.request()
          .input('Id',sql.SmallInt , req.body.Id)
          .input('Race',sql.VarChar , req.body.Race)
          .query(queries.updateRace)
          console.log("updateRace result: ", req.body.Id);
          res.json({Id: req.body.Id})
        } else {
          res.status(200).json({ success:false, message:'Please fill all the details!' })
        }
      } catch (error) {
        console.log(error)
        res.status(500).json({ success:false, message:'Unable to update race. Please try again.', error })
      }
    }
    async deleteRace(req , res){
      try {
          console.log(req);
        if(req.params.Id != null) {
          const pool = await poolPromise
            const result = await pool.request()
            .input('Id',sql.SmallInt , req.params.Id)
            .query(queries.deleteRace)
            console.log("deleteRace result: ", req.params.Id);
            res.json({Id: req.params.Id})
          } else {
            res.status(200).json({ success:false, message:'Please fill all the details!' })
          }
      } catch (error) {
        console.log(error)
        res.status(500).json({ success:false, message:'Unable to delete race. Please try again.', error })
      }
    }
}

const controller = new RaceController()
module.exports = controller;