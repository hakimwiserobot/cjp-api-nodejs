const { sql,poolPromise } = require('../database/db')
const fs = require('fs');
var rawdata = fs.readFileSync('./query/queries.json');
var queries = JSON.parse(rawdata);

class TaskController {

    async updateDOB(req , res){
      try {

        let counter = 0;
        let arrayErrLength = [];
		let arrayErrFormat = [];

        const pool = await poolPromise;
        const result = await pool.request().query("SELECT [LoginEmail], [DOB], [IC] FROM [Applicant] WHERE [Nationality]='Malaysia'");
        let users = result.recordset;
		console.log("Found " + users.length + " users");
        // Loop applicant list
        for (let i = 0; i < users.length; i++) {
            let user = result.recordset[i];
            let NRIC = user.IC
            if (NRIC && NRIC.length == 12) {
                let year = '19' + NRIC.slice(0, 2);
                let month = NRIC.slice(2, 4);
                let date = NRIC.slice(4, 6);
                let DOB = year + '-' + month + '-' + date;

                let query_DOB = "UPDATE [Applicant] SET DOB=@DOB WHERE LoginEmail=@LoginEmail"

				if (parseInt(date) <= 31 && parseInt(month) <= 12) {
					const result2 = await pool
					.request()
					.input('DOB', sql.VarChar, DOB)
					.input('LoginEmail', sql.VarChar, user.LoginEmail)
					.query(query_DOB)

					console.log('NRIC ' + i + ' : ' + NRIC);

					counter++
				} else {
					arrayErrFormat.push(user.LoginEmail);
				}
                
            } else {
                arrayErrLength.push(user.LoginEmail);
            }
        }

		console.log('Updated ' + counter + ' user date of birth');
        res.status(200).json({ success:true, message:'Updated ' + counter + ' user date of birth', LengthIssue:arrayErrLength, FormatIssue:arrayErrFormat });

      } catch (error) {
        res.status(500).json({ success:false, message:'Unable to update users DOB', error});
      }
    }

    async moveApplicationDocIntoFolder(req , res) {
        console.log('Get all applications');
        const pool = await poolPromise;
        const result = await pool.request().query("SELECT [LoginEmail] FROM [Applicant]");
        const applicants = result.recordset;
        console.log('Total applicant : ' + applicants.length);

        console.log('Get all files');
        const files = fs.readdirSync('../dist/assets/UserDoc');
        console.log('Total files : ' + files.length);

        console.log('Search for applicants file');
        for(let i = 0; i < applicants.length; i++) {
            let email = applicants[i].LoginEmail;

            const filesApplicant = files.filter((file) => file.indexOf('_' + email) >= 0);
            if (filesApplicant.length > 0) {
                console.log('Found ' + filesApplicant.length + ' files for applicant ' + email);
                console.log('Moving applicant file into folder');
                let totalMovedFilesApplicant = 0;

                let dir = "../dist/assets/UserDoc";
                let folder = email;

                // Check if user folder exist
                if (!fs.existsSync(dir + "/" + folder)) {
                    fs.mkdirSync(dir + "/" + folder, { recursive:true });
                }

                for (let j = 0; j < filesApplicant.length; j++) {
                    let filename = filesApplicant[j];
                    var pathSource = dir + '/' + filename;
                    var pathDest = dir + '/' + folder + '/' + filename;
                    var source = fs.createReadStream(pathSource);
                    var dest = fs.createWriteStream(pathDest);

                    let result = await new Promise((resolve, reject) => {
                        source.pipe(dest);
                        source.on('end', function() { 
                            fs.unlink(pathSource, function(err) {
                                if(err) { 
                                    console.log('Error deleting file : ', err)
                                    resolve(false);
                                };
                                // console.log('Successfully move file ' + filename);
                                totalMovedFilesApplicant++;
                                resolve(true);
                            }); 
                        });
                        source.on('error', function(err) { 
                            console.log('Error moving file : ' + filename, err);
                            resolve(false);
                        });
                    });

                    if (!result) {
                        break;
                    }
                }

                console.log('Successfully move ' + totalMovedFilesApplicant + ' out of ' + filesApplicant.length + ' file(s) for ' + email)
            }
        }

        res.status(200).json({ success: true, message: 'Sucessfully move file into their respective folder' })
    }
}

const controller = new TaskController()
module.exports = controller;