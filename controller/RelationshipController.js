const { sql,poolPromise } = require('../database/db')
const fs = require('fs');
var rawdata = fs.readFileSync('./query/queries.json');
var queries = JSON.parse(rawdata);

class RelationshipController {

    async getRelationship(req , res){
      try {
        const pool = await poolPromise
        const result = await pool.request()
          .query(queries.getRelationship)
          
          res.json(result.recordset)
      } catch (error) {
        console.log(error)
        res.status(500).json({ success:false, message:'Unable to retrieve list of relationship. Please try again.', error })
      }
    }

    async addRelationship(req , res){
      try {
        console.log("addRelationship: ", req.body);
        if(req.body.Relationship != null) {
          const pool = await poolPromise
          const result = await pool.request()
          .input('Relationship', sql.VarChar, req.body.Relationship)
          .query(queries.addRelationship)
          console.log("addRelationship result: ", result.recordset[0].Id);
          res.json(result.recordset[0])
        } else {
          res.status(200).json({ success:false, message:'Please fill all the details!' })
        }
      } catch (error) {
        console.log(error)
        res.status(500).json({ success:false, message:'Unable to add relationship. Please try again.', error })
      }
    }
    async updateRelationship(req, res){
      try {
        console.log("updateRelationship: ", req.body);
        if(req.body.Id != null && req.body.Relationship != null) {
        const pool = await poolPromise
          const result = await pool.request()
          .input('Id',sql.SmallInt , req.body.Id)
          .input('Relationship',sql.VarChar , req.body.Relationship)
          .query(queries.updateRelationship)
          console.log("updateRelationship result: ", req.body.Id);
          res.json({Id: req.body.Id})
        } else {
          res.status(200).json({ success:false, message:'Please fill all the details!' })
        }
      } catch (error) {
        console.log(error)
        res.status(500).json({ success:false, message:'Unable to update relationship. Please try again.', error })
      }
    }
    async deleteRelationship(req , res){
      try {
          console.log(req);
        if(req.params.Id != null) {
          const pool = await poolPromise
            const result = await pool.request()
            .input('Id',sql.SmallInt , req.params.Id)
            .query(queries.deleteRelationship)
            console.log("deleteRelationship result: ", req.params.Id);
            res.json({Id: req.params.Id})
          } else {
            res.status(200).json({ success:false, message:'Please fill all the details!' })
          }
      } catch (error) {
        console.log(error)
        res.status(500).json({ success:false, message:'Unable to delete relationship. Please try again.', error })
      }
    }
}

const controller = new RelationshipController()
module.exports = controller;