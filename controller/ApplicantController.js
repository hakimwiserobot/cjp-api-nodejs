const { sql, poolPromise } = require('../database/db')
const fs = require('fs')
const sgMail = require('@sendgrid/mail')
var rawdata = fs.readFileSync('./query/queries.json')
var queries = JSON.parse(rawdata)
var rawdata2 = fs.readFileSync('./query/qApplicantApply.json');
var qApplicantApply = JSON.parse(rawdata2);
var rawdata3 = fs.readFileSync('./query/qApplicantDocument.json');
var qApplicantDocument = JSON.parse(rawdata3);
var GenerationController = require('./GenerationController.js')
var path = require('path')
const nodemailer = require("nodemailer");

//added 20/1/2021
const { PDFNet } = require('@pdftron/pdfnet-node')
const { __ } = require('i18n')

class ApplicantController {
  async getApplicant(req, res) {
    //console.log('getApplicant')
    try {
      const pool = await poolPromise
      const result = await pool.request().query(queries.getApplicant)

      
      res.json(result.recordset)
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }

  async getCurrency(req, res) {
    try {
      const pool = await poolPromise
      const result = await pool.request().query(queries.getCurrency)

      
      res.json(result.recordset)
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }

  async getApplicantGeneralQuestion(req, res) {
    try {
      const pool = await poolPromise
      const result = await pool
        .request()
        .query(queries.getApplicantGeneralQuestion)

      
      res.json(result.recordset)
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }

  async getApplicantGeneralAnswerById(req, res) {
    try {
      const pool = await poolPromise
      const result = await pool
        .request()
        .input('ApplyID', sql.SmallInt, req.params.ApplyID)
        .query(queries.getApplicantGeneralAnswerById)
      
      res.json(result.recordset)
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }

  // Added by Hakim on 14 Jan 2021 - Start
  async getApplicantMedicalReportQuestion(req, res) {
    try {
      const pool = await poolPromise
      const result = await pool
        .request()
        .query(queries.getApplicantMedicalReportQuestion)
      
      res.json(result.recordset)
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }

  async getApplicantMedicalReportAnswerById(req, res) {
    try {
      const pool = await poolPromise
      const result = await pool
        .request()
        .input('ApplyID', sql.SmallInt, req.params.ApplyID)
        .query(queries.getApplicantMedicalReportAnswerById)
      
      res.json(result.recordset)
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }
  // Added by Hakim on 14 Jan 2021 - End

  async getApplicantById(req, res) {
    try {
      const pool = await poolPromise
      const result = await pool
        .request()
        .input('Id', sql.SmallInt, req.params.Id)
        .query(queries.getApplicantById)

      
      res.json(result.recordset[0])
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }

  async getApplicantApply(req, res) {
    try {
      const pageNumber = req.query.page ? req.query.page : 1;
      const name = req.query.name ? req.query.name : '';
      const email = req.query.email ? req.query.email : '';
      const position = req.query.position ? req.query.position : '';
      const rowPerPage = 10;
      let numOfPage = 0;
      let searchParams = '';

      if (name != null && name.length > 0) {
        searchParams = searchParams + " AND (Name LIKE '%" + name + "%'"
        searchParams = searchParams + " OR MiddleName LIKE '%" + name + "%'"
        searchParams = searchParams + " OR LastName LIKE '%" + name + "%')"
      }
      if (email != null && email.length > 0) {
        searchParams = searchParams + " AND LoginEmail LIKE '%" + email + "%'"
      }
      if (position != null && position.length > 0) {
        searchParams = searchParams + " AND ApplyPosition='" + position + "'"
      }

      const pool = await poolPromise;

      // Get total rows in tabl
      let queryTotalRows = qApplicantApply.v_getTotalRowCount + " WHERE ApplyStatus='Offered'" + searchParams
      const resultTotalRows = await pool.request().query(queryTotalRows);
      const numOfRows = resultTotalRows.recordset[0].NumOfRows;
      numOfPage = Math.ceil(numOfRows / rowPerPage);
      // console.log('Total page : ', numOfPage);
      if (pageNumber > numOfPage && numOfRows > 0) {
        throw 'Exceed the number of page';
      }

      // Get requested pages
      let data = [];
      if (numOfRows > 0) {
        let skipRows = (pageNumber-1) * rowPerPage;
        let queryPagination = qApplicantApply.v_getOfferedApplication + searchParams + ' ORDER BY [ApplyDtApplication] DESC OFFSET ' + skipRows + ' ROWS FETCH NEXT ' + rowPerPage + ' ROWS ONLY';
        const resultPageRows = await pool.request().query(queryPagination);
        data = resultPageRows.recordset;
      }

      res.json({ success: true, page: pageNumber, totalPages: numOfPage, data: data });

    } catch (error) {
      console.log(error);
      res.status(500).send({ success: false, message: error.message ? error.message : error })
    }
  }

  async getApplicantApplyByIdAndLoginEmail(req, res) {
    try {
      const pool = await poolPromise
      const result = await pool
        .request()
        .input('Id', sql.VarChar, req.params.Id)
        .input('LoginEmail', sql.VarChar, req.params.LoginEmail)
        .query(queries.getApplicantApplyByLoginEmailById)
      //console.log('getApplicantApplyByLoginEmail')

      
      res.json(result.recordset[0])
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }

  async getApplicantApplyByLoginEmail(req, res) {
    try {
      const pool = await poolPromise
      const result = await pool
        .request()
        .input('LoginEmail', sql.VarChar, req.params.LoginEmail)
        .query(queries.getApplicantApplyByLoginEmail)
      //console.log('getApplicantApplyByLoginEmail')

      
      res.json(result.recordset[0])
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }

  async getApplicantByLoginEmail(req, res) {
    try {
      const pool = await poolPromise
      const result = await pool
        .request()
        .input('LoginEmail', sql.VarChar, req.params.LoginEmail)
        .query(queries.getApplicantByLoginEmail)

      
      res.json(result.recordset[0])
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }

  async getApplicantDropdownId(req, res) {
    try {
      var queryStr = queries.getApplicantDropdownId.join(' ')
      //console.log(queryStr)
      //console.log('req.params.Id: ', req.params.Id)
      const pool = await poolPromise
      const result = await pool
        .request()
        .input('Id', sql.SmallInt, req.params.Id)
        .query(queryStr)

      
      res.json(result.recordset)
      //res.json("ok")
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }

  async getApplicantStatus(req, res) {
    try {
      const pool = await poolPromise

      const result = await pool.request().query(queries.getApplicantStatus)

      
      res.json(result.recordset)
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }

  async getApplicantNextOfKin(req, res) {
    try {
      const pool = await poolPromise
      const result = await pool
        .request()
        .input('UserID', sql.VarChar, req.params.UserID)
        .query(queries.getApplicantNextOfKin)
      
      res.json(result.recordset)
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }

  async getApplicantTransferLog(req, res) {
    try {
      // console.log("in transfer log controller applicant")
      const pool = await poolPromise
      const result = await pool
        .request()
        .input('ApplicantApplyID', sql.VarChar, req.params.Id)
        .query(queries.getHistoryTransferLog)

      // console.log("get history transfer log")
      // console.log(result.recordset)
      
      res.json(result.recordset)
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }

  // Added by Hakim on 26 Jan 2021 - Start
  async getApplicantExperience(req, res) {
    try {
      const pool = await poolPromise
      const result = await pool
        .request()
        .input('UserID', sql.VarChar, req.params.UserID)
        .query(queries.getApplicantExperience)
      
      res.json(result.recordset)
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }
  // Added by Hakim on 26 Jan 2021 - End

  async getCharterer(req, res) {
    try {
      const pool = await poolPromise
      const result = await pool.request().query(queries.getCharterer)

      
      res.json(result.recordset)
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }

  async getCompetency(req, res) {
    try {
      const pool = await poolPromise
      const result = await pool.request().query(queries.getCompetency)

      
      res.json(result.recordset)
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }

  async getWorking(req, res) {
    try {
      const pool = await poolPromise
      const result = await pool.request().query(queries.getWorking)

      
      res.json(result.recordset)
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }

  async getGender(req, res) {
    try {
      const pool = await poolPromise
      const result = await pool.request().query(queries.getGender)

      
      res.json(result.recordset)
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }

  async getEducation(req, res) {
    try {
      const pool = await poolPromise
      const result = await pool.request().query(queries.getEducation)

      
      res.json(result.recordset)
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }

  async getDynamicPos(req, res) {
    try {
      const pool = await poolPromise
      const result = await pool.request().query(queries.getDynamicPos)

      
      res.json(result.recordset)
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }

  async getApplicantDocument(req, res) {
    try {
      //console.log("getApplicantDocument")
      //console.log(req.query)
      /* //console.log(req.query)
      //console.log(req.query.Id)
      //console.log(req.query.LoginEmail)
      //console.log(req.query.PositionID)
      //console.log(queries.getApplicantDocument) */
      // var queryStr = queries.getApplicantDocument.join(' ')
      var queryStr = qApplicantDocument.getLatestByPositionIdAndUserId.join(' ');
      ////console.log(queryStr)
      const pool = await poolPromise
      var result = await pool
        .request()
        .input('Id', sql.VarChar, req.query.Id)
        .input('LoginEmail', sql.VarChar, req.query.LoginEmail)
        .input('PositionID', sql.VarChar, req.query.PositionID)
        .query(queryStr)
      // loop thru to get single Document name
      //console.log("get document result")
      
      var applicationDocuments = [];
      if (result.recordset != null) {
        if (result.recordset.length == 0) {
          console.log("Get reusable documents");
          const DocumentIds = "1, 2" // Id of document to get when applicant document is empty
          result = await pool
            .request()
            .input('Id', sql.VarChar, req.query.Id)
            .input('LoginEmail', sql.VarChar, req.query.LoginEmail)
            .query(qApplicantDocument.getLatestReusableDocByUserId.join(' '))
  
          console.log("Number of reusable documents : " + result.recordset.length);
        }

        for (var documentItem of Object.values(result.recordset)) {
          ////console.log("documentItem: ", documentItem)
          if (documentItem != null && Array.isArray(documentItem.Document)) {
            documentItem.Document = documentItem.Document[0]
          }

          if (req.query.Id != 0 && documentItem.ApplyID != null && documentItem.ApplyID == req.query.Id) {
	  
            // Count number of null values
            var emptyCounter = 0;
            for (var key in documentItem) {
              if (documentItem[key] == null || documentItem[key] == '') {
                  emptyCounter++;
              }
            }
            documentItem.emptyCounter = emptyCounter;
            
            // Check for duplicate documents
            // Check if document existed in array
            var indexExisted = applicationDocuments.findIndex((doc) => {
              return doc.Document == documentItem.Document;
            })
            
            // If existed, compare the number of null/empty value
            // Object with higher count of null/empty value will be replace 
            if (indexExisted >= 0) {
              var docExisted = applicationDocuments[indexExisted];
              if (docExisted.emptyCounter > emptyCounter) {
                applicationDocuments[indexExisted] = documentItem;
              }
            } else {
              applicationDocuments.push(documentItem);
            }
          }
        }
      }
      
      if (applicationDocuments.length > 0) {
        res.json(applicationDocuments);
      } else {
        res.json(result.recordset);
      }
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }

  async updateApplicant(req, res) {
    try {
      req.body.adminName = req.user.loginId;
      req.body.adminID = req.user.id;

      if (
        req.body.Id != null &&
        req.body.ApplyPosition != null &&
        //&& req.body.DailyRate != null && req.body.StandbyRate != null
        //&& req.body.Allowance != null && req.body.TypesofAllowance != null
        //&& req.body.ContractPeriodFromInMth != null && req.body.ContractPeriodFrom != null
        //&& req.body.ContractPeriodTo != null && req.body.NameofVessel != null
        //&& req.body.IMONo != null && req.body.PortofRegistry != null
        req.body.Status != null &&
        req.body.adminName != null
      ) {
        // // Calculate daily rate
        // if (req.body.ContractPeriodFrom != null && req.body.ContractPeriodTo != null && req.body.Salary != null) {
        //   let startDate = new Date(req.body.ContractPeriodFrom)
        //   let endDate = new Date(req.body.ContractPeriodTo)
        //   let epochPeriod = endDate.valueOf() - startDate.valueOf()
        //   let numOfdays = epochPeriod / 86400000
        //   let totalSalary = req.body.Salary * Number(req.body.ContractPeriodFromInMth)
        //   req.body.DailyRate = (totalSalary / numOfdays).toFixed(2)
        // }

        var queryStr = queries.updateApplicant.join(' ')
        //console.log('queryStr: ', queryStr, ' IMONo: ', req.body.IMONo)
        const pool = await poolPromise
        const result = await pool
          .request()
          .input('Id', sql.SmallInt, req.body.Id)
          .input('ApplyPosition', sql.VarChar, req.body.ApplyPosition)
          // .input('DailyRate', sql.VarChar, req.body.DailyRate) // Comment by Hakim on 16 Feb 2021
          .input('DailyRate', sql.VarChar, req.body.Salary) // Added by Hakim on 16 Feb 2021
          .input('StandbyRate', sql.VarChar, req.body.StandbyRate)
          .input('StandbyAllowance', sql.VarChar, req.body.StandbyAllowance) // Added by Hakim on 25 Jan 2021
          .input('Allowance', sql.VarChar, req.body.Allowance)
          .input('AllowanceRemarks', sql.VarChar, req.body.AllowanceRemarks) // Added by Hakim on 27 Jan 2021
          .input('TypesofAllowance', sql.VarChar, req.body.TypesofAllowance)
          .input(
            'RepatriationHomePort',
            sql.VarChar,
            req.body.RepatriationHomePort
          ) // Added by Hakim on 16 Feb 2021
          .input(
            'ContractPeriodFromInMth',
            sql.VarChar,
            req.body.ContractPeriodFromInMth
          )
          .input('ContractPeriodFrom', sql.Date, req.body.ContractPeriodFrom)
          .input('ContractPeriodTo', sql.Date, req.body.ContractPeriodTo)
          .input('NameofVessel', sql.VarChar, req.body.NameofVessel)
          .input('IMONo', sql.VarChar, req.body.IMONo)
          .input('PortofRegistry', sql.VarChar, req.body.PortofRegistry)
          .input('Status', sql.VarChar, req.body.Status)
          .input('Currency', sql.VarChar, req.body.Currency)
          .input('Salary', sql.VarChar, req.body.Salary)
          .input('SalaryRemarks', sql.VarChar, req.body.SalaryRemarks) // Added by Hakim on 27 Jan 2021
          .input('OtherAllowance', sql.VarChar, req.body.OtherAllowance)
          .query(queryStr)
        //console.log('updateApplicant result: ', req.body.Id)

        // Set ApplyID for doc generation
        req.body.ApplyID = req.body.Id

        // Generate AFE/CV doc if Status = 'Review'
        // Generate SEA doc if Status = 'Offered'
        if (req.body.Status == 'Review') {
          var afe_result = await GenerationController.generateAFE(req, res)
          var cv_result = await GenerationController.generateCV(req, res)
        } else if (req.body.Status == 'Offered') {
          console.log('Confirm application ' + req.body.Id + ' from update flow')
          const resultConfrim = await pool
          .request()
          .input('Id', sql.SmallInt, req.body.Id)
          .input('AdminID', sql.VarChar, req.body.adminID)
          .input('AdminName', sql.VarChar, req.body.adminName)
          .query(qApplicantApply.UpdateConfirmById)

          var afe_result = await GenerationController.generateAFE(req, res)
          var cv_result = await GenerationController.generateCV(req, res)
          var sea_result = await GenerationController.generateSEA(req, res)
        }
        res.json({ Id: req.body.Id })
      } else {
        res.send('All fields are required!')
      }
    } catch (error) {
      res.status(500)
      res.send(error.message)
    }
  }

  async updateConfirmApplicant(req, res) {
    try {

      req.body.adminName = req.user.loginId;
      req.body.adminID = req.user.id;
      console.log('Admin', req.body.adminName, '(', req.body.adminID, ')', 'confirm application', req.body.Id);

      if (
        req.body.Id != null &&
        req.body.OfferPosition != null &&
        req.body.Status != null &&
        req.body.adminName != null
      ) {
        const pool = await poolPromise

        // const applicant = await pool
        //   .request()
        //   .input('Id', sql.SmallInt, req.body.Id)
        //   .query(queries.getApplicantById)

        const result = await pool
          .request()
          .input('Id', sql.SmallInt, req.body.Id)
          .input('OfferPosition', sql.VarChar, req.body.OfferPosition)
          // .input('DailyRate', sql.VarChar, req.body.DailyRate) // Comment by Hakim on 16 Feb 2021
          .input('DailyRate', sql.VarChar, req.body.Salary) // Added by Hakim on 16 Feb 2021
          .input('StandbyRate', sql.VarChar, req.body.StandbyRate)
          .input('StandbyAllowance', sql.VarChar, req.body.StandbyAllowance) // Added by Hakim on 25 Jan 2021
          .input('Allowance', sql.VarChar, req.body.Allowance)
          .input('AllowanceRemarks', sql.VarChar, req.body.AllowanceRemarks) // Added by Hakim on 27 Jan 2021
          .input('TypesofAllowance', sql.VarChar, req.body.TypesofAllowance)
          .input(
            'RepatriationHomePort',
            sql.VarChar,
            req.body.RepatriationHomePort
          ) // Added by Hakim on 27 Jan 2021
          .input(
            'ContractPeriodFromInMth',
            sql.VarChar,
            req.body.ContractPeriodFromInMth
          )
          .input('ContractPeriodFrom', sql.Date, req.body.ContractPeriodFrom)
          .input('ContractPeriodTo', sql.Date, req.body.ContractPeriodTo)
          .input('NameofVessel', sql.VarChar, req.body.NameofVessel)
          .input('IMONo', sql.VarChar, req.body.IMONo)
          .input('PortofRegistry', sql.VarChar, req.body.PortofRegistry)
          .input('Currency', sql.VarChar, req.body.Currency)
          .input('Salary', sql.VarChar, req.body.Salary)
          .input('SalaryRemarks', sql.VarChar, req.body.SalaryRemarks) // Added by Hakim on 27 Jan 2021
          .input('OtherAllowance', sql.VarChar, req.body.OtherAllowance)
          .input('AdminID', sql.VarChar, req.body.adminID)
          .input('AdminName', sql.VarChar, req.body.adminName)
          .query(queries.updateConfirmApplicant.join(' '))

        // Set ApplyID & Status for doc generation
        req.body.ApplyID = req.body.Id
        req.body.Status = 'Offered'

        // Generate AFE doc if Status = 'Offered'
        // console.log('generate AFE for application ', req.body.Id)
        var afe_result = true// await GenerationController.generateAFE(req, res)
        // if (afe_result == false) {
        //   console.log('AFE not generated due to file not found!')
        //   throw { success:false, message:'AFE file is not generated. Please proceed to "Report" -> "AFE/CV/SEA" to regenerate the document(s).' }

        // }
		
        // Generate CV doc if Status = 'Offered'
	      // console.log('generate CV for application ', req.body.Id)
        var cv_result = true//await GenerationController.generateCV(req, res)
        // if (cv_result == false) {
        //   console.log('CV not generated due to file not found!')
        //   throw { success:false, message:'CV file is not generated. Please proceed to "Report" -> "AFE/CV/SEA" to regenerate the document(s).' }

        // }

        // console.log('generate SEA for application ', req.body.Id)
        var sea_result = true//await GenerationController.generateSEA(req, res)
        // if (sea_result == false) {
        //   console.log('SEA not generated due to file not found!')
        //   throw { success:false, message:'SEA file is not generated. Please proceed to "Report" -> "AFE/CV/SEA" to regenerate the document(s).' }
        // }

        if (
          result != null &&
          sea_result == true &&
          cv_result == true &&
          afe_result == true
        ) {

          res.status(200).json({ success:true, message:__("success.confirmApplication") })

          // const applicantApply = await pool
          //   .request()
          //   .input('Id', sql.SmallInt, req.body.Id)
          //   .query(queries.getApplicantApplyById)

          // //added 20/1/2021
          // const extend = '.pdf'
          // let enterPath = path.resolve(
          //   '../dist/assets/UserDoc/' + applicantApply.recordset[0].FileSEA
          // )
          // const outputPath = path.resolve(
          //   '../dist/assets/UserDoc/' +
          //     applicantApply.recordset[0].FileSEA.replace('.docx', extend)
          // )

          //added 20/1/2021
          // const convertPDF = async () => {
          //   const pdfdoc = await PDFNet.PDFDoc.create()
          //   await pdfdoc.initSecurityHandler()
          //   await PDFNet.Convert.toPdf(pdfdoc, enterPath)
          //   pdfdoc.save(outputPath, PDFNet.SDFDoc.SaveOptions.e_linearized)
          // }
          //added 20/1/2021
          // await PDFNet.runWithCleanup(convertPDF)
          //   .then(() => {
          //     fs.readFileSync(outputPath, (err, data) => {
          //       if (err) {
          //         console.log('PDF ERROR 1!')
          //       }
          //     })
          //   })
          //   .catch((err) => {
          //     console.log('PDF ERROR 2!')
          // })
          //added 20/1/2021

          // // Added on 10 March 2021 - Start
          // // Convert doc to pdf - Start
          // const { PDFDocument } = require("pdf-lib")
          // const { wordToPdf } = require('node-docto')
          // var source = enterPath
          // var destination = outputPath

          // try {

          // } catch(error) {

          // }
  
          // let wordpdf_result = await wordToPdf(source, destination)
          //   .then(stdout => { 
          //     console.log("Successfully generate SEA pdf")
          //   });

          // if (wordpdf_result) {
          //   throw { success:false, message:'Unable to generate PDF file to email. Please proceed to "Report" -> "AFE/CV/SEA" page and click "Send Email" button to generate pdf and send email.' }
          // }

          // // await new Promise((resolve, reject) => {
          // //   /*convert docx to pdf (start)*/
          // //   wordToPdf(source, destination)
          // //   .then(stdout => { 
          // //     console.log("Successfully generate SEA pdf")
          // //     return resolve()
          // //   });
          // //   /*convert docx to pdf (end)*/
          // // })

          // let pathToPDF = outputPath /*converted file*/
          // let pathToImage = "../dist/assets/images/SKOM-logo.png" /*watermark pmg image*/

          // //check the pdf file existing
          // let pdf_result = await new Promise((resolve, reject) => {
          //   fs.stat(pathToPDF, (exists) => {
          //     if (exists == null) {
          //       console.log("PDF file exist");
          //       return resolve(true)
          //     } else if (exists.code === "ENOENT") {
          //       console.log("PDF file not exist");
          //       console.log("path:" + pathToPDF);
          //       return resolve(false)
          //     }
          //   });
          // })

          // if (!pdf_result) {
          //   throw { success:false, message:'Unable to generate PDF file to email. Please proceed to "Report" -> "AFE/CV/SEA" page and click "Send Email" button to generate pdf and send email.' }
          // }

          // //load pdf file
          // const pdfDoc = await PDFDocument.load(fs.readFileSync(pathToPDF));
          // //load watermark image
          // const img = await pdfDoc.embedPng(fs.readFileSync(pathToImage));

          // /* watermark placement (no need change) */
          // // const imagePage = pdfDoc.insertPage(0);
          // for (let i = 0; i < pdfDoc.getPageCount(); i++) {
          //   let imagePage = "";
          //   imagePage = pdfDoc.getPage(i);
          //   let xx = imagePage.getWidth();
          //   let yy = imagePage.getHeight();
          //   imagePage.drawImage(img, {
          //     x: imagePage.getWidth() / 2 - img.width / 2,
          //     y: imagePage.getHeight() / 2 - img.height / 2,
          //     width: img.width,
          //     height: img.height,
          //     opacity: 0.2
          //   });
          // }
          // /* watermark placement (no need change) */

          // //write pdf file
          // const pdfBytes = await pdfDoc.save();
          // const newFilePath = `../dist/assets/UserDoc/${path.basename(pathToPDF, ".pdf")}-result.pdf`;
          // fs.writeFileSync(newFilePath, pdfBytes);
          // // Convert doc to pdf - End
          // // Added on 10 March 2021 - End

          // let attachmentPDF = fs.readFileSync(outputPath).toString('base64')
          // let Filename = applicantApply.recordset[0].FileSEA.replace(
          //   '.docx',
          //   extend
          // )

          //added 26/1/2021
          // const adminDetails = await pool
          //   .request()
          //   .input('UserName', sql.VarChar, req.body.adminName)
          //   .query(queries.getLoginAdmin2)

          // let AdminName = adminDetails.recordset[0].UserName
          // let AdminLastName = adminDetails.recordset[0].LastName
          // let AdminEmail = adminDetails.recordset[0].LoginEmail

          // console.log('AdminData')
          // console.log(AdminName, AdminEmail)

          //send email
          // try {
          //   sgMail.setApiKey(
          //     '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'
          //   )
		  
          // let htmlText =
          //   '<strong>Dear <strong>' +
          //   (applicant.recordset[0].Name ? applicant.recordset[0].Name : '') +
          //   '<br />' +
          //   '<strong>Thank you for your application.<strong>' +
          //   '<br />' +
          //   '<strong>We are pleased to make the following offer of employment.<strong>' +
          //   '<br />' +
          //   '· Rank : ' +
          //   (applicantApply.recordset[0].OfferPosition ? applicantApply.recordset[0].OfferPosition : '') +
          //   '<br />' +
          //   '· Vessel Name : ' +
          //   (applicantApply.recordset[0].NameofVessel ? applicantApply.recordset[0].NameofVessel : '') +
          //   '<br />' +
          //   '· Daily Rate : ' +
          //   (applicantApply.recordset[0].DailyRate ? applicantApply.recordset[0].DailyRate : '') +
          //   '<br />' +
          //   '· Standby Allowance : ' +
          //   (applicantApply.recordset[0].StandbyAllowance ? applicantApply.recordset[0].StandbyAllowance : '') +
          //   '<br />' +
          //   '· Other Allowance : ' +
          //   (applicantApply.recordset[0].OtherAllowance ? applicantApply.recordset[0].OtherAllowance : '') +
          //   '<br />' +
          //   '· Contract Period : ' +
          //   (applicantApply.recordset[0].ContractPeriodFromInMth ? applicantApply.recordset[0].ContractPeriodFromInMth : '') +
          //   '	/month – this may subject to your final acceptance and sign on date.' +
          //   '<br />' +
          //   'Please acknowledge your acceptance of the above offer and email signed SEA to ' +
          //   (applicant.recordset[0].LoginEmail ? applicant.recordset[0].LoginEmail : '') +
          //   '<br />' +
          //   'Should you need further clarification, please contact ' +
          //   (AdminName ? AdminName : '') +
          //   ' ' +
          //   (AdminLastName ? AdminLastName : '') +
          //   ' at ' +
          //   (AdminEmail ? AdminEmail : '') +
          //   '<br />' +
          //   'SKOM Sdn. Bhd.' +
          //   '<br />' +
          //   'This is a computer generated message and no signature is required.'
		  
		      // console.log('3 File is generated inside userDoc folder!')

		      // Added by Hakim on 5 March 2021 - Start
          // var nodemailer = require('nodemailer')

          // let transporter = nodemailer.createTransport({
          //   sendmail: true,
          //   newline: 'windows',
          //   path: '/usr/lib/sendmail'
          // })

          // transporter.verify(function (error, success) {
          //   if (error) {
          //     console.log(error)
          //     throw { success:false, message:'Failed to send email' }
          //   } else {
          //     console.log("Server ready to send email")
          //   }
          // })
		  
          // transporter.sendMail({
          //   from: process.env.EMAIL_ADDRESS,
          //   to: applicant.recordset[0].LoginEmail,
          //   subject: 'SKOM eCrew Job Portal',
          //   html: htmlText,
			    //   attachments: [
          //     {
          //       filename: Filename,
          //       content: fs.createReadStream(newFilePath)
          //     }
          //   ]
          // }, 
          // async function(err, info) {
			    //   console.log("Sent email to " + applicant.recordset[0].LoginEmail)
          //   if (!err) {
          //     await pool
          //       .request()
          //       .input('Id', sql.SmallInt, applicant.recordset[0].Id)
          //       .query(qApplicantApply.UpdateSendEmailCountById)
          //     res.status(200).json({ success:true, message:'Successfully confirm application. Email has been sent to ' + applicant.recordset[0].LoginEmail, Id: req.body.Id })
          //   } else {
			    //     console.log(err)
          //     res.status(200).json({ success:false, message:'Failed to send email. Please proceed to "Report" -> "AFE/CV/SEA" page to send email.' })
          //   }
          // })
          // Added by Hakim on 5 March 2021 - End
        } else {
          throw { success:false, message:'Failed to generate applicant documents. Please proceed to "Report" -> "AFE/CV/SEA" to regenerate the document(s).' }
        }

        //res.json({ Id: req.body.Id })
      } else {
        throw { success:false, message:'All fields is require' }
      }
    } catch (error) {
	    console.log(error)
      res.status(500).json(error)
    }
  }

  async updateRejectApplicant(req, res) {
    try {

      req.body.adminName = req.user.loginId;
      req.body.adminID = req.user.id;
      console.log('Admin', req.body.adminName, '(', req.body.adminID, ')', 'reject application', req.body.Id);

      if (
        req.body.Id != null &&
        req.body.OfferPosition != null &&
        req.body.Status == 'Rejected' &&
        req.body.adminName != null
      ) {
        const pool = await poolPromise

        const application = await pool
          .request()
          .input('Id', sql.SmallInt, req.body.Id)
          .input('AdminID', sql.SmallInt, req.body.adminID)
          .input('AdminName', sql.VarChar, req.body.adminName)
          .query(qApplicantApply.UpdateApplyAsReject)

        if (application.recordset[0]) {
          res.status(200).json({ success:true, message:"Successfully reject application" });
        } else {
          res.status(200).json({ success:false, message:"Unable to reject application. Please try again later." });
        }
      } else {
        res.status(200).json({ success:false, message:"Unable to reject application. Please try again later." });
      }
    } catch (error) {
      console.log(error);
      res.status(500).json(error);
    }
  }

  async updateTerminateApplicant(req, res) {
    try {
      //console.log("check terminate")
      // console.log(req.body)

      req.body.adminName = req.user.loginId;
      req.body.adminID = req.user.id;
      console.log('Admin', req.body.adminName, '(', req.body.adminID, ')', 'terminate application', req.body.Id);

      if (
        req.body.Id != null && req.body.OfferPosition != null && req.body.adminName != null) {
        const pool = await poolPromise

        const applicant = await pool
          .request()
          .input('Id', sql.SmallInt, req.body.Id)
          .query(queries.getApplicantById)

        // console.log("check applicant data")
        // console.log(applicant.recordset[0])

        var originalData = applicant.recordset[0];

        const result = await pool
          .request()
          .input('Id', sql.SmallInt, req.body.Id)
          .input('OfferPosition', sql.VarChar, req.body.OfferPosition)
          // .input('DailyRate', sql.VarChar, req.body.DailyRate) // Comment by Hakim on 16 Feb 2021
          .input('DailyRate', sql.VarChar, req.body.Salary) // Added by Hakim on 16 Feb 2021
          .input('StandbyRate', sql.VarChar, req.body.StandbyRate)
          .input('StandbyAllowance', sql.VarChar, req.body.StandbyAllowance) // Added by Hakim on 25 Jan 2021
          .input('Allowance', sql.VarChar, req.body.Allowance)
          .input('AllowanceRemarks', sql.VarChar, req.body.AllowanceRemarks) // Added by Hakim on 27 Jan 2021
          .input('TypesofAllowance', sql.VarChar, req.body.TypesofAllowance)
          .input('RepatriationHomePort', sql.VarChar, req.body.RepatriationHomePort) // Added by Hakim on 27 Jan 2021
          .input('ContractPeriodFromInMth', sql.VarChar, req.body.ContractPeriodFromInMth)
          .input('ContractPeriodFrom', sql.Date, req.body.ContractPeriodFrom)
          .input('ContractPeriodTo', sql.Date, req.body.ContractPeriodTo)
          .input('NameofVessel', sql.VarChar, req.body.NameofVessel)
          .input('IMONo', sql.VarChar, req.body.IMONo)
          .input('PortofRegistry', sql.VarChar, req.body.PortofRegistry)
          .input('Currency', sql.VarChar, req.body.Currency)
          .input('Salary', sql.VarChar, req.body.Salary)
          .input('SalaryRemarks', sql.VarChar, req.body.SalaryRemarks) // Added by Hakim on 27 Jan 2021
          .input('OtherAllowance', sql.VarChar, req.body.OtherAllowance)
          .input('AdminID', sql.VarChar, req.body.adminID)
          .input('AdminName', sql.VarChar, req.body.adminName)
          .query(queries.updateTerminateApplicant.join(' '))

        var recordUser = result.rowsAffected[0];

        if (recordUser) {
          const resultEmail = await module.exports.sendEmail(req.body.LoginEmail,req.body.Name);

          const resultInsert = await pool.request()
            .input('ApplicantApplyID', sql.VarChar, originalData.Id)
            .input('OfferPosition', sql.VarChar, originalData.OfferPosition)
            // .input('DailyRate', sql.VarChar, req.body.DailyRate) // Comment by Hakim on 16 Feb 2021
            .input('DailyRate', sql.VarChar, originalData.Salary) // Added by Hakim on 16 Feb 2021
            .input('StandbyRate', sql.VarChar, originalData.StandbyRate)
            .input('StandbyAllowance', sql.VarChar, originalData.StandbyAllowance) // Added by Hakim on 25 Jan 2021
            .input('Allowance', sql.VarChar, originalData.Allowance)
            .input('AllowanceRemarks', sql.VarChar, originalData.AllowanceRemarks) // Added by Hakim on 27 Jan 2021
            .input('TypesofAllowance', sql.VarChar, originalData.TypesofAllowance)
            .input('RepatriationHomePort', sql.VarChar, originalData.RepatriationHomePort) // Added by Hakim on 27 Jan 2021
            .input('ContractPeriodFromInMth', sql.VarChar, originalData.ContractPeriodFromInMth)
            .input('ContractPeriodFrom', sql.Date, new Date(originalData.ContractPeriodFrom).toISOString().split('T')[0])
            .input('ContractPeriodTo', sql.Date, new Date(originalData.ContractPeriodTo).toISOString().split('T')[0])
            .input('NameofVessel', sql.VarChar, originalData.NameofVessel)
            .input('IMONo', sql.VarChar, originalData.IMONo)
            .input('PortofRegistry', sql.VarChar, originalData.PortofRegistry)
            .input('Currency', sql.VarChar, originalData.Currency)
            .input('Salary', sql.VarChar, originalData.Salary)
            .input('SalaryRemarks', sql.VarChar, originalData.SalaryRemarks) // Added by Hakim on 27 Jan 2021
            .input('OtherAllowance', sql.VarChar, originalData.OtherAllowance)
            .input('AdminID', sql.VarChar, req.body.adminID)
            .input('AdminName', sql.VarChar, req.body.adminName)
            .input('OldStatus', sql.VarChar, originalData.ApplyStatus)
            .input('Status', sql.VarChar, 'Terminated')
            .input('Reason', sql.VarChar, req.body.Reason)
            .query(queries.insertTransferLog.join(' '))

          res.status(200).json({ success:true, message:__("success.terminateApplication") })


          //res.status(200).send({"token" : token, "name": req.body.Name, "email": req.body.LoginEmail});
          
        } else {
          //res.status(400).send('Failed to send email!')
          throw { success:false, message:'Failed to terminate application' }
        }

        //res.status(200).json({ success:true, message:'Successfully terminate application. Please click "Send Email" to send email to the applicant' })

        //res.json({ Id: req.body.Id })
      } else {
        throw { success:false, message:'All fields is require' }
      }
    } catch (error) {
	    console.log(error)
      res.status(500).json(error)
    }
  }

  async updateReofferApplicant(req, res) {
    try {
      
      req.body.adminName = req.user.loginId;
      req.body.adminID = req.user.id;
      console.log('Admin', req.body.adminName, '(', req.body.adminID, ')', 'reoffer application', req.body.Id);

      if (req.body.Id != null && req.body.OfferPosition != null && req.body.ApplyStatus != null && req.body.adminName != null) {
        const pool = await poolPromise

        const applicant = await pool
          .request()
          .input('Id', sql.SmallInt, req.body.Id)
          .query(queries.getApplicantById)
        
        var originalData = applicant.recordset[0];
        // console.log("check original data")
        // console.log(originalData)

        // create new application record
        const newRecord = await pool
          .request()
          .input('Id', sql.SmallInt, originalData.Id)
          .query(queries.addConfirmApplicant.join(' '))

        var newRecordId = newRecord.recordset[0].Id

        if (newRecordId) {
          const terminateOldRecord = await pool
            .request()
            .input('Id', sql.SmallInt, originalData.Id)
            .query(queries.setApplicantTerminated.join(' '))
          
          // update new record to the latest information
          const result = await pool
            .request()
            .input('Id', sql.SmallInt, newRecordId)
            .input('OfferPosition', sql.VarChar, req.body.OfferPosition)
            // .input('DailyRate', sql.VarChar, req.body.DailyRate) // Comment by Hakim on 16 Feb 2021
            .input('DailyRate', sql.VarChar, req.body.Salary) // Added by Hakim on 16 Feb 2021
            .input('StandbyRate', sql.VarChar, req.body.StandbyRate)
            .input('StandbyAllowance', sql.VarChar, req.body.StandbyAllowance) // Added by Hakim on 25 Jan 2021
            .input('Allowance', sql.VarChar, req.body.Allowance)
            .input('AllowanceRemarks', sql.VarChar, req.body.AllowanceRemarks) // Added by Hakim on 27 Jan 2021
            .input('TypesofAllowance', sql.VarChar, req.body.TypesofAllowance)
            .input('RepatriationHomePort', sql.VarChar, req.body.RepatriationHomePort) // Added by Hakim on 27 Jan 2021
            .input('ContractPeriodFromInMth', sql.VarChar, req.body.ContractPeriodFromInMth)
            .input('ContractPeriodFrom', sql.Date, req.body.ContractPeriodFrom)
            .input('ContractPeriodTo', sql.Date, req.body.ContractPeriodTo)
            .input('Position', sql.VarChar, req.body.OfferPosition)
            .input('NameofVessel', sql.VarChar, req.body.NameofVessel)
            .input('IMONo', sql.VarChar, req.body.IMONo)
            .input('PortofRegistry', sql.VarChar, req.body.PortofRegistry)
            .input('Currency', sql.VarChar, req.body.Currency)
            .input('Salary', sql.VarChar, req.body.Salary)
            .input('SalaryRemarks', sql.VarChar, req.body.SalaryRemarks) // Added by Hakim on 27 Jan 2021
            .input('OtherAllowance', sql.VarChar, req.body.OtherAllowance)
            .input('AdminID', sql.VarChar, req.body.adminID)
            .input('AdminName', sql.VarChar, req.body.adminName)
            .query(queries.updateConfirmApplicant.join(' '))

          if (!req.body.adminName) {
            req.body.adminName = 'user'
          }

          const resultInsertOld = await pool.request()
            .input('ApplicantApplyID', sql.VarChar, originalData.Id)
            .input('OfferPosition', sql.VarChar, originalData.OfferPosition)
            // .input('DailyRate', sql.VarChar, req.body.DailyRate) // Comment by Hakim on 16 Feb 2021
            .input('DailyRate', sql.VarChar, originalData.Salary) // Added by Hakim on 16 Feb 2021
            .input('StandbyRate', sql.VarChar, originalData.StandbyRate)
            .input('StandbyAllowance', sql.VarChar, originalData.StandbyAllowance) // Added by Hakim on 25 Jan 2021
            .input('Allowance', sql.VarChar, originalData.Allowance)
            .input('AllowanceRemarks', sql.VarChar, originalData.AllowanceRemarks) // Added by Hakim on 27 Jan 2021
            .input('TypesofAllowance', sql.VarChar, originalData.TypesofAllowance)
            .input('RepatriationHomePort', sql.VarChar, originalData.RepatriationHomePort) // Added by Hakim on 27 Jan 2021
            .input('ContractPeriodFromInMth', sql.VarChar, originalData.ContractPeriodFromInMth)
            .input('ContractPeriodFrom', sql.Date, new Date(originalData.ContractPeriodFrom).toISOString().split('T')[0])
            .input('ContractPeriodTo', sql.Date, new Date(originalData.ContractPeriodTo).toISOString().split('T')[0])
            .input('NameofVessel', sql.VarChar, originalData.NameofVessel)
            .input('IMONo', sql.VarChar, originalData.IMONo)
            .input('PortofRegistry', sql.VarChar, originalData.PortofRegistry)
            .input('Currency', sql.VarChar, originalData.Currency)
            .input('Salary', sql.VarChar, originalData.Salary)
            .input('SalaryRemarks', sql.VarChar, originalData.SalaryRemarks) // Added by Hakim on 27 Jan 2021
            .input('OtherAllowance', sql.VarChar, originalData.OtherAllowance)
            .input('AdminID', sql.VarChar, req.body.adminID)
            .input('AdminName', sql.VarChar, req.body.adminName)
            .input('OldStatus', sql.VarChar, 'Terminated')
            .input('Status', sql.VarChar, 'Offered')
            .input('Reason', sql.VarChar, req.body.Reason)
            .query(queries.insertTransferLog.join(' '))
          
          const resultInsertNew = await pool.request()
            .input('ApplicantApplyID', sql.VarChar, newRecordId)
            .input('OfferPosition', sql.VarChar, originalData.OfferPosition)
            // .input('DailyRate', sql.VarChar, req.body.DailyRate) // Comment by Hakim on 16 Feb 2021
            .input('DailyRate', sql.VarChar, originalData.Salary) // Added by Hakim on 16 Feb 2021
            .input('StandbyRate', sql.VarChar, originalData.StandbyRate)
            .input('StandbyAllowance', sql.VarChar, originalData.StandbyAllowance) // Added by Hakim on 25 Jan 2021
            .input('Allowance', sql.VarChar, originalData.Allowance)
            .input('AllowanceRemarks', sql.VarChar, originalData.AllowanceRemarks) // Added by Hakim on 27 Jan 2021
            .input('TypesofAllowance', sql.VarChar, originalData.TypesofAllowance)
            .input('RepatriationHomePort', sql.VarChar, originalData.RepatriationHomePort) // Added by Hakim on 27 Jan 2021
            .input('ContractPeriodFromInMth', sql.VarChar, originalData.ContractPeriodFromInMth)
            .input('ContractPeriodFrom', sql.Date, new Date(originalData.ContractPeriodFrom).toISOString().split('T')[0])
            .input('ContractPeriodTo', sql.Date, new Date(originalData.ContractPeriodTo).toISOString().split('T')[0])
            .input('NameofVessel', sql.VarChar, originalData.NameofVessel)
            .input('IMONo', sql.VarChar, originalData.IMONo)
            .input('PortofRegistry', sql.VarChar, originalData.PortofRegistry)
            .input('Currency', sql.VarChar, originalData.Currency)
            .input('Salary', sql.VarChar, originalData.Salary)
            .input('SalaryRemarks', sql.VarChar, originalData.SalaryRemarks) // Added by Hakim on 27 Jan 2021
            .input('OtherAllowance', sql.VarChar, originalData.OtherAllowance)
            .input('AdminID', sql.VarChar, req.body.adminID)
            .input('AdminName', sql.VarChar, req.body.adminName)
            .input('OldStatus', sql.VarChar, 'Terminated')
            .input('Status', sql.VarChar, 'Offered')
            .input('Reason', sql.VarChar, req.body.Reason)
            .query(queries.insertTransferLog.join(' '))

          // Set ApplyID & Status for doc generation
          req.body.ApplyID = newRecordId
          req.body.Status = 'Offered'

          // Generate AFE/CV/SEA doc if Status = 'Offered'
          console.log('generate AFE ', newRecordId)
          var afe_result = await GenerationController.generateAFE(req, res)
          if (afe_result == false) {
            console.log('AFE not generated due to file not found!')
            throw { success:false, message:'AFE file is not generated. Please proceed to "Report" -> "AFE/CV/SEA" to regenerate the document(s).' }
          }
      
          console.log('generate CV ', newRecordId)
          var cv_result = await GenerationController.generateCV(req, res)
          if (cv_result == false) {
            console.log('CV not generated due to file not found!')
            throw { success:false, message:'CV file is not generated. Please proceed to "Report" -> "AFE/CV/SEA" to regenerate the document(s).' }
          }

          // Set ApplyID & Status for doc generation
          req.body.Status = 'Offered'

          console.log('generate SEA ', newRecordId)
          var sea_result = await GenerationController.generateSEA(req, res)
          if (sea_result == false) {
            console.log('SEA not generated due to file not found!')
            throw { success:false, message:'SEA file is not generated. Please proceed to "Report" -> "AFE/CV/SEA" to regenerate the document(s).' }
          }

          if (result != null && sea_result == true && cv_result == true && afe_result == true && resultInsertNew != null) {

            res.status(200).json({ success:true, id: newRecordId, message:__("success.reofferApplication") })
            
          } else {
            throw { success:false, message:'Failed to generate applicant documents. Please proceed to "Report" -> "AFE/CV/SEA" to regenerate the document(s).' }
          }
        } else {
          throw { success:false, message: 'Failed to create new record. Please try again' }
        }
        
      } else {
        throw { success:false, message:__("error.fillDetails") }
      }
    } catch (error) {
	    console.log(error)
      res.status(500).json(error)
    }
  }

  async updateExtendTransferApplicant(req, res) {
    try {

      req.body.adminName = req.user.loginId;
      req.body.adminID = req.user.id;
      console.log('Admin', req.body.adminName, '(', req.body.adminID, ')', 'extend/transfer application', req.body.Id);
      
      if (req.body.Id != null && req.body.OfferPosition != null && req.body.ApplyStatus != null && req.body.adminName != null) {
        const pool = await poolPromise

        const applicant = await pool
          .request()
          .input('Id', sql.SmallInt, req.body.Id)
          .query(queries.getApplicantById)
        
        var originalData = applicant.recordset[0];
        // console.log("check original data")
        // console.log(originalData)

        // create new application record
        const newRecord = await pool
          .request()
          .input('Id', sql.SmallInt, originalData.Id)
          .query(queries.addConfirmApplicant.join(' '))

        var newRecordId = newRecord.recordset[0].Id

        if (newRecordId) {
          const terminateOldRecord = await pool
            .request()
            .input('Id', sql.SmallInt, originalData.Id)
            .query(queries.setApplicantTerminated.join(' '))

          // update new record to the latest information
          const result = await pool
            .request()
            .input('Id', sql.SmallInt, newRecordId)
            .input('OfferPosition', sql.VarChar, req.body.OfferPosition)
            // .input('DailyRate', sql.VarChar, req.body.DailyRate) // Comment by Hakim on 16 Feb 2021
            .input('DailyRate', sql.VarChar, req.body.Salary) // Added by Hakim on 16 Feb 2021
            .input('StandbyRate', sql.VarChar, req.body.StandbyRate)
            .input('StandbyAllowance', sql.VarChar, req.body.StandbyAllowance) // Added by Hakim on 25 Jan 2021
            .input('Allowance', sql.VarChar, req.body.Allowance)
            .input('AllowanceRemarks', sql.VarChar, req.body.AllowanceRemarks) // Added by Hakim on 27 Jan 2021
            .input('TypesofAllowance', sql.VarChar, req.body.TypesofAllowance)
            .input('RepatriationHomePort', sql.VarChar, req.body.RepatriationHomePort) // Added by Hakim on 27 Jan 2021
            .input('ContractPeriodFromInMth', sql.VarChar, req.body.ContractPeriodFromInMth)
            .input('ContractPeriodFrom', sql.Date, req.body.ContractPeriodFrom)
            .input('ContractPeriodTo', sql.Date, req.body.ContractPeriodTo)
            .input('Position', sql.VarChar, req.body.OfferPosition)
            .input('NameofVessel', sql.VarChar, req.body.NameofVessel)
            .input('IMONo', sql.VarChar, req.body.IMONo)
            .input('PortofRegistry', sql.VarChar, req.body.PortofRegistry)
            .input('Currency', sql.VarChar, req.body.Currency)
            .input('Salary', sql.VarChar, req.body.Salary)
            .input('SalaryRemarks', sql.VarChar, req.body.SalaryRemarks) // Added by Hakim on 27 Jan 2021
            .input('OtherAllowance', sql.VarChar, req.body.OtherAllowance)
            .input('AdminID', sql.VarChar, req.body.adminID)
            .input('AdminName', sql.VarChar, req.body.adminName)
            .query(queries.updateConfirmApplicant.join(' '))

          if (!req.body.adminName) {
            req.body.adminName = 'user'
          }

          const resultInsertOld = await pool.request()
            .input('ApplicantApplyID', sql.VarChar, originalData.Id)
            .input('OfferPosition', sql.VarChar, originalData.OfferPosition)
            // .input('DailyRate', sql.VarChar, req.body.DailyRate) // Comment by Hakim on 16 Feb 2021
            .input('DailyRate', sql.VarChar, originalData.Salary) // Added by Hakim on 16 Feb 2021
            .input('StandbyRate', sql.VarChar, originalData.StandbyRate)
            .input('StandbyAllowance', sql.VarChar, originalData.StandbyAllowance) // Added by Hakim on 25 Jan 2021
            .input('Allowance', sql.VarChar, originalData.Allowance)
            .input('AllowanceRemarks', sql.VarChar, originalData.AllowanceRemarks) // Added by Hakim on 27 Jan 2021
            .input('TypesofAllowance', sql.VarChar, originalData.TypesofAllowance)
            .input('RepatriationHomePort', sql.VarChar, originalData.RepatriationHomePort) // Added by Hakim on 27 Jan 2021
            .input('ContractPeriodFromInMth', sql.VarChar, originalData.ContractPeriodFromInMth)
            .input('ContractPeriodFrom', sql.Date, new Date(originalData.ContractPeriodFrom).toISOString().split('T')[0])
            .input('ContractPeriodTo', sql.Date, new Date(originalData.ContractPeriodTo).toISOString().split('T')[0])
            .input('NameofVessel', sql.VarChar, originalData.NameofVessel)
            .input('IMONo', sql.VarChar, originalData.IMONo)
            .input('PortofRegistry', sql.VarChar, originalData.PortofRegistry)
            .input('Currency', sql.VarChar, originalData.Currency)
            .input('Salary', sql.VarChar, originalData.Salary)
            .input('SalaryRemarks', sql.VarChar, originalData.SalaryRemarks) // Added by Hakim on 27 Jan 2021
            .input('OtherAllowance', sql.VarChar, originalData.OtherAllowance)
            .input('AdminID', sql.VarChar, req.body.adminID)
            .input('AdminName', sql.VarChar, req.body.adminName)
            .input('OldStatus', sql.VarChar, 'Terminated')
            .input('Status', sql.VarChar, 'Offered')
            .input('Reason', sql.VarChar, req.body.Reason)
            .query(queries.insertTransferLog.join(' '))

          const resultInsertNew = await pool.request()
            .input('ApplicantApplyID', sql.VarChar, newRecordId)
            .input('OfferPosition', sql.VarChar, originalData.OfferPosition)
            // .input('DailyRate', sql.VarChar, req.body.DailyRate) // Comment by Hakim on 16 Feb 2021
            .input('DailyRate', sql.VarChar, originalData.Salary) // Added by Hakim on 16 Feb 2021
            .input('StandbyRate', sql.VarChar, originalData.StandbyRate)
            .input('StandbyAllowance', sql.VarChar, originalData.StandbyAllowance) // Added by Hakim on 25 Jan 2021
            .input('Allowance', sql.VarChar, originalData.Allowance)
            .input('AllowanceRemarks', sql.VarChar, originalData.AllowanceRemarks) // Added by Hakim on 27 Jan 2021
            .input('TypesofAllowance', sql.VarChar, originalData.TypesofAllowance)
            .input('RepatriationHomePort', sql.VarChar, originalData.RepatriationHomePort) // Added by Hakim on 27 Jan 2021
            .input('ContractPeriodFromInMth', sql.VarChar, originalData.ContractPeriodFromInMth)
            .input('ContractPeriodFrom', sql.Date, new Date(originalData.ContractPeriodFrom).toISOString().split('T')[0])
            .input('ContractPeriodTo', sql.Date, new Date(originalData.ContractPeriodTo).toISOString().split('T')[0])
            .input('NameofVessel', sql.VarChar, originalData.NameofVessel)
            .input('IMONo', sql.VarChar, originalData.IMONo)
            .input('PortofRegistry', sql.VarChar, originalData.PortofRegistry)
            .input('Currency', sql.VarChar, originalData.Currency)
            .input('Salary', sql.VarChar, originalData.Salary)
            .input('SalaryRemarks', sql.VarChar, originalData.SalaryRemarks) // Added by Hakim on 27 Jan 2021
            .input('OtherAllowance', sql.VarChar, originalData.OtherAllowance)
            .input('AdminID', sql.VarChar, req.body.adminID)
            .input('AdminName', sql.VarChar, req.body.adminName)
            .input('OldStatus', sql.VarChar, 'Terminated')
            .input('Status', sql.VarChar, 'Offered')
            .input('Reason', sql.VarChar, req.body.Reason)
            .query(queries.insertTransferLog.join(' '))

          // Set ApplyID & Status for doc generation
          req.body.ApplyID = newRecordId
          req.body.Status = 'Offered'

          // Generate AFE/CV/SEA doc if Status = 'Offered'
          console.log('generate AFE ', newRecordId)
          var afe_result = await GenerationController.generateAFE(req, res)
          if (afe_result == false) {
            console.log('AFE not generated due to file not found!')
            throw { success:false, message:'AFE file is not generated. Please proceed to "Report" -> "AFE/CV/SEA" to regenerate the document(s).' }
          }
      
          console.log('generate CV ', newRecordId)
          var cv_result = await GenerationController.generateCV(req, res)
          if (cv_result == false) {
            console.log('CV not generated due to file not found!')
            throw { success:false, message:'CV file is not generated. Please proceed to "Report" -> "AFE/CV/SEA" to regenerate the document(s).' }
          }

          // Set ApplyID & Status for doc generation
          req.body.Status = 'Offered'

          console.log('generate SEA ', newRecordId)
          var sea_result = await GenerationController.generateSEA(req, res)
          if (sea_result == false) {
            console.log('SEA not generated due to file not found!')
            throw { success:false, message:'SEA file is not generated. Please proceed to "Report" -> "AFE/CV/SEA" to regenerate the document(s).' }
          }

          if (result != null && sea_result == true && cv_result == true && afe_result == true && resultInsertNew != null) {

            res.status(200).json({ success:true, message:__("success.transferApplication"), Id: newRecordId })
            
          } else {
            throw { success:false, message:'Failed to generate applicant documents. Please proceed to "Report" -> "AFE/CV/SEA" to regenerate the document(s).' }
          }

          // res.json({ Id: newRecordId })
          
        } else {
          throw { success:false, message: 'Failed to create new record. Please try again' }
        }

      } else {
        throw { success:false, message:__("error.fillDetails") }
      }
    } catch (error) {
	    console.log(error)
      res.status(500).json(error)
    }
  }

  async sendEmail(email,username){
    try {
      //console.log("in send email")
      
      let htmlText =
        'Dear ' +
        username +
        '<br /><br />' +
        'Your application or position has been terminated.'
        '<br />'

      let transporter = nodemailer.createTransport({
          pool: false,
          host: "smtp.office365.com",
          port: 587,
          secureConnection: false,
          tls: { ciphers: 'SSLv3' },
          auth: {
              user: process.env.EMAIL_ADDRESS,
              pass: process.env.EMAIL_PASSWORD
          }
      });

      transporter.verify(function (error, success) {
          if (error) {
              console.log(error)
          } else {
              console.log("Server ready to send email")
          }
      })

      transporter.sendMail({
          from: process.env.EMAIL_ADDRESS,
          to: email,
          subject: 'Termination Notice',
          html: htmlText
      }, 
      async function(err, info) {
          console.log("Sent email to " + email)
          if (err) {
              console.log(err)
          }
      })

    } catch (err) {
      console.log(err)
    }
  }

}

const controller = new ApplicantController()
module.exports = controller
