var express = require("express");
const controller = require('../controller/BankAllotmentController')
var router = express.Router();

router.get('/get_allbankallotment', controller.getAllBankAllotment);
router.get('/get_bankallotment/:Id', controller.getBankAllotment);
router.get('/get_bankallotmentapplicant/:LoginEmail', controller.getBankAllotmentApplicant);
router.post('/add_bankallotment' , controller.addBankAllotmentDetails);
router.put('/update_bankallotmentdetails',controller.updateBankAllotmentDetails);
router.put('/update_bankallotmentfiles' , controller.updateBankAllotmentFiles);
router.put('/update_bankallotmentselections' , controller.updateSelection);

module.exports = router;