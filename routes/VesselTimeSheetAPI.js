var express = require("express");
const controller = require('../controller/VesselTimeSheetController')
var router = express.Router();

var sql = require('mssql/msnodesqlv8');

router.get('/get_vesseltimesheet', controller.getVesselTimeSheet);

router.post('/add_export', async (req, res) => {
    const result = await controller.generateTimesheet(req, res);
    if (result && result.isSuccess) {
        res.status(200).json({ value: result.filePath })
    } else {
        res.status(500).send('Unable to generate Timesheet')
    }
});

module.exports = router;