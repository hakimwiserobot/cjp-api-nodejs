var express = require('express');
var router = express.Router();
var authController = require('../controller/AuthController');
var moment = require('moment');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/token-refresh', async function (req, res) {

  const authorization = req.headers['authorization'];
  const bearer = authorization.split(' ');
  const token = bearer[1];

  let newToken = await authController.refreshToken(token);
  if (newToken) {
    res.status(200).json({ success:true, token: newToken, expiredAt: moment(new Date()).add(1, 'h') });
  } else {
    res.status(500).json({ success:false, message: "Unable to generate new token" })
  }
});

module.exports = router;
