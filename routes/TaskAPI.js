var express = require("express");
const controller = require('../controller/TaskController')
var router = express.Router();

router.post('/updateAllDOB', controller.updateDOB);
router.post('/moveFileIntoFolder', controller.moveApplicationDocIntoFolder);

module.exports = router;