var express = require("express");
const controller = require('../controller/GenerationController')
var router = express.Router();
const { sql, poolPromise } = require('../database/db')
const fs = require('fs');
var rawdata = fs.readFileSync('./query/queries.json');
var queries = JSON.parse(rawdata);

router.post('/add_AFE', async (req, res) => {
    const pool = await poolPromise
    const resultApproval = await pool
        .request()
        // .input('LoginEmail', sql.VarChar, req.body.LoginEmail)
        .input('Id', sql.VarChar, req.body.ApplyID)
        .query(queries.getApplicantionApprovalById)

    if (resultApproval.recordset[0]) {
        req.body.adminName = resultApproval.recordset[0].ConfirmByName;
        if (req.body.adminName == null || req.body.adminName == '') {
            res.status(500).send('Application have not been approve')
        }

        const result = await controller.generateAFE(req, res);
        if (result) {
            res.status(200).json({ Id: req.body.ApplyID })
        } else {
            res.status(500).send('Unable to generate AFE document')
        }
    } else {
        res.status(500).send('Application not found')
    }
});
router.post('/add_CV', async (req, res) => {
    const result = await controller.generateCV(req, res);
    if (result) {
        res.status(200).json({ Id: req.body.Id })
    } else {
        res.status(500).send('Unable to generate CV document')
    }
});
router.post('/add_SEA', async (req, res) => {
    const pool = await poolPromise
    const resultApproval = await pool
        .request()
        // .input('LoginEmail', sql.VarChar, req.body.LoginEmail)
        .input('Id', sql.VarChar, req.body.ApplyID)
        .query(queries.getApplicantionApprovalById)
        
    if (resultApproval.recordset[0]) {
        req.body.adminName = resultApproval.recordset[0].ConfirmByName;
        if (req.body.adminName == null || req.body.adminName == '') {
            res.status(500).send('Application have not been approve')
        }
        const result = await controller.generateSEA(req, res);
        if (result) {
            res.status(200).json({ Id: req.body.ApplyID })
        } else {
            res.status(500).send('Unable to generate SEA document')
        }
    } else {
        res.status(500).send('Application not found')
    }
});
router.post('/add_BAF', async (req, res) => {
    const pool = await poolPromise
    const resultApproval = await pool
        .request()
        .input('LoginEmail', sql.VarChar, req.body.LoginEmail)
        .input('Id', sql.VarChar, req.body.ApplyID)
        .query(queries.getApplicantionApprovalById)
        
    if (resultApproval.recordset[0]) {
        req.body.adminName = resultApproval.recordset[0].ConfirmByName;
        if (req.body.adminName == null || req.body.adminName == '') {
            res.status(500).send('Application have not been approve')
        }
        const result = await controller.generateBAF(req.body.ApplyID);
        if (result) {
            res.status(200).json({ Id: req.body.ApplyID })
        } else {
            res.status(500).send('Unable to generate BAF document')
        }
    } else {
        res.status(500).send('Application not found')
    }
});
router.post('/convert_toPDF', async (req, res) => {
    const docxName = req.body.docname
        
    if (await controller.convertWordToPdf(docxName)) {
        res.status(200).json({ success:true, message:'Successfully convert document to pdf' });
    } else {
        res.status(500).send('Error convert document');
    }
});
router.post('/verify', async (req, res) => {
    const Id = req.body.ApplyID;
    let result = await controller.verifyFileExist(Id)
    if (result.OfferPosition) {
        res.status(200).json(result);
    } else {
        res.status(500).send('Document not available');
    }
});

module.exports = router;