var express = require("express");
const controller = require('../controller/EmailCtrl')
var router = express.Router();

router.post('/test', controller.test);
router.post('/add_approved', controller.sendApprovedApplication);
router.post('/add_terminated', controller.sendTerminatedApplication);
router.post('/add_accepted', controller.sendAcceptedApplication);

module.exports = router;