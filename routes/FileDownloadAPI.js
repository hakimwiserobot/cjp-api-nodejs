var express = require("express");
const controller = require('../controller/FileDownloadController')
var router = express.Router();

var sql = require('mssql/msnodesqlv8');

router.get('/get_document', controller.downloadAFE_CV_SEA);
router.get('/get_timesheet', controller.downloadTimesheet)

module.exports = router;
